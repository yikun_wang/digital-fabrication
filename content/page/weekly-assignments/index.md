---
title: "Categories"
slug: "categories"
layout: "archives"
outputs:
    - html
    - json
menu:
    main:
        weight: -70
        params:
            icon: assignment
---
