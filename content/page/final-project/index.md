---
title: Final Project
date:
layout:
slug:
image:
aliases:
  - about-us
  - about-hugo
  - contact

menu:
    main:
        weight: -80
        params:
            icon: final
---

{{< video "./final.mp4">}}

## Inspirations and sketch


### My first idea is to play with light caustics.

"Caustics may be defined as the envelope of light rays that have been reflected or refracted from a curved surface and projected onto a surface where they can be visualised."

![Water drop and cause light caustics](notion_motion2.jpg)
![Olafur eliasson notion motion](notion_motion.jpg)
![The Ripple Light by poetic Lab](ripple-light.jpg)

The light dancing and waving always draws my attention. Light passing layers of leaves, traveling through shallow water, and wiggling on the sand, brings me peace and I can just stare for a long time. Thus, I came up with an idea about making a wall-mounted lamp and using the wall as a caustic canvas.

![light caustics](first_sketch1.jpg)

The first and second sketches use a small stick. When the sensor detects movement, it sends the signal to a motor, which actuates a stick to hit the surface of the liquid, and makes the light dance.

The third drawing uses a denser medium panel with a patterned surface, such as glass or acrylic. And the signal triggers the panel to rotate, to cause caustic waves.  


### The second idea uses an array of LEDs
![light art](first_sketch2.jpg)
In the first sketch. LED Lights are placed around the circle and the signal tells them to switch on and off.

The second sketch shows an array of LEDs that are placed on different rails on the frame. A motor controls the lights to move left and right, up and down, to create different light images every time.

Then later, I also come up with another idea of adding clock hands to the light frame and making it into an interactive light clock.

## Add some spice -- Cymatics and vibrations
### what is Cymatics
I want to create wave on the liquid surface so that light can dance when bouncing on the wall. Then Matti recogmmend a very cool video by music artist Nigel Stanford. He is exploring cymatics with different medium.
{{< youtube Q3oItpVa9fs >}}

**So what is cymatics?**  
Cymatics is a subset of modal vibrational phenomena, which can create wave pattern in a much elegent way. Hence I want to take a deeper look into it and see how can I exploit this phenomena.

> Typically the surface of a plate, diaphragm, or membrane is vibrated, and regions of maximum and minimum displacement are made visible in a thin coating of particles, paste, or liquid.<br>
> — <cite>Jenny, Hans[^1]</cite>

[^1]: Jenny, Hans (July 2001). Cymatics: A Study of Wave Phenomena & Vibration (3rd ed.). Macromedia Press. ISBN 978-1-888138-07-8.

There are also other scientists looking into this phenomena, such as John Stuart Reid and James Oliverio.  Here is a lecture  [Secrets of Cymatics II](https://www.youtube.com/watch?v=K8w_frOvfGo&ab_channel=CymaScope) by John Stuart Reid.

According to John Stuart Reid, the cymatics pattern in liquid are only created on the surface, and the water molecules are orgnized by the sound through the body of liquid.  

Cymatics patterns emerge from the medium are depending on the **driving frequency**. And I wonder how frequency would affect the pattern, then I found this video which shows how different frequency change the sand patterns on Chladni Plate.
{{< youtube wvJAgrUBF4w >}}
From the video, it looks like the higher frenquency, the more complicated the pattern is pattern is.

**But how would it affect the liquid?**   
I found a few video about testing frequency in a ripple tank:
[Properties of waves](https://www.youtube.com/watch?v=4_IlU5P1KEg&ab_channel=LightNmatter)  

Most of people exploring cymatics in liquid use a speaker. To know a bit more about speakers and better understand how sound works, I looked up and found those video which explained quite well:  
[How Speakers Make Sound](https://www.youtube.com/watch?v=RxdFP31QYAg&ab_channel=Animagraffs)  
[A Simple Animated Explanation of Pitch and Frequency](https://www.youtube.com/watch?v=6WIDhLeryWM&ab_channel=AdamWolf)
[How Sound Works - The Physics of Sound Waves](https://www.youtube.com/watch?v=QBYz82nS_xk&ab_channel=AdamMcGrath)


![section of speaker](speaker.png)
![How speakers transfer electricity to sound](speaker_sound.png)  
There are also some old and simple apparatus exploiting this phenomena, such as spouting bowl(龙洗) originate from China, or Himalayan singing bowl.  

### My experienment
Now I know a bit about the sound and cymatics. To see how this would work, I want to do some home test with the speaker. Here is a short video:
{{< video "./test.mp4">}}

**What I find**
* The volumn affect quite a lot
* I like the cymatics pattern more in low frequency (Low E : 82.4Hz), as the high frenquency seems very delicate and sharp.

**What can be affecting fators?**
* Different liquid medium?  
frozen vodka  
Cornstarch and water solution

### Further development of my project
The home test with the speaker seems promising. Thus, I want to develop further with my first idea. Currently, I designed four types.
![details of parts](sketch2.jpg)  
![other sketches](sketch3.jpg)

* Type 1 Mounted glass lamp
![Mounted glass lamp](type1.png)

![Mounted glass lamp detail](type1-1.png) ![Mounted glass lamp detail](type1-2.png)
The first type use the wave glass which is fixed with a motor. The LED light is fixed with the lamp cover, there will be a light sensor on the exterior of lamp cover.
* Type 2 Mounted water lamp
![Mounted water lamp](type2.png)

* Type 3 Standing water lamp
![Standing water lamp](type3.png)

![Standing water lamp details](type3-1.png)
* Type 4 Hanging glass lamp
![Hanging glass lamp](type4.png)


[Building DIY LED lights](https://www.youtube.com/watch?v=Y06VHj1GvLI&ab_channel=DIYPerks)
![Bulb electronic structure](bulb.JPG)

![dripping lamp](sketch4.jpg)   
![rotating lamp cover](sketch5.jpg)

[LED](https://www.digikey.fi/fi/products/detail/luminus-devices-inc/MP-3014-1100-50-80/6109331?s=N4IgTCBcDaIIxjgFgLTKQdjSgcgERAF0BfIA)

* development
Since we started to look into electronics and design our own board and program, I want to list the possibilities that my lamp could develop:  

* Sensing the ambient light condition
The water lamp brightness would adjust along the space brightness.  
If the room is dark, the LED brightness would become a bit dimmer, so light dancing is softer.  

Or the other way around, when the surrounding is bright (daytime), the switch would be off, and when it is getting dark, the light turns on automatically.


* Button
switch on and off the lamp  
change the light mode  
lamp brightness


Three different types
1. Glass, acrylic  
motor, LED, button  
(weight?) acrylic surface  

2. Audio, cymatics  
Vibration, speaker  
LED, button  

3. Water  
water dripping into the tank, disturb the surface and create wave.  
button,  
water tank, film of water  
valve to adjust the how many drops  
pump  
LED, button  


Motor
DC motor
is any of a class of rotary electrical motors that converts direct current electrical energy into mechanical energy. The most common types rely on the forces produced by magnetic fields.

 A DC motor's speed can be controlled over a wide range, using either a variable supply voltage or by changing the strength of current in its field windings.

A servomotor (or servo motor) is a rotary actuator or linear actuator that allows for precise control of angular or linear position, velocity and acceleration. It consists of a suitable motor coupled to a sensor for position feedback.

BLDCs are more efficient and require lower maintenance than brushed DC motors.

Stepper motor
a brushless DC electric motor that divides a full rotation into a number of equal steps.


## Final

## Sensor
### PIR Sensor
I want the light turns on to attract attention when people passes by. I am using PIR sensor from DFROBOT. There are three pins and easy to connect.

```
int ledPin = 10; // LED
int pirPin = 0;  // PIR Out pin
int pirStat = 0; // PIR status

void setup() {
 pinMode(ledPin, OUTPUT);     
 pinMode(pirPin, INPUT);     
 Serial.begin(115200);
}
void loop(){
 pirStat = digitalRead(pirPin);
 if (pirStat == HIGH) { // if motion detected
   digitalWrite(ledPin, HIGH);  
   Serial.println("You moved!");
 }
 else {
   digitalWrite(ledPin, LOW);
 }
}
```
There is a tiny blue LED on the board already but I linked the white LED on the main board still.
The first thing I notice is that there is a small delay, even though I stopped moving, the LED is still on for a while. At first I thought the LED is just delayed after receive the signal.
{{< video "./PIR_testing.mp4">}}

Then I opened serial monitor to check the massage the sensor is sending.
{{< video "./PIR_test.mp4">}}
I find that it is still sending signal even though I stopped moving. The LED turn off when the signal stopped. I am not sure if I want to adjust this for my final project yet.

Then I was testing the sensing distance. It can be around **3 meters** far. I think that is plenty for my final project.
{{< video "./PIR_distance.mp4">}}

## Lighting
* test with light
  * light position
  * amount
  * color pattern

### RGB
I am also really interested adjust the color for the lamp, so the atmosphere would be adjusted as well, thus I would need RGB LED.

RGB LED by mixing different intensity of the Red, Blue and Green to create any color. Equal amount of R,G,B will create white.

I find this video talking about RGB LEDs quite interesting: [RGB LEDs with Arduino - Standard & NeoPixel](https://www.youtube.com/watch?v=JpEFAXenTyY&ab_channel=DroneBotWorkshop)

There are more details in this article: [RGB LEDS](https://dronebotworkshop.com/rgb-leds/)

![](rgb-led-pinout.jpg)
![](rgb-neopixel-pinout.jpg)

In conventional RGB LED, each pin would control the intensity of Red, Blue and Green, while NeoPixels don’t expose the individual LED elements, instead their connections are as follows:
- Data In – An input from either the microcontroller or from the previous LED in a chain.
- 5-Volts – The LED power supply. No dropping resistors are required as they are internal.
- Ground – The Ground connection.
- Data Out – An output that is connected to the next LED on the chain.


Dropping Resistors
>As with any LED to calculate the value of the dropping resistors we need to know a few parameters first:  
 Supply Voltage (Vs) | The voltage of the power supply.  
 LED Forward Drop Voltage (Vf) | The amount of voltage the LED drops when inserted into the circuit.  
 LED Maximum Current (i) | How much current the LED can handle.  
R = (Vs - Vf) / i

I found about `NeoPixels` which seems interesting. NeoPixels is a brand name for Adafruit’s line of addressable RGB LEDs. These addressable LEDs make use of an integrated circuit called a WS2812 which is an RGB LED controller chip. The LED’s have this chip built-in to the same package.

A few interesting application of NeoPixels:  
[punk-helme](https://learn.adafruit.com/3d-printed-daft-punk-helmet-with-bluetooth)
[ambient-color-controller](https://learn.adafruit.com/ambient-color-controller)

### NeoPixel test
{{< video "./NeoPixel_test.mp4">}}

[How to Control WS2812 RGB](https://create.arduino.cc/projecthub/electropeak/neopixel-how-to-control-ws2812-rgb-led-w-arduino-46c08f)

[Tutorial](https://www.youtube.com/watch?v=BxO1_VIflTI&ab_channel=RoboCircuits)


`pixels.begin();` // INITIALIZE NeoPixel strip object  
`pixels.clear();` // Set all pixel colors to 'off'

The first NeoPixel in a strand is 0, second is 1, all the way up

```
for(int i=0; i<NUMPIXELS; i++) {
  pixels.setPixelColor(i, pixels.Color(0, 150, 0));
  pixels.show();   // Send the updated pixel colors to the hardware.
  delay(DELAYVAL); // Pause before next pass through loop
}
```
I find [this](https://learn.adafruit.com/adafruit-neopixel-uberguide/powering-neopixels) and [this](https://www.arduinoplatform.com/led-strips/controlling-and-powering-neopixels-with-arduino/) link powering NeoPixels

[Controlling and powering NeoPixels with Arduino](https://electronics.stackexchange.com/questions/443275/is-it-good-to-wire-ws2813-led-strip-di-and-bi-together)

Also I read there might be an issue of LED glitching and I need to add capacitor, but I was not sure how big capacitor should I use. Karl recogmmend 10uf for each pixel and I will use 13 LED, and 100uF for power cap. So I used a 220 uF capacitor.
https://forum.arduino.cc/t/ws2813-led-strip-glitches-after-1-hour/950156

I run a test with WS2812 in output week. But the NeoPixel strip I got for my final project is a newer version [WS2813]( https://www.utmel.com/components/ws2813-led-light-source-datasheet-power-and-wiring?id=629). And WS2813 has a backup data pin. I was a bit confused about the pin connection. And I find those info:  
![WS2813](WS2813.png)

If the first LED is working, then it receives and shows the data on it's DIN and then forwards the next pixel data via DOUT to the DIN of the next LED in the chain. Note that the second LED will now receive it's data on both BIN (direct from the first LED's DIN) and on DIN (repeated by the first LED).

If the first LED is non-functional, then the next LED first sees the first pixels' data on it's BIN (and disregards it). It is then expecting data for itself on the DIN channel but receives none. Thus it goes into backup mode and uses the data that came in on the BIN channel direct from the DIN of the first LED.

Thus, DIN is the one that needs to be connected on the first LED.

![](WS2813_work.png)

## Touch controller
* Touch pad
* Board
* Enclosure

### Touch pad
I design the touch pad layout in Grasshopper. And export the outline in Adobe illustrator. Then I use the vinyl cutter to cut the copper. As for the acylic panal, I laser cut it.

![pad layout designed in Grasshopper](controller_pad_design.png)

![]()
![copper_pad_cutout](copper_pad_cut.jpg)

![]()
![panal_cutout](panal_cutout.jpg)

Then I carefully move the copper pad with a tranfer tape, and attach it to the acylic panal.  
![pad_transfer](pad_transfer.jpg)
![copper_pad_on_acrylic](copper_pad_on_acrylic.jpg)

Then I solder each pad with a cable
![](add_cable.jpg)
![](add_cable2.jpg)

A few thing I notice for the first test:
- I find that the soldering point is bigger than I expect and they exposed to touch. I did not like that, so I update the design by adding a **small soldering pad** next to touch pad.
- And also the panal need to be **fixed** to the enclosure, so I add the screw holes.
- And I want the pad to be **"invisible"**, And I got a sheet of ITO film (Indium Tin Oxide), whoich is coated with indium tin oxide on one side and become a transparent conductive film.
![ITO](ITO.jpg)

![update pad layout]()

I use the multimeter to test the conductive side, which is the **blue side**. And another thing to pay attention is that not like copper, it still has **resistance**. Then I stick it to the plywood and test the laser cutting settings
![ITO_lasercut_test](ITO_lasercut_test.jpg)
![test conductive side]()

Then I laser cut the ITO film on plywood, with the blue side on top. Since the ITO film is not adhesive, I add three row of double sided tape in the back of the film and remove the excessive tape.  
![lasercut the pad shape of ITO film](ITO_lasercut.jpg)

![add double sided tape to transparent side](ITO_add_tape.jpg)

![transfer ITO pad to acylic panal](ITO_panal.jpg)
After carefully attach the pad on acylic panal, I need to add a bit of copper to the pad because the ITO film can not be soldered directly. Again I use the vinyl cutter to cut the copper tape. I was thinking remain the shape so I can tranfer accurately but they are so tiny in the end I just move one by one. So should have line them up to save the copper tape.  

![Cut copper soldering pad](ITO_copper_pad.jpg)

![copper on ITO pad](ITO_copper.jpg)

![ITO_solder_backside](ITO_solder_backside.jpg)

![ITO_solder_frontside](ITO_solder_frontside.jpg)

* After connect to TRILL and run test, most of the pad are working, but 3-4 pad are not stable and the data looks small. When I put on fingers on soldering point the number are normal again, so I figure the connection of those pad between ITO and copper were not good. I try to press it, or re-solder it, it was just not working, I suspect it is because I changed a few copper pad because they were wrinkly and that may have taken the ITO coat away.

### touch controller enclosure
After getting the touch pad prototype, I want to fix the touch pad acrylic as well as the trill break out board to the controller box. So I build the first enclosure version.
![model_V1](model_V1.png)

But I was not sure how it could be fixed, and I find [this site](https://www.instructables.com/Bolts-Screw-Thread-Nuts-and-Heat-Insert-Into-3D-Pr/) introduce different ways of inserting nut into 3D print. And it even include the 3D print STL file. I mostly want to try the insertion of nut since I want to keep the box pure and not expose the metal parts (My acylic panal is semi_tranparent).  

So I print the test model. The first time I was too late to insert the nut. The second time the nut insertion was successfully. The nut hole with 45 angle work a bit better than flat in the test model as it support itself and do not leave any excessive filament on the screw hole. So I change the model in the design. And since the enclosure is bigger and it should give me enough time to add nuts.
![test](nuthole_test.jpg)
![nut design](nut_design.png)

![first version of enclosure_model](enclosure_model.png)

![Add nut during printing](Add_screw.png)

However, one of the corner warped. In the beginning it was just a bit warped of brim, but after print out the whole model, the corner deviate from its origin location. Then I figure the Ultimaker printing bed is not leveled, and one corner brim was extra thin while the diagonal one was super thick and warped.

The other thing is that the box height is not enough and the cable was hard pressed. Another little improvement could be **rotating the TRILL fixture** so the I2C cable do not need to turn 90 degrees.

![](enclosure_model_V1.png)
![](enclosure_model_final.jpg)

### Board for touch pad
The idea I have is that I am going to have a controller that is going to have a few button's that can **select the frenquency**, and another group of button that can **play music**. At first I was checking those [Silicone Elastomer 4x4 Button Keypad](https://www.adafruit.com/product/1611). There is also a board [NeoTrellis](https://github.com/adafruit/Adafruit_Trellis) designed for this. [Adafruit Trellis with Python or CircuitPython](https://learn.adafruit.com/adafruit-trellis-diy-open-source-led-keypad/python-circuitpython)

There is another [capacitor touch sensors](https://www.elfadistrelec.fi/fi/mpr121-kapasitiivisen-kosketusanturin-kytkentae-adafruit-1982/p/30091164?ext_cid=shgooaqfifi-Shopping-CSS&gclid=Cj0KCQjw1ZeUBhDyARIsAOzAqQKhA3Njd8NvVPGT23dRprTTw5_PIukgdKyp2sz51OhQT9oOiKGMPhUaAoizEALw_wcB) break out board available. But after saw Matti's project I decide to use [TRILL CRAFT](https://eu.shop.bela.io/products/trill-craft/), a capacitive touch breakout board which has **30 channels** and can be used for custom touch interfaces, also it was using I2C to communicate, so much easier. This [tutorial](https://learn.bela.io/tutorials/trill-sensors/working-with-trill-craft/) introduce Trill Craft and general tips for working with Trill Craft.

[capacitive touch sensor](https://www.youtube.com/watch?v=3KQd9F0P6PI&feature=share&ab_channel=FriendlyWire)

[Programming Trill with Arduino](https://learn.bela.io/using-trill/trill-and-arduino/#arduino-example-sketches)

I was trying the example file of Trill craft, and here is what I got:

```
#include <Trill.h>

Trill trillSensor; // for Trill Craft

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  int ret = trillSensor.setup(Trill::TRILL_CRAFT);
  if(ret != 0) {
    Serial.println("failed to initialise trillSensor");
    Serial.print("Error code: ");
    Serial.println(ret);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(100);
  trillSensor.requestRawData();

  while(trillSensor.rawDataAvailable() > 0) {
    int data = trillSensor.rawDataRead();
    if(data < 1000)
      Serial.print(0);
    if(data < 100)
      Serial.print(0);
    if(data < 10)
      Serial.print(0);
    Serial.print(data);
    Serial.print(" ");
  }
  Serial.println("");

}
```
{{< video "./trill_test.mp4">}}

A few things I noticed:
* The maximum number is different every time
* Every time I reopen the serial monitor, all numbers goes to zero
* The wire touching could affect the data number

[Settings and sensitivity](https://learn.bela.io/using-trill/settings-and-sensitivity/)

Then I was trying to get the pin number.
```
void loop() {
  delay(100);
  trillSensor.requestRawData();

  if (trillSensor.rawDataAvailable() > 0) {
    for (int i = 0; i < 30; i++) {
      int data = trillSensor.rawDataRead();
      if ( data > 0) {
        Serial.print(i);
        Serial.print("=");
        Serial.print(data);
        Serial.print(" ");
      }
    }
    Serial.println("");
  }
}
```




```
//baseline
//sensorMode can be one of {centroid, raw, baseline, differential})
//prescaler

#include <Trill.h>

Trill trillSensor;

String serialInput;
char gCommandToken = ':';
char gEndToken = '\n';

long gLastMillis = 0;
bool printSensorVal = true;

int value[30];
int touch[30];
int threshold = 150;

void setup() {
  // Initialise serial and touch sensor
  Serial.begin(115200);
  int ret = trillSensor.setup(Trill::TRILL_CRAFT);
  if (ret != 0) {
    Serial.println("failed to initialise trillSensor");
    Serial.println("Error code: ");
    Serial.println(ret);
    Serial.println("\n");
  }
}

void loop() {

  if (Serial.available() > 0) {
    serialInput = Serial.readStringUntil(gEndToken);
    serialInput.toLowerCase();
    serialInput.trim();
    if (serialInput == "t") {
      printSensorVal = !printSensorVal;
    } else {
      int delimiterIndex = serialInput.indexOf(gCommandToken);
      String command = serialInput.substring(0, delimiterIndex);
      command.trim();
      String commandValue = serialInput.substring(delimiterIndex + 1);
      commandValue.trim();

      if (command == "prescaler") {
        Serial.print("setting prescaler to ");
        Serial.println(commandValue.toInt());
        trillSensor.setPrescaler(commandValue.toInt());
        gLastMillis = millis();  // Give 100ms for the chip to catch up
      } else if (command == "baseline") {
        Serial.println("updating baseline");
        trillSensor.updateBaseline();
        gLastMillis = millis();  // Give 100ms for the chip to catch up
      } else if (command == "threshold") {
        Serial.print("setting noise threshold to ");
        Serial.println(commandValue.toInt());
        trillSensor.setNoiseThreshold(commandValue.toInt());
        gLastMillis = millis();  // Give 100ms for the chip to catch up
      } else if (command == "bits") {
        Serial.print("setting numBits to ");
        Serial.println(commandValue.toInt());
        trillSensor.setScanSettings(0, commandValue.toInt());
        gLastMillis = millis();  // Give 100ms for the chip to catch up
      } else if (command == "mode") {
        Serial.print("setting mode to ");
        Serial.println(commandValue);
        trillSensor.setMode(modeFromString(commandValue));
        gLastMillis = millis();  // Give 100ms for the chip to catch up
      } else {
        Serial.println("unknown command");
      }
    }
  }

  if (printSensorVal) {
    if (millis() - gLastMillis > 100) {
      gLastMillis += 100;
      trillSensor.requestRawData();

      if (trillSensor.rawDataAvailable() > 0) {
        for (int i = 0; i < 30; i++) {
          int data = trillSensor.rawDataRead();
          value[i] = data;
          Serial.print(value[i]);
          Serial.print(" ");
        }
        Serial.println("");
      }
      for (int i = 0; i < 13; i++) {
        if (value[i] > threshold) {
          touch[i] = 1;
        }
        else {
          touch[i] = 0;
        }
        Serial.print(touch[i]);
        Serial.print(" ");
      }
      Serial.println("");
    }
  }
}

Trill::Mode modeFromString(String &  modeString) {
  modeString.toLowerCase();
  if (modeString == "centroid") {
    return Trill::CENTROID;
  } else if (modeString == "raw") {
    return Trill::RAW;
  } else if (modeString == "baseline" || modeString == "base") {
    return Trill::BASELINE;
  } else if (modeString == "differential" || modeString == "diff") {
    return Trill::DIFF;
  }
  return Trill::AUTO;
}

```

mode:raw
dispay all the raw value
baseline
mode:differential

prescaler:6

[state change detection](arduino.cc/en/Tutorial/BuiltInExamples/StateChangeDetection)


## amplifier
* Find frequency range
  * find songs fit in that frequency

### amplify and speaker test

![touch designer generate frequency](touch_designer.jpg)

{{< video "./amp_test.mp4">}}

![Laser cut amplify holder](amp_holder.jpg)
![amplify holder](amp_holder1.jpg)

![acylic water plate](water_plate.jpg)

![amplify and water plate](amp_plate.jpg)

[hello.ESP-WROOM-02D](https://www.digikey.fi/en/products/detail/espressif-systems/ESP-WROOM-02D-N4/10259353)
[wav trigger](https://www.sparkfun.com/products/13660)


[DFPlayer Mini MP3 Player](https://wiki.dfrobot.com/DFPlayer_Mini_SKU_DFR0299#target_6)

![DF player test](DF_player_test.png)

![Pin_Map_DF_player](Pin_Map_DF_player.png)  
![DF_player_pin_description](DF_player_pin_description.png)

![]()


[Youtube music](https://studio.youtube.com/channel/UCsOlKUuUunV1hqMpRNpvElA/music)


![]()

### Generate Frequency
I want to generate a certain frequency and people can just see what the pattern is at each frenquency. And I find this [Tutorial](https://www.programmingelectronics.com/an-easy-way-to-make-noise-with-arduino-using-tone/) that use `tone()` to generate frequency directly.

A duration can be specified, otherwise the wave continues until a call to `noTone()`, `noTone()` release or free the pin, so that another tone can be written.

`tone()` works independently of the `delay()` function.

The limitations of the tone() function include:
It can’t go lower than 31 hertz.
The tone() function cannot be used by two separate pins at the same time. When using tone() on different pins, you have to turn off the tone on the current pin with noTone() before using tone() on a different pin.

![tune basic](tune_basic.png)

`tone(pin, frequency)`
`tone( pin number, frequency in hertz, duration in milliseconds);`

```
int amplifier = 3;
void setup() {

}
void loop() {
  tone(amplifier, 300);
  delay(3500);
  noTone(amplifier);
  delay(1500);
}
```
- lamp cover
- water plate on top
- speaker and its holder beneath water plate
- LED strip around water plate

## Lamp base
Then I need to design the lamp base. I list a few things I need to incorporate:
- space for speaker and its holder
- LED strip outlet
- I2C outlet for touch controller
- power outlet
- PIR sensor outlet

And I use rhinocero to build the model.
The PCB need a surface to mount so it is more logic for me to seperate the base into two parts.

![](base_model_V0.png)
![](model_section.png)

![](base_model_V1.png)

![](base_model_V2.png)

![](base_model_V3.png)

![](all_model.png)

![](insert_magnet.jpg)
1
## Lamp cover
![](lamp_cover_test.jpg)

## PCB
![PCB_skematic](PCB_skematic.png)

![PCB_design](PCB_design.png)
![]()

## Programming

```

//baseline
//sensorMode can be one of {centroid, raw, baseline, differential})
//prescaler:6

#include <Trill.h>
#include "Arduino.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"
#define led 9

SoftwareSerial mySoftwareSerial(0, 1); // RX, TX
DFRobotDFPlayerMini myDFPlayer;
void printDetail(uint8_t type, int value_DF);

Trill trillSensor;

String serialInput;
char gCommandToken = ':';
char gEndToken = '\n';

long gLastMillis = 0;
bool printSensorVal = true;

int value[30];
int touch[30];
int threshold = 150;
int touch_state[30];
int Prev_touch_state [30];


void setup() {
  pinMode(led,OUTPUT);
  digitalWrite(led,HIGH);
  // Initialise serial and touch sensor
  mySoftwareSerial.begin(9600);
  Serial.begin(115200);
  Serial.println("HELLO");
  int ret = trillSensor.setup(Trill::TRILL_CRAFT);
  if (ret != 0) {
    Serial.println("failed to initialise trillSensor");
    Serial.println("Error code: ");
    Serial.println(ret);
    Serial.println("\n");
  }

  Serial.println();
  Serial.println(F("DFRobot DFPlayer Mini Demo"));
  Serial.println(F("Initializing DFPlayer ... (May take 3~5 seconds)"));

  if (!myDFPlayer.begin(mySoftwareSerial)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    while(true);
  }
  Serial.println(F("DFPlayer Mini online."));
}

void loop() {

  if (Serial.available() > 0) {
    serialInput = Serial.readStringUntil(gEndToken);
    serialInput.toLowerCase();
    serialInput.trim();
    if (serialInput == "t") {
      printSensorVal = !printSensorVal;
    } else {
      int delimiterIndex = serialInput.indexOf(gCommandToken);
      String command = serialInput.substring(0, delimiterIndex);
      command.trim();
      String commandValue = serialInput.substring(delimiterIndex + 1);
      commandValue.trim();

      if (command == "prescaler") {
        Serial.print("setting prescaler to ");
        Serial.println(commandValue.toInt());
        trillSensor.setPrescaler(commandValue.toInt());
        gLastMillis = millis();  // Give 100ms for the chip to catch up
      } else if (command == "baseline") {
        Serial.println("updating baseline");
        trillSensor.updateBaseline();
        gLastMillis = millis();  // Give 100ms for the chip to catch up
      } else if (command == "threshold") {
        Serial.print("setting noise threshold to ");
        Serial.println(commandValue.toInt());
        trillSensor.setNoiseThreshold(commandValue.toInt());
        gLastMillis = millis();  // Give 100ms for the chip to catch up
      } else if (command == "bits") {
        Serial.print("setting numBits to ");
        Serial.println(commandValue.toInt());
        trillSensor.setScanSettings(0, commandValue.toInt());
        gLastMillis = millis();  // Give 100ms for the chip to catch up
      } else if (command == "mode") {
        Serial.print("setting mode to ");
        Serial.println(commandValue);
        trillSensor.setMode(modeFromString(commandValue));
        gLastMillis = millis();  // Give 100ms for the chip to catch up
      } else {
        Serial.println("unknown command");
      }
    }
  }

  if (printSensorVal) {
    if (millis() - gLastMillis > 100) {
      gLastMillis += 100;
      trillSensor.requestRawData();

      if (trillSensor.rawDataAvailable() > 0) {
        for (int i = 0; i < 30; i++) {
          int data = trillSensor.rawDataRead();
          value[i] = data;
          Serial.print(value[i]);
          Serial.print(" ");
        }
        Serial.println("");
      }
      for (int i = 0; i < 9; i++) {
        if (value[i] > threshold) {
          touch[i] = 1;
        }
        else {
          touch[i] = 0;
        }
        touch_state[i] = touch[i];
        Serial.print(touch[i]);
        Serial.print(" ");
        if (touch_state[i] != Prev_touch_state[i]) {
          if (touch_state[i] == 1) {
            myDFPlayer.play(i);
          }
        }
        Prev_touch_state[i] = touch_state[i];
      }

      for (int i = 9; i < 13; i++) {
        if (value[i] > threshold) {
          touch[i] = 1;
        }
        else {
          touch[i] = 0;
        }
        touch_state[i] = touch[i];
        Serial.print(touch[i]);
        Serial.print(" ");
        if (touch_state[i] != Prev_touch_state[i]) {
          if (touch_state[9] == 1) {
            myDFPlayer.volumeUp();
          }
          if (touch_state[10] == 1) {
            myDFPlayer.next();
          }
          if (touch_state[11] == 1) {
            myDFPlayer.volumeDown();
          }
          if (touch_state[12] == 1) {
            myDFPlayer.previous();
          }
        }
        Prev_touch_state[i] = touch_state[i];
      }

      Serial.println("");
    }
  }
  if (myDFPlayer.available()) {
    printDetail(myDFPlayer.readType(), myDFPlayer.read()); //Print the detail message from DFPlayer to handle different errors and states.
  }
}



Trill::Mode modeFromString(String &  modeString) {
  modeString.toLowerCase();
  if (modeString == "centroid") {
    return Trill::CENTROID;
  } else if (modeString == "raw") {
    return Trill::RAW;
  } else if (modeString == "baseline" || modeString == "base") {
    return Trill::BASELINE;
  } else if (modeString == "differential" || modeString == "diff") {
    return Trill::DIFF;
  }
  return Trill::AUTO;
}


void printDetail(uint8_t type, int value_DF){
  switch (type) {
    case TimeOut:
      Serial.println(F("Time Out!"));
      break;
    case WrongStack:
      Serial.println(F("Stack Wrong!"));
      break;
    case DFPlayerCardInserted:
      Serial.println(F("Card Inserted!"));
      break;
    case DFPlayerCardRemoved:
      Serial.println(F("Card Removed!"));
      break;
    case DFPlayerCardOnline:
      Serial.println(F("Card Online!"));
      break;
    case DFPlayerPlayFinished:
      Serial.print(F("Number:"));
      Serial.print(value_DF);
      Serial.println(F(" Play Finished!"));
      break;
    case DFPlayerError:
      Serial.print(F("DFPlayerError:"));
      switch (value_DF) {
        case Busy:
          Serial.println(F("Card not found"));
          break;
        case Sleeping:
          Serial.println(F("Sleeping"));
          break;
        case SerialWrongStack:
          Serial.println(F("Get Wrong Stack"));
          break;
        case CheckSumNotMatch:
          Serial.println(F("Check Sum Not Match"));
          break;
        case FileIndexOut:
          Serial.println(F("File Index Out of Bound"));
          break;
        case FileMismatch:
          Serial.println(F("Cannot Find File"));
          break;
        case Advertise:
          Serial.println(F("In Advertise"));
          break;
        default:
          break;
      }
      break;
    default:
      break;
  }
}
```


FTDI good
softwareSerial questionable
