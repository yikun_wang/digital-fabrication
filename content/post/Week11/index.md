---
title: Output devices
Week: 11
weight: 110
description:
date:
slug:
image:
categories:
---

## Output devices
### Motor
- **DC motor**  
Any of a class of rotary electrical motors that converts **direct current (DC)** electrical energy into **mechanical energy**.
A DC motor's speed can be controlled over a wide range, using either a variable supply **voltage** or by changing the **strength of current** in its field windings.

![DC motor](DC_motor.png)
There is mechanical friction between the brushes and commutator – and since it is an electrical contact, it generally cannot be lubricated – there is mechanical wear of the brushes and commutator over the lifetime of the motor.

![H bridge](H_bridge.png)

- **Brushless DC motor** (BLDC)  
Instead of a mechanical commutator and brushes, the magnetic field of the stator is rotated by using **electronic commutation**. This requires the use of active control electronics.
![Brushless DC Motors](Brushless_DC_Motors.png)

![Brushed and Brushless Motors: Advantages and Disadvantages]( Advantages_and_Disadvantages.png)

- **Stepper motor**  
a **brushless DC electric motor** that divides a full rotation into a number of **equal steps**.

#### Motor Driver  
- Step Stick Motor driver
The SilentStepStick is a stepper driver board for 2-phase motors
peak output currents to **±2 A** and operating voltages to **40 V**.
![A4988 Motor Driver](A4988_Driver.png)

[Motor Controller/Driver](https://www.digikey.com/en/products/detail/watterott-electronic-gmbh/20170003-002/10071142)

![SilentStepStick Application Circuit](Application_Circuit.png)

**Motor Outputs**  
`M1A | M1B` -	Motor Coil 1   
`M2A	| M2B` - Motor Coil 2

**Control Inputs**  
`STEP` |	Step-Signal Input  
`DIR`	| Direction-Signal Input (internal pull-down resistor)  
`EN`	| Enable Motor Outputs (GND=on, VIO=off)  
[ The power stage becomes switched off (all motor
outputs floating) when this pin becomes driven to a high level.]

**Configuration**  
`MS1 | MS2 | MS3`	Step-Configuration, pd  

>MS1 + MS2 + MS3 to 5V (high), which will set the TMC2208 into 1/16 stealthChop mode.  
Microstep resolution configuration (internal pull-down resistors)    
MS2 10 DI (pd) MS2, MS1: 00: 1/8, 01: 1/2, 10: 1/4 11: 1/16  

**What is microstepping?**  
Microstepping is a way to make small steps even smaller in a stepper motor. The smaller the step, the higher the resolution and the better the vibration characteristics. In microstepping, a phase is not `fully on` or `fully off`.


- Full-Bridge DMOS PWM Motor Drivers
![A4952 block diagram](A4952_block_diagram.png)

Input terminals: controlling the speed and direction

`LSS` | Power return – sense resistor connection  
`VBB` | Load supply voltage  
`VREF` | Analog input  

![Full-Bridge PWM Control Truth Table](Control_Truth_Table.png)

![Bill_of Materials](Bill_of_Materials.jpg)

### Transistors
A semiconductor device used to **amplify** or **switch** electrical signals and power. Each transistor is build to handle a certain voltage and current, its important to check its datasheet. I also learn some basic knowledge about transistor from this [Transistors Explained - How transistors work](https://www.youtube.com/watch?v=J4oO7PT_nzQ&ab_channel=TheEngineeringMindset).

- **Bipolar transistor**
![Bipolar transistor configuration](transistor_configuration.png)

![](BJT.png)

![NPN Transistor and PNP transistor](npn_pnp.png)

![The characteristics of each type of transistor](characteristics_transistor.png)

![Metal help remove the heat](transistor_type.png)

- **MOSFET**  
[MOSFETs and Transistors with Arduino](https://www.youtube.com/watch?v=IG5vw6P9iY4&ab_channel=DroneBotWorkshop)
metal–oxide–semiconductor field-effect transistor, also known as the metal–oxide–silicon transistor. The direction can be changed easily and the speed can be controlled.
![MOSFET](MOSFET.png)

![Nchannel_Pchannel](Nchannel_Pchannel.png)

N-Ch turn ON (conductive state) when `positive` voltage is applied to G (Gate) for S (Source).  
P-Ch turn ON (conductive state) when `negative` voltage is applied to G (Gate) for S (Source).  
**N-Ch** have better performance and are also easier to use in terms of routing, so the majority of the MOSFETs used in the market are N-Ch.

### LED
[LED resistor calculator](https://ledcalculator.net/#p=5&v=4&c=20&n=1&o=w)

- [187 lm/W bright LED](https://www.digikey.com/en/products/detail/luminus-devices-inc/MP-3014-1100-50-80/6109331)

- RGB led  
For the RGB led linked in the notes, from datasheet, R fwd voltage (typ) = 2.0V, G = 3.2V, B = 3.2V.

- Neo pixel  
[WS2812B RGB LED in Circuits](https://www.hackster.io/Arnov_Sharma_makes/ws2812b-rgb-led-in-circuits-7cb39e)  
[Adafruit_NeoPixel](https://github.com/adafruit/Adafruit_NeoPixel)

### Other Components  
- **diode**  
an electrical component that allows the flow of current in only one direction

- **Potentiometer**  

- **polarized capacitor**   
Polarization is seen in the majority of `electrolytic capacitors`. This means that only when they are charged in line with their marked polarity can they achieve their rated maximum operating voltage, capacitance, and internal resistance.

**Why use polarized electrolytic capacitors?**  
To get a high capacitive density and value. Using ceramic or air as the dielectric would necessitate a capacitor volume of 100 to 1000 times greater.

A film or ceramic capacitor is an alternative, although they are physically larger and do not come in high capacitance values.

So my understanding of Polarized Capacitor is that it can store more voltage, which means give enormous capacitance values in tiny, cost-effective packaging.


## board for Motor
### Components
- microcontroller
- motor driver
  - Step stick
  - Full bridge
- Potentiometer
- LED
- Phototransistor - visible
- Button
- power supply
- UPDI connector

![Schematic](schematic.png)

![Assign thicker trace for high voltage power](Board_Setup.png)
Fatter traces for high current

### Import the outline for PCB
In the previous week I only tried the rectangle shape for my PCB board. And this time I want to make my motor board a bit special, and import the outline.  

![import graphics in KiCad](import_graphics.png)

![Import Vector Graphics ](Import_Vector_Graphics.png)
Select the graphic layer of `Edge Cuts`.

At first I was using Adobe Illustrator. I get the outline shape and export the **DXF**. There are a few DXF versions. I go with the default option but it did not work when I import into KiCad. It said imported succussfully, but I do not see the shape. I exported **SVG** as well, still nothing. (Those later become the issue)

Then I was searching online for this issue, and I find this link: [Import DXF to KiCad](https://forum.kicad.info/t/unable-to-import-dxf-to-kicad-pcbnew-v5-1-5-3/20635). And one answer was recommending **R12 format**. I did not see **R12 format** in my Adobe Illustrator, and I am not sure if it is because of my Illustrator version. Another answer was suggesting a plug-in **LibreCAD**, but I did not try.  

Then I was using this converting tool **Convertio** this [video](https://www.youtube.com/watch?v=VEf9hRK-8o0&ab_channel=PlumPot) suggested.
![Convertio DXF imported](Convertio_dxf.png)
It has something but the outline is a mess.

So I turned to Rhino which I am familier with. I draw the outline in rhino again and export the DXF file.
![outline in rhino](outline_rhino.png)
![DXF export version options from Rhino](dxf_option.png)

There is the R12 format verion. So I chose it and exported. Unfortunately, there is nothing. (still it shows imported succussfully).

![rhino default DXF export](dxf_default.png)
Then I exported with default format from rhino and there is the outline finally!! (even thouugh it is broken, still a break through LOL)

Then I just kept trying the different format. Finally! this **2007 lines** format imported succussfully the shape! Although the lines were grouped, and when I ungroup then, they become lots of short lines.
![](2007_line.png)
![2007_result](2007_result.png)

Then later When I open the gerbers in CopperCam, I find there are some werid dots and pad around the my PCB design. (I realize those are the `previous failed imported outline`. When I try to import, I do not see the shape but they imported succussfully.)
![failed imported](failed_imported.png)

Then I got some help from Kris. He helped erase those failed imported outlines and re-organize the layer. (Somehow, some components were in` Edge Cuts` layer also.)

Since the line were a bunch of short lines, we try to use AI to export the outline again. Then I figured that R14 could also work.
![AI DXF export version](AI_DXF_export.png)
Although when I check the line in CopperCam, it is the same result with the rhino exported DXF file. But it looks better in Kicad.

Kris also point out a few problem in my PCB design.
- The power supply component direction is wrong and the capacitor would block the changer to plug in.
- One capacitor is missing. I was imcorporating two motor drive and both of them need the capacitor. When I updated from schematic, I found both of the capacitors would just connect `HIGH VOLTAGE` and `GND`. And I thought one would just work, but Kris said it is not the way it works, so I add the other capacitor back.
![Motor final PCB design](Motor_PCB_Editor.png)

### Milling
Then my PCB milling failed a bunch of times.

![1st milling result](1st_mill.png)
The first failure, the back engrave did not match. The `white cross` moved when I allign the back layer with front layer. I check the origin before alignment and did not realize the origin moved after allignment. Even though when I export path calculation, I select `south west` as origin instead of `white cross`, it did not work out.   
Also one hole is mysteriously missing.

So I set the origin again after allignment, and calculated path again and exported.

![2nd milling result](2nd_mill.png)
This time the back layer engraving allign nicely. However, some traces are extra fine and some of the traces disappear totally. The back side traces looks perfect though. I was not sure what the reason is and I adjust the position of the board and mill again.

![3nd milling result](3rd_mill.png)
This time I thought it would be succussfully but still no. It looks a bit better than the second try but still there are some traces just milling.

Kris also point out that I should not **overlap the tape**.

When I comparing my failed board with succussful board I found that for some region, the milling bits goes way too deep into the board. Then I was using a thin piece of paper to test the Z origin. And I found that the diagnol corner's **Z origin** has 0.12mm difference.

![Finally! success milling!](success_milling.jpg)
After the MDF got milled flat, I milled again, also tuned it a bit (0.05mm) and the results was quite nice! For the back side, some traces did not mill through. Probably there were some chips on the edge still, I should clean it more thoroughly next time.

After clean the trace, testing the connectivity with multimeter.

### Stuffing
- Add rivet
![Pointy rivet punch to curve the rivet edge ](rivet1.jpg)
Push the rivet through holes. Use the pointy rivet punch to curve the rivet edge. Then the flat rivet punch to flatten the rivet edge.
![flat rivet punch to flatten the rivet edge](rivet2.jpg)

![Fix failed rivet](rivet3.jpg)
I got one rivet, and the rivet deformed. Here Kris was helping me drill the deformed part to the pin can go through.

### Programming
```
// defines pins numbers
const int LED = 8;
const int button = 10;
const int MOT_1 = 0;
const int MOT_2 = 1;

int val = 0;

void setup() {
  // Sets the two pins as Outputs
  pinMode(LED, OUTPUT);
  pinMode(button, INPUT_PULLUP);
  pinMode(MOT_1,OUTPUT);
  pinMode(MOT_2,OUTPUT);
}
void loop() {
  val = digitalRead(button);
  if (val == LOW) {
    digitalWrite(LED, LOW);
    digitalWrite(MOT_1,HIGH);
    digitalWrite(MOT_2,LOW);
  }
  else {
    digitalWrite(LED, HIGH);
    digitalWrite(MOT_2,LOW);
    digitalWrite(MOT_1,HIGH);
  }
}
```

In the end the board did not work when connect to 12V. The DC motor worked for a few seconds and stopped. Then the microcontroller burned. I think there are shorts somewhere. Matti helped me find the where the shorts are: the **vias** were too close to the track and did not mill through. so **the 5V track stick with the 12V via hole** and burn the ATtiny 1614.

## Seperate board for Motors (Still in prograss)
After the failure, I want to change the strategy, instead of stuffing everthing on one board, I want to seperate them. So I was making DC motor board, Step motor board and a power board.
![seperate board](seperate_board.png)

### Power board
![Power schematic](Power_schematic.png)
I mostly put regulator on this board and it can provide high voltage as well as 5V and 3.3V. I could measure the output voltages and ensure the output voltages are currect.

![power board](power_board.jpg)

### DC motor
![DC motor schematic](DC_motor_schematic.png)

![DC board connection](DC_board.jpg)

{{< video "./DC_motor.mp4">}}

### Step Motor
![Step motor schematic](Step_motor_schematic.png)

I was following this [link](https://www.instructables.com/Drive-a-Stepper-Motor-with-an-Arduino-and-a-A4988-/) to learn how to control Step motor. I was also looking into specifiations of two motors.

* Motor 1  
Stepper Motor: Bipolar, 200 Steps/Rev, 35×26mm, **7.4V**, 0.28 A/Phase  
This NEMA 14-size hybrid bipolar stepping motor has a 1.8° step angle (200 steps/revolution). Each phase draws 280 mA at 7.4 V, allowing for a holding torque of 650 g-cm (9 oz-in).

* Motor 2  
SM-42BYG011-25 - Kaksinapainen askelmoottori, NEMA 17 **12V**, SparkFun Electronics

*  **Set the Maximum Current**  
VREF max = (TrimpotMaxR/(TrimpotMaXR+R1)) x VDD = (10,000 / (10,000 + 30,000)) * 5 = 1.25V
ITripMAX (effectively max motor current) = VREF / ( 8 x Sense_resistor) = 1.25 / ( 8 * 0.1 ) = 1.5625A   
To calculate amps from measured VREF: **A = VREF / 0.8**  
To calculate VREF required for a target current: **VREF = A * 0.8**

* [StepStick Stepper Driver](https://sparks.gogo.co.nz/stepstick/index.html)  
The SLEEP pin is pulled high on the breakout, this pin is active low, so by default the board is NOT sleeping.  
The RESET pin is floating on the breakout (not pulled high or low), you can simply connect this to the SLEEP pin if you want, otherwise you will want to (so it will be pulled high also), or control it as you wish.  
The ENABLE pin is pulled low on the breakout, this pin is active low, so by default the board IS enabled.  
The MS1/2/3 pins are internally pulled down in the A4988, so by default all are low, and the board is in full step mode.  


```
int x;
#define BAUD (9600)
#define Pin_step 2
#define Dir 1
#define LED 6

void setup()
{
  Serial.begin(BAUD);
  pinMode(Pin_step,OUTPUT); // Step
  pinMode(Dir,OUTPUT); // Dir
}

void loop()
{
  digitalWrite(Dir,HIGH); // Set Dir high
  Serial.println("Loop 200 steps (1 rev)");
  for(x = 0; x < 200; x++) // Loop 200 times
  {
    digitalWrite(Pin_step,HIGH); // Output high
    delay(10); // Wait
    digitalWrite(Pin_step,LOW); // Output low
    delay(100); // Wait
  }
  Serial.println("Pause");
  delay(1000); // pause one second
}
```
Unfortunately, it did not work, not sure if the stepstick is broken or something is wrong.
```
#define DIR_PIN          1
#define STEP_PIN         2
//#define ENABLE_PIN       3
#define LED         6

void setup() {
  pinMode(DIR_PIN,    OUTPUT);
  pinMode(STEP_PIN,   OUTPUT);
  pinMode(LED,   OUTPUT);
  digitalWrite(LED, HIGH);
  //pinMode(ENABLE_PIN, OUTPUT);
  //digitalWrite(ENABLE_PIN, LOW);
}

void loop() {
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(100);
  digitalWrite(STEP_PIN, LOW);
  delay(1);
}
```
I test with a different code but still nothing.



## PCB for LED strip
Since my motor drive board does not work, I made a another board with only MOSFET to power the LED strip.

### Components
- microcontroller ATTINY412
- LED strip
- LED
- Button
- power supply
- UPDI connector

![LED schematic](LED_schematic.png)

![LED PCB design](LED_pcb.png)

![LED PCB board milling result](LED_milling.jpg)

![Gathering components](LED_component.jpg)

![Stuffing the board](LED_stuffing.jpg)

### Programming

```
// defines pins numbers
const int LED = 2;
const int button = 1;
const int high_vol = 3;

int val = 0;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED, OUTPUT);
  pinMode(button, INPUT_PULLUP);
  pinMode(high_vol, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {

  val = digitalRead(button);
  if (val == LOW) {
    digitalWrite(LED, LOW);
    digitalWrite(high_vol, HIGH);
  }
  else {
    digitalWrite(LED, HIGH);
  }
}
```

{{< video "./LED_strip.mp4">}}


## Measure the power consumption

![Connect positive and negative when power suppoly is off](Set_current_limit1.jpg)
![Turn on power suppoly and set current limit](Set_current_limit2.jpg)

Measure current
![plug in ampere meter](Measure_current1.png)
![How to connect multimeter](Measure_current2.jpg)

- Measure motor
![](measure_motor1.jpg)
![](measure_motor2.jpg)

- Measure LED strip
![](measure_LED1.jpg)
![](measure_LED2.jpg)

## Reflection
All in all, with all of this test, I learned my lesson that do not try to stuff everthing in in one board, as the saying goes, do not put all eggs in one basket. It is quite problematic and hard to debug when there are lots of components. And seperate the board works is easier to debug and even though one board broke, I just need to fix that one and others remind safe.

## File
[LED strip gerbers file](LED_strip_gerbers.zip)
