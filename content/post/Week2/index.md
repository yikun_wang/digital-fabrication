---
title: Project Management
week: 02
description: Learning about Git and adapt Hugo themes
weight: 20
date:
slug:
image: head.PNG
categories:
    - Git
    - HTML
    - CSS
    - Hugo
---

## Goal for the week

* Learning about hugo
* adapting themes template
* Use Git and GitLab to manage files in my repository

## Hugo setup
**Hugo** is a static website generator that work like frames for building websites. It allows us to define blocks such as head block or foot block, and add them to the framework of our website. Those blocks will display on every site instead of adding code to each page. Also the contents and frame of the website are divided. In this way, building the website is more feasible and clearer. Here is the [Hugo tutorial](https://www.youtube.com/watch?v=7wCidM35tp8&t=2267s&ab_channel=AaltoFablab) I learned to setup hugo.

### Install Hugo
To install, download the needed version from [Hugo Releases](https://github.com/gohugoio/hugo/releases), and unzip into a folder /C/Hugo/bin.

Add Hugo to Windows PATH settings:  
`which mv`  //check where is the directory for command
`mv hugo.exe /usr/bin/mv`.  
or
`mv hugo.exe D/cmder/bin`(my cmder installed in D disk).

Change the directory and verify if hugo is Executable.


### Update Hugo version
When I was looking at hugo themes, I found the template I wanted to use require hugo extended version. So I needed to switch my hugo version.  
Since I installed Chocolatey, I tried to use choco to install hugo extented:
`choco install hugo-extended -confirm`.
Choco install package in a different directory. And I encountered problems of completely removing old hugo, as well as setting the PATH of new Hugo.  

In the end, I use the same way of downloading hugo extended from github:  
`which hugo` // remove the old hugo folder  
`rm hugo.exe /usr/bin/mv` // remove the old hugo PATH  
Install the hugo extended.  
`./hugo version` // check current directory hugo version is hugo extended   
hugo PATH setting.   
`hugo version`  //Verify the hugo extended set up correctly.


### Initialize a new website
`hugo new site ./`//may error   
`hugo new site ./ --force`

Here is the basic structure for my hugo website setup:
```
└── content
    └── _index.md  //underscore to link the markdown file with index template
    └── about.md
    └── final_project.md
    └── weekly_assignment
        ── week01.md

└── layouts //HTML template
    └── _default
        ── single.html
    └── partial
        ── head.html
        ── navigation.html
        ── sidebar.html
        ── footer.html
    └── shortcodes
        ── video.html
    └── static //image

└── config.toml
```

To run hugo:  
`hugo`  // This will generate the public folder.  
`hugo server`// we use hugo server while we build the websites as it has high performance.

Others commonly used command:  
`hugo -h`//help for hugo

## Learning about Hugo
A bit notes about what I learned:  
### Hugo variables
* Predefined variable  
* The custom variables: need to be prefixed with `$`  

`{{ .Site.title}}`//dot-prefix to access elements  
`{{ partial "navigation.html" . }}`// pass along the current context **.**"
`{{ . }}`/refers to the current context unless in loop  

#### Site variable  
`.Site.Sections`//top-level directories of the site.  
`.Site.Pages`//array of all content ordered by Date with the newest first  
`.Site.Taxonomies`//the taxonomies for the entire site.  
`.Site.Params`
* absolute path -- /about/index.html  
* relative path -- about/index.html  
./about/index.html  

#### Page variable
`.TableOfContents`// built-in, outputs a <nav id="TableOfContents">  
`.Permalink`//adjust the directory paths  
`.RelPermalink`  
`Aliases`//create redirects to page from other URLs  
`Taxonomy`//Taxonomy(Actor); Term(Bruce); Value(Unbreakable)
`Configure Markup in yaml`//highlight;   tableOfContents  
`.PrevInSection `  `.NextInSection` //Points up to the previous and next regular page below the same top level section
```
markup:
  highlight:
    style: monokai
  tableOfContents:
    endLevel: 3
    ordered: false
    startLevel: 2
```
* A branch bundle---list page--template for many pages  
* A leaf bundle---single page---template for one page

Syntax Highlighting:  
[prism](https://prismjs.com/)  
[highlight.js](https://highlightjs.org/)
Hugo uses Chroma as its code highlighter.  
[Chroma Style Gallery](https://xyproto.github.io/splash/docs/all.html)  

### Hugo functions  

`.Param "image"(KEY)` //Calls page or site variables into template.  
`.GetPage PATH`//returns a page of a given path.  {{ with .Site.GetPage "/blog" }}{{ .Title }}{{ end }}  
`.Get INDEX/INDEX `  
`float INPUT`//{{ float "1.23" }} → 1.23 // turning strings into floating point numbers.  
`dict KEY VALUE` // passing more than one value to a partial template.//{{ partial "article-list" (dict "context" . "size") }}
`range COLLECTION`//to iterate  
examples:
```
{{ range $elem_index, $elem_val := $array }}
   {{ $elem_index }} -- {{ $elem_val }}
{{ end }}
```

```
{{ with .Param "description" }}
    {{ . }}
{{ else }}
    {{ .Summary }}
{{ end }}
```
Conditionals:  
```
{{ with .Param "description" }}
    {{ . }}
{{ else }}
    {{ .Summary }}
{{ end }}
```
```
{{ if (isset .Params "description") }}
    {{ index .Params "description" }}
{{ else }}
    {{ .Summary }}
{{ end }}
```


comments:
`{{/* {{ texts}} */}}`  
image:
`![description](image.jpg)`  
link:
`[link description](URL)`  
video:



## Adapting themes to my website
### Install themes
I found those hugo themes that I am interested in:
[PaperMod](https://themes.gohugo.io/themes/hugo-papermod/)
[Hugrid](https://themes.gohugo.io/themes/hugrid/)
[Tanka](https://themes.gohugo.io/themes/hugo-tanka/)
[Dopetrope](https://themes.gohugo.io/themes/hugo-theme-dopetrope/)
[Stack](https://themes.gohugo.io/themes/hugo-theme-stack/)

![PaperMod](PaperMod.png)
![Hugrid](Hugrid.png)
![Tanka](Tanka.png)
![Dopetrope](Dopetrope.png)
![Stack](Stack.png)

To install the themes:  
`mkdir themes`    // Creates a "themes" Folder in my root  
`cd themes`        // Move to the "themes" Folder  
`git clone https://github.com/CaiJimmy/hugo-theme-stack ` // download themes

At first, I chose the **Dopetrope** themes, then I found out there are many block in the layouts that I want to modify. After trying with the template for two days, I realized that as a newbie to web design that I am not capable of making big adjustments for my template yet. So I changed my themes template to **Stack**, which is designed for bloger, has many great features such as tags, and does not require many modifications.




### Adjust themes
The stack theme templates are well structured and organized, almost too complicated for me. It still took me quite some time to familiarize with this themes, but it felt really awsome that for the first time my contents dispaly like a real website.
![Stack example page](stack-page.JPG)  


#### Template modifications
I had many strggling moments while trying to adjust the layouts HTML. Still I managed to adjust a few things.

The fist problems I had was the path of single page. When push to gitlab repository, "digital-fabrication" would add to the path and show 404, even though it worked well on localhost. The problem is **relLangURL**, a function which adds the relative URL with correct language prefix according to site configuration for multilingual.  
To fix this:
in `layouts/partials/sidebar/left.html `  
replace `{{.URL|relLangURL }}` with `{{.URL }}`

**Adjust the left side bar**  
I kept most of the function from template, and added Final project page and Weekly assignment page.

**Add weekly assignments table**  
The second is that I want to replaces the archives block with weekly assignments titles in the weekly assignments page, similar to table of contents in post page. To do that, I modify the table of contents structure and added those codes:
```html
<h2 class="widget-title section-title">WEEKLY ASSIGNMENTS</h2>
{{ $pages := where .Site.RegularPages "Type" "in" "post" }}

<div class="widget-archive--list">
    {{range $pages}}
        <div class="post-title">
            <a href="{{ .RelPermalink }}">{{ .Params.week }} | {{ .Title -}}</a>
        </div>
    {{ end }}
</div>
```

**Add week infomation on title details:**  
I find that if I just add week infomation without conditions, "week" would appear on all the single page including the about page and final project page, and {{with}} solve this issure well.
```HTML
{{ with .Params.week -}}
<h3 class="article-week">
    Week {{ . }}
</h3>
{{ end }}
```

**Adjust the sticky back home button**  
During the writting of weekly assignments, I realized it is not very convenient to jump between posts. The template back button take me directly to home page. I still want to have a previous and next button so I dont need to go back and forth to switch posts. Also, I want the button stick to the same place so I can still switch during the reading. Here is the code I added use the `.PrevInSection` and `.NextInSection` functions:
```html
<div class="next-sticky">
    {{if .NextInSection}}
        <a href="{{ .NextInSection.Permalink }}" class="next-post">
            <span>Next</span>
            {{ (resources.Get "icons/next.svg").Content | safeHTML }}
        </a>
    {{ end }}
</div>
```
```html
<div class="prev-sticky">
    <a href="{{ .Site.BaseURL | relLangURL }}" class="back-home">
        {{ (resources.Get "icons/home.svg").Content | safeHTML }}
        <span>Home</span>
    </a>
    {{if .PrevInSection}}
        <a href="{{ .PrevInSection.Permalink }}" class="prev-post">
            {{ (resources.Get "icons/prev.svg").Content | safeHTML }}
            <span>Previous</span>
        </a>
    {{ end }}
</div>
```

#### Adjust color scheme
My favorite colors are orange and blue, being omplementary colors, they represent vivid and peace to me. So I want to adjust the color scheme of **orange** as main color with a warm style, and **blue** as supplementary color.

There is the overall style variables in `assets/scss/variables.scss`. I mostly adjust the background and text color, font size, hover and so on. For the style not in `variables.scss` , I add to the `custom.scss`.

I also dove into the partials folder `assets/scss/partials` to have more spefic adjustments such as align title dertails in the middle.

![Here is my page after the adjustments](my-page.JPG)  


#### markdown basics
In the first week when we were developing plain HTML websites, I was not aware that markdown basics were quite important as well . Luckily they are easy to pick up after reading through the those basics:  
[Markdown cheatsheet](https://www.markdownguide.org/cheat-sheet/)  
[Markdown cheatsheet in github](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

![Using Atom to edit my markdown files](Atom.png)

It is much easier to orgnize my documentation.

## Git setup
* Install [Git](https://gitforwindows.org/)
Remember select atom as the text editor when install.
* Configure Git
git config –-global user.name “YOUR_USERNAME”  
git config -–global user.email “jSmith@mail.com”
### [Use SSH keys to communicate with GitLab](https://docs.gitlab.com/ee/ssh/)
SSH allows secure, encrypted access to git repository, so no need to enter user/password every time I push.
* **generate SSH key**  
`ssh-keygen -t ed25519 -C "comment"`  
Accept the suggested filename and directory  
checking SSH key
* **add SSH keyto gitlab**  
public key-- can share with gitlab  
private key-- will be secret
in terminal:  
`cat ~/.ssh/id_ed25519.pub` | clip  
in gitlab:  
Avatar -- Preferences -- SSH Keys -- paste public key to key box --- type a description in title box (worklaptop) ---add key
* **clone repository to local**  
git clone 'repository path'


## Upload repository to GitLab
### Work-flow
`git status`  
`git Add` // stagging   
`git commit -m "descriptive commit message"` // Add flag for changes  
`git push` // sychonize with remote Gitlab repository  
refresh project in gitlab  
check pipe status ---passed
![using git to update my gitlab repository](git_push.PNG)

git log
git restore

### Rollback to the former version---git reset
`git rest` // soft reset--- only remove commit from log, still keep the file
`git reset --hard 'log ID' `  //  hard reset---all changes after that are gone

### Branch working and push conflicts
For group project with multiple developer, there maybe be conflicts when one developer try to push to GitLab.

* **branch working Git push**  
`git checkout -b wip` // create a new branch "wip"  
`git checkout master`  // get back to master branch  
`git log` // check the commit version  
`git reset --hard 'commit edition ID'` //get back to the version  
`git pull`// pull changes from GitLab (pull remote changes back to local)   
`git checkout wip` // go back to wip branch  
`git rebase master` // rewind all changes in wip branch on top of master branch  
`git checkout master`  
`git merge wip` //merge the wip branch to master  
`git push`  

`git branch `//show branch  
`git branch -d wip` //delete wip branch  

* **The same person working on the same repository:**  
1. Using a working branch, do changes in working branch, `git add` and `git commit`   
2. Switch to master branch, `git pull`, switch to working branch,`git rebase master`,back to master, git merge and push  

Other learning resources:  
[Dangit](https://dangitgit.com/)  
[Learn about Git branch](https://learngitbranching.js.org/)

### Git push to repository limitation
git push may fail when file were too big(10Mb limit)  
git add, commit and push one file everytime

If roll back to version before big file, and git push, maybe fail:  
`git push --force` // will delete all files after that version in history

Unprotect branches:  
GitLab: setting --repository --protected branches---unprotect

### Multiple remote repository
We can an infinate amount of repository  
`cd folder`  
`git init`  
`git remote add origin git@... `//add URL  
origin // _default of specify name of allias  
`git remote -v` // show existing remote   
each repository has two(fetch and push)shield from random push  

`git help remote`  
`git remote rm origin` // remove repository  
`git remote set-url origin git@... ` // if already have a set remote  


## Image and video optimizing
### Image optimizing
I mostly want to bulk resizing the images. [ImageMagick](https://imagemagick.org/index.php) seems quite convenient combining with Git.  
[Examples of Resize or Scaling](https://legacy.imagemagick.org/Usage/resize/)

**A few basic about resizing:**  
one dimension is given---> width  
to specify height ---> x200  
`widthxheight`	Maximum values of height and width given, aspect ratio preserved.  
`widthxheight^`	Minimum values of width and height given, aspect ratio preserved.  
to only downsize the image bigger than 500px `magick -resize 500\> *.jpg`  
to specify quality `magick -resize 500\> -quality 100 *.jpg`  

`$ magick *.jpg -resize 500 *.jpg`// set the width 500px, keep width/height ratio

What I want to achive is:
* resize to 500*500px
* preserve aspect ratio
* keep the shortest edge fit 500px
* crop the other edge to 500px

And with test, this works for me:  
`magick *.jpg -resize 500x500^ -gravity Center  -extent 500x500  *.jpg`

Although, I found the images were duplicated, but what I want is overwriting. And I discoverd command `magick mogrify`can do that.

I also found that for image smaller than 500px, it is generating one more copy
(I have 4 pictures that I want to resize, and I got 6 after resizing, 2 were repetitive).

Also some temperary file (suffix jpg~) were generated, and files become too much to manage. There are two ways to solve that:   
Method 1: To ignore the temp image file  
`echo "*.jpg~" >> .gitignore`  (add *.jpg~ to .gitignore file)
![Git ignore the temp image file](ignore_temp.png)

Method 2: remove temp file in the end.
` rm *.jpg~`

**Other tools notes：**

* screenshot  
I am using [Greenshot](https://getgreenshot.org/downloads/), and it is quite convenient.  
PrintScreen for area screenshot  
Alt + PrintScreen for the window screenshot.  

imbeded windows screenshot:  
windows key + shift +s
windows key + w

* Image Editing  
Raster: Photoshop, [Gimp](https://www.gimp.org/), [Krita](https://krita.org/en/).  
Vector: Adobe illustrator, [Inkscape](https://inkscape.org/), Adobe Indesign, [Scribus](https://www.scribus.net/).

* Other Image optimization:  
[IrfanView](https://www.irfanview.com/)  


### Video optimizing
To download [FFmpeg](https://www.ffmpeg.org/)  
`git clone https://git.ffmpeg.org/ffmpeg.git ffmpeg`

[H.264 Video Encoding Guide](https://trac.ffmpeg.org/wiki/Encode/H.264)  
Resize original.mp4 to 1280px width preserving aspect ratio.  
`ffmpeg -i original.mp4 -vf scale=1280:-1`  
Change frame rate to 24 fps with H.264 encoding, high profile.  
`-filter:v fps=24 -c:v libx264 -profile:v high \`  
Use slow encoding preset, use Constant Rate Factor 28, optimize for fast start.  
`-preset slow -crf 28 -movflags +faststart \`  
Use AAC audio codec with 160kbit/s bit rate and 44100 Hz sampling rate  
`-c:a aac -b:a 160k -ar 44100 optimized.mp4`  
trim the video by start and end time  
` ffmpeg -ss 00:01:00 -to 00:02:00  -i input.mp4 -c copy output.mp4`

Explanation of the command:  
-i: This specifies the input file. In that case, it is (input.mp4).  
-ss: Used with -i, this seeks in the input file (input.mp4) to position.  
00:01:00: This is the time your trimmed video will start with.  
-to: This specifies duration from start (00:01:40) to end (00:02:12).  
00:02:00: This is the time your trimmed video will end with.  
-c copy: This is an option to trim via stream copy. (NB: Very fast)  

**Other tools notes：**
* production:   
[OBS Studio](https://obsproject.com/) free open source video recording and live streaming  
[asciinema ](https://asciinema.org/)to capture some terminal sessions   
* video Editing:   
[Kdenlive](https://kdenlive.org/en/)
* Other video optimization:   
[HandBrake](https://handbrake.fr/) free and open-source transcoder for digital video files

## Wrap up
All in all, during this two weeks, I learned many knowlesges about HTML and CSS, learned how to use hugo and generate my own website for the first time. It was not an easy journey for someone who does not have any web developing experience, but I am quite satisfied with the results. Choose the suitable template can save lots of energy and time, but learning from the basics such as hugo variables and functions is the backbone which can greatly help with the understanding of website structure. Then just fumble things little by little in layout and in CSS, All in all, I learned a lot in this process and happy with my website!
