---
title: Computer-Aided Design
Week: 03
weight: 30
date:
slug:
image: array.png
categories:
    - modeling
    - rhino
---

## Goal for the week

* Learning about 2D vector and raster tools
* Learning about 3D modelling tools
* Develope my final project idea

## 2D vector and raster tools
I tried Gimp and found the functions are very similar to Photoshop, since I have been using Photoshop for a while that I would like to just continue Photoshop.
### Photoshop
I do not have something in my final project need Photoshop yet. For skeching I prefer draw on paper with pen, so I just use an reference picture first.
![Original reference picture](PS_original.png)

I want to change the background color, `W` (Magic Wand Tool) select background color, `Ctrl+U` adjust hue,saturation, and brightness.
![change the background color](PS_background.png)

Next I want to add filter to make the image more interesting. `Ctrl +J`copy image to a new layer,  Choose Filter > Filter Gallery. Here I chose the Glass filter, and adjust the parameter.
![Glass filter](PS_glass1.png)
![Glass filter with adjustment](PS_glass2.png)

I also want to turn the image into black and white outline. `Ctrl +J`copy image to a new layer,  Filter > Stylize > Find Edges. `Shift+Ctrl+U` desaturate, to remove all the color and make the layer grayscale.`Ctrl+L`open Levels and add contrast. Then `B` brush (color white) to clean the outlines.
![apply Find Edges filter and level adjust contrast](PS_outline1.png)
![After cleaning](PS_outline2.png)

* Other raster tools:  
procreate  
concept   

### Adobe illustrator
I use the same image to find the vector outline.
Insert the picture, use image tracing and select the proper mode. Then extend it and ungroup, select all vectors and remove fill, only keep stroke. I also adjusted some of the lines.
![raster picture in AI](AI_original.png)
![image tracing for raster pictures](AI_image_tracing.png)

![Adjust the strock](AI_strock.png)
![Prepare for laser cutting](AI_laser.png)

* Other vector tools:   
[svgator](https://www.svgator.com/)   
Inkscape

## 3D modeling tools
### Rhinoceros
With the background of architecture, modelling is not too hard for me. I am used to use Rhinoceros and grasshopper. And before I try out other modelling tools, I want to develope my project a bit further with the tool I am familiar with.

A few process of modelling:
![rebuild the surface](rebuild1.png)
![F10 for control points](rebuild2.png)
![drag control points to make wave surface](rebuild3.png)

![rotate lines](pipe1.png)
![pipe with radius 1.5mm](pipe2.png)

![rebuild curve and adjust control points](rebuild_curve.png)
![Array LED light](array.png)
![split surface](split.png)



### Blender
I know about blender for a while and I have been quite interested in it, especially it's sculpting ability, which is something Rhinoceros is not good at. And now it is the best time to try it!    
I also find [Blender Manual](https://docs.blender.org/manual/en/2.79/index.html) is quite useful.

#### Make a donut first
Before I can jump into my design, I need to learn some basics of blender. I want to follow [Blender Guru's tutorial](https://www.youtube.com/playlist?list=PLjEaoINr3zgFX8ZsChQVQsuDSjEqdWMAD) I found a few years ago  and make a donut, too!


**A few useful Hot key**  
Shift + scroll wheel --> rotate view  
Ctrl + scroll wheel --> fine zoom  
select object + `,`  --> zoom to object  

`G` -- move / `XYZ` for axis / hold scroll wheel --> move to nearest / `Alt G` --move to origin  
`R` -- rotate  
`S` -- scale  
`A` -- select all ; `Alt + left click` -- select edge  
`H` -- Hide; `Alt H` -- bring back  
`N` -- object panal

`1` -- front view;
`3` -- side view;
`7` -- top view;
`0` -- camera perspective view  

When change numbers, hold shift and drag -- subtle changes

**Object editting**  
I need to create a donut shape geometry. First, add a torus shape, change shade to smooth. Then edit the shape to look more like a real donut by adjust nodes with proportional editting.

![create a basic shape](donut_basic.png)
![proportional editting](donut_edit.png)

`Shift + A` --> Add, mesh, torus  
`F9` -- bring back the add panal once more  
resolution: can be low in the beginning (or it will be hard to edit with too many nodes)  
right click -- shade smooth (fake smooth)  

`Tab` --> edit  
`Shift + click` --> select multiple nodes  
`O` --> proportional editting (scroll up to small area)  
`Ctrl + tab` --> select edit mode  
`Z` --> select show mode  

In edit mode, can not do any other things than editting, needs to switch back to object mode first.

![Select random nodes](donut_random_select.png)
![Looks fun!](donut_random_select2.png)

**Modifier and Modelling**  
Then I need to add an icing layer. I need to use X-ray mode to see all nodes, so I can select the top half and duplicate them. After that, I need to solidify the object to add thickness. And adjust the modifier sequence, solidify first(add thickness), then subdivision(smooth)  
![Select the top half and duplicate](donut_create_icing.png)
![Add thickness of icing](donut_solidify.png)
`Alt Z` --> x-ray, to select all nodes(include hiding ones)  
`Shift D` --> Dduplicate  
`Esc` --> remain the same place  
`P` --> seperate duplicate to different layer  
`Ctrl + L` -- select linked (if missed seperation)  

Then similar to donut editing, I adjust the icing nodes around the edge to make icing runnier and more realistic. Also, remember click face in snapping, and check project to individual elements, so when I adjust drips, the icing can keep on the donut surface. When it is unable to adjust further, need to use `E` extrude more nodes.
![extrude icing drip.png](extrude_drip.png)

Then adjust the frying ring of donuts. `Alt + left click` to select edge, and scale. After scaling, icing is flying, needs to add modifier -- `Shrinkwrap` --select target -- move it to top of modifier.
![project flying icing.png](project_flying_icing.png)

**Brush sculpting**  
Now it looks like a good donut, except the drips looks a bit flat still. To make it "fatter", use `Inflate` brush in the sculpting tools, and change it to Air. Use `Grab` to stretch icing edge, and `Smooth` to smooth the surface.(smooth brush is usually big, needs to adjust strength first)    
![donurt_sculpting.png](donurt_sculpting.png)

`Ctrl + A `-- apply modifier first  
`F` -- adjust brush size  
`Shift + F` -- adjust brush strength  

**Rendering**  
Set the camera, find a view I like `ctrl+alt+0`(check *camera to view* in view panal to adjust the view. Or select camera frame, `G` to adjust object.

![My camera and lighting setup](render_setup.png)
`F12` -- Render

![view port shading to display render results](view_port_shading.png)


* **Eevee** -- real time render engine, needs to change something to make it looks real
  * light shadow -- bias -- reduce self shadowing
 can adjust contact shadow, too
  * render properties -- check ambient occlusion, screen space reflections

* **Cycles** -- calculate light bunces, looks real from start
if use cycles, remember to set GPU:  
preferences -- system --  CUDA or OptiX (better)

Add material  
  * base color diffuse
  * roughness
  * subsurface and radius
compare render results -- change slots than F12

![My donut rendering with Eevee](donut_render.png)

#### Render caustics in Blender
I find a few tutorial rendering caustics with blender seems quite interesting.    
[Fake Caustics in Blender](https://www.youtube.com/watch?v=X9YmJ0zGWHw&t=3s&ab_channel=Polyfjord),
[REAL Caustics](https://www.youtube.com/watch?v=GHv2U9AXbNA)    
Before, to render caustics, we have to fake it because there is no such function. But a great great news is that new version of blender has a very simple way to render real caustics now. This version is currently developing still, but I want to give it a try.

* First, go to [Blender Patch Builds](https://builder.blender.org/download/patch/) and download the version D3533.
* Change render engine to Cycles
* In object properties --> shading -->caustics   
select object as caustics caster and plane as caustics receiver
* Go to light, and check Caustics Light
![Glass monkey following the tutorial, looks promissing](glass_monkey.png)

![Light settings for mounted lamp](light_setting.png)   

I set two lights in the scene, one is to light up the environment and the other mimic the bulb in the lamp. The first rendering was wrong as different objects materials linked to each other. After adjust the materials, I got the second rendering results which did not show cautics that well. I tried to adjust the light intensity, the distance between objects, and thickness of glass, none works very well. But I will continue trying.  
![The first rending results](lamp_render1.png)
![The second rending results](lamp_render2.png)


### Open SCAD
I want to try Open SCAD because it has very nice libraries with bolts, electroniscs etc.

(to be continued)

### Houdini
(to be continued)
