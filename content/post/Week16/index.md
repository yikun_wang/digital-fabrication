---
title: Wild Card
Week: 16
weight: 160
description:
date:
slug:
image: cover.jpg
categories:
---


## Zünd cutter

### About Zünd
[Zünd](https://www.zund.com/en) is the high-precision digital cutting machine with modular tooling system.

![different size of zund machine](size_zund.png)

![modular tools ](modular.png)

![modular tool](modular_tool.png)
There is a vacuum under the cutting bed to ensure the material hold-down during the cutting process.

![different shape of blades](blades.jpg)
different shapes of blades and more blades stored in box.
![more blades](more_blades.jpg)

![registration marks in the corner](registration_mark.png)
To cut out the printed geometry, those **black dots** in the corner are needed, so the machine can **registrate** the cutting outline with the geometry.

![side difference](side_difference.jpg)
For KT board, the cutting side maybe not as clean as the back side, it is important to take this into consideration and decide which side will be on top.

![fabric](fabric.jpg)
Some fabric might not cut well too. Always run test before cut the real piece.

* How to use Zünd
![move_crease](move_crease.jpg)
1. Import Adobe illustrator file.
2. Edit in the new windows, select crease lines and rigth click, move them to `Crease`. Then select the outline and move to `Through Cut`. Close it and save. The file can also be found in cutqueue.

![Cutlit, software for Zünd cutter](Cutlit_1.PNG)
1. Tape the material to machine bed
2. `long press` a point on software canvas to move the cutting tool there.
3. `Arrow keys` to slowly move the cutting head  
`shift` + `Arrow keys`to fast move the cutting head
4. When the laser point is on the **east-south** corner, set XY origin
5. long press the north west edge of pattern to make sure the pattern is within the material edge, Otherwise, right click the pattern and edit and scale the pattern.
6. checking the creasing and cutting settings.   
7. start cutting


* Material
  - paper
  - cardboard
  - KT board
  - fabric
  - acrylic

### Design
My final project is about lighting design and I have made a lamp in laser cutting week, so I was thinking making a lamp cover. During the introduction of Zünd cutter, I realize Zünd can do creasing. So in the end, my idea is to make an origami lamp cover.
[origami](https://ladyblackdress.com/casi-origami/)
[Radical Soft Robot](https://eunyoungpark.co/portfolio-item/radical-soft-robot/)

![origami lamp by nellianna](origami_lamp.png)
![origami lamp by studio snowpuppe](studio_snowpuppe.png)

First I was trying to find the **folding patterns**. There are many examples online, and I found this example and build this pattern with **Grasshopper** so it can be parametric.
![reference pattern online](ESQUEMA.jpg)

{{< video "./origami.mp4">}}

The number of edge, the size, the location of points can be easily adjusted.

![GH export to AI](GH_AI.png)

### Testing material
* I bought a variety of material from Aaltopahvi to test:  
  * paper of different weight : 150g/ 175g / 200g
  * Thick tracing paper
  * semi-transparent platic (PVC or PP I am not sure)
  * cardboard

* different weight paper 150g/ 175g / 200g   
**Thin paper** can be cut nicely with little details like small holes, but the creasing can be tricky. I tried to reduce creasing weight, but a few creasing line still break.
![creasing tool tear the thin paper](crease_paper.png)  
**Thick paper (150g+)** creasing result is much better.
![crease of thick paper](crease_thick_paper.jpg)

![After folding](origami_1.jpg)

* Thick tracing paper (175g)  
The thick tracing paper is too brittle and creasing tool would rupture the paper. Even through I reduce creasing weight, most of the creasing line still tear apart.
![Thick tracing paper crease tear the papaer](crease_fail.jpg)

* semi-transparent platic  
This type of material can not be creased properly. I tried to **add weight** in both X and Y axis and creased repeatly from 2 to 4 (the limitation). But it just did not work out. The traces remain light and inpossible to fold.
![semi-transparent platic crease failed](crease_fail1.jpg)

So I was thinking, instead of using creasing tool, maybe I can use the universal blade to **lightly cut** the plastic but not cut through to do the folding crease. And it works！

When `Down position` is **0**, the blade cut through the material. When `Down position` is -0.3, the blade lift 0.3mm. I started with -0.8, and find **-0.3-0.4** get pretty good result for my material. Since it denpends on the material thickness, some test cuts are needed.

![blade_depth](blade_depth.png)

First, select the crease line and move them to `Through Cut`, set the `Down position` to **-0.3**, run the machine. Then right click the shape, select all lines and move them to `Not placed`, and select outline and move to `Through Cut`, change the `Down position` back to **0**, run the machine again to cut the outline.
![cut the crease](cut_crease1.PNG)
![cut through the outline](cut_crease2.PNG)

![cut the creasing line](PP_light_cut.jpg)
![folding result](PP_origami.jpg)

* Thick tracing retry
The platic was successful, so I was thinking maybe I can try this method again with the thick tracing paper.  
![Cut the crease](tracing_paper1.jpg)  
Somehow a few line disappeared, I think it is relate to the angle and direction of the crease line. When I try to fold it, again some creases tear apart due to the brittleness of material.
![crease tear again](tracing_paper2.jpg)  

* Thin cardboard
The cardboard just cut and crease nicely with the default settings.
![cardboard](cardboard.jpg)


![My testing material leftover](testing_material.jpg)

### Other Origami
![Draw lines in AI](lamp_AI.png)

![different origami pattern cutout](origami_2_cutout.jpg)

![origami pattern after folding](origami_2.jpg)

### Bunny
I was also thinking about making a mask. I find this [Japanese Fox Mask](https://sagara-works.jp/research-and-development/cardboard-work/komen/) really cool and I thought I could make something similar, but due to time limitation, in the end I decide to cut a bunny.
![Draw the bunny in Adobe illustrator](Bunny.png)

![bunny cutout](bunny_cutout.jpg)

![Bunny folded](Bunny_fold.jpg)





## WaterJet cutter

EDM machine  
vaccum former

## File
[pattern1](pattern1.ai)
[pattern2](pattern2.ai)  
[origami](origami.ai)  
[Bunny](Bunny.ai)  
[ORIGAMI Grasshopper](ORIGAMI.gh)
