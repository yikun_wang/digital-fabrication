---
title: Molding and casting
Week: 10
weight: 100
description:
date:
slug:
image: epoxy_result2.jpg
categories:
---

## Modeling
### single-sided
This week I want to try to cast the wavy plate for my final project. I have built the model ealier. My original design was a 250mm diameter double-sided wavy plate, but I want to make a test piece first.

![wavy plate](model.png)

At first I was planing to make a single-sided open cast mold.
![silicone resin model](silicone_resin_model.png)

![Foam silicone resin model](foam_model.png)

![machining model](machining_model.png)

Dimention: 160 * 160 * 34

### double-sided
After I made the open cast model, it is clearer for me how the machining model should be like, then I continued to make the double=sided model. I remembered that Neil was saying that the registration nodes are not as good as the fit registration, so I was trying to use that.
![](double_perspective.png)

![](double_side.png)

![](double_machining.png)
And this is how the model looks like with registration.

![](radius1.png)
![](radius2.png)
Then I realize the milling bits can not do sharp 90 degree corner, so I manually add the radius to the inside corner. Also the tolenrance occur to me that the inside registration rim shall be slightly smaller, so two mold can actually fit.

![](radius_model.png)

Since this is going to be a closed cast, the mold also need the infill hole. I prefer to place the infill hole on the side and cast side-standing. Then I realize there is another problem. The plate thickness varies and I did not place the thickest part at the infill hole place, which limit the infill hole size.

![](thickness_issue.png)

### Add mounting hole
![silicone new model](silicone_new_model.png)

![final machining model](final_machining_model.png)

## Cook my wax
Since the wax block size is a bit too small, I need to cook my own wax block.
![Melting the block](melt_wax1.jpg)
First start with **80 Celsius**, when there are some wax melt into liquid already, tune to **100 Celsius**. Stir gently from time to time to melt evenly.  
(As it is sticky and thick, **do not stir too much or too hard** and introduce too much bubbles)   

![check if there is any lumps left](melt_wax2.jpg)

![Pour the wax into the mold](melt_wax3.jpg)
**Heat the container a bit**, so it is not cold and solidify wax immediately.   
(Here I am just place it on stove for a few seconds, but I guess hot water bath or oven would be pretty good too).  
Pour the wax into the container **slowly and thinly** to remove the bubbles.


![Cool the wax slowly](wax_cooling.jpg)
I was using a towl and wood covering it so it can cool down slowly.


## 3D milling
**Measure** the dimention of block: [width, length, thickness]

[MDX-40A Milling Machine Specifications](http://www.ati.com.ph/products/roland.web/products1/mdx40a/mdx40a.specs.htm)

![MDX-40A Milling Machine Specification](specification.png)

> Feedrate = N * C * S  
N - number of flutes  
C - chip load (mm) (depend on the manufacturer)  
S - spindle speed (rpm)  
plunge rate(verticle movement) = 1/4 of feed rate  
step over  40%  

Here we choose **14000rpm**, a bit smaller than limit.

![Speed and feed guide values of 2-Flute UNIVERSAL Endmill](SF_guide.png)
Then we find the speed and feed guide values of our milling bits. Here we are using MDF as the reference since there is no rigid foam.

[Sorotec tools](https://www.sorotec.de/shop/Cutting-Tools/sorotec-tools/)  
[2-Flute UNIVERSAL Endmill](https://www.sorotec.de/shop/Cutting-Tools/sorotec-tools/2-flute-mills/END-Mill-UNI-299/)


### Clean up the surface
First, we need to remove the rough irregular surface.

Milling bits: **22mm butterfly mill**  
Select **collet** by shank size. Insert collet, use wrench to tighten it a bit (or screw a bit), so inserted milling bits can stay. Insert around 1cm of shank into collet, tighten with wrench again.

![install the 22mm butterfly mill](22mm_mill.jpg)
The cooked wax block was not flat, even the button curved a bit, and tape is not enough to fix it on the milling bed. So I was taped some wood block around the wax block to fix the wax, and it works quite well.

![set up wax block](set_up_wax.jpg)

In V carve, check **tool database**, calculate feeds and speeds
>14000 * 0.2 * 2 = 5600 (estimate 0.2 since there is no 22mm )  
plunge rate = 5600 * 0.25 = 1400 mm/min

However, in the Milling Machine Specifications, the maximun feedrate is **3000 mm/min**, 5600 mm/min is excessding the machine limitation. So here we are using 2500 mm/min instead.
![22mm tool data base](22tool.png)

Draw a rectangle over the block, and select **pocket toolpath**, specify the cleaning depth. After calculation, we found there is some leftover material in the corner. So offset the tool path a bit with **negative pocket allowance** so the corners will be cleaned up as well.
![negative pocket allowance to clean up corner](clean_calculation.png)

Save the toolpath select `Aalto Fablab MDX-40(mm)(*.nc)` for post processor.


**G54**  
**NC code**  
G54 coordinate system

set Z origin with sensor, set XY origin


Check **overwrite speed**, which would adjust speed based on the feed parameters  
Start slow (50% with foam) (harder material 10% ). If it goes well, add the speed to 100%  


![milling the top layer](top_milling.jpg)
Since my materail surface is not totally flat, I manually set the Z origin and tune it 2-3 times to make it just touch the top edge for first milling and not hurt the milling bits. Also make sure the XY origin is correct as the corner is round and hard to find exact origin immediately. And milling a few times with changing depth(like 2mm) to make sure the top uneven layer milled enough.

![milling the warped button](button_milling.jpg)
Since the button is also warped , I also need to mill the button to make it flat, thus stable for rough milling.

### Rough milling
thickness = original - cleaned up thickness  
Milling bits: 6mm  
- Import 3D model (stl file)  
file-import-3D model

![material setup](material_setup.png)
center the model
initial orientation  
0 depth below the top

- calculate rough machining toolpath  

- Select tool

>0.07 * 2 * 14000 = 1960  
1960 * 0.25 = 490

![6mm endmill tool setting](6tool.png)

**Machine allowance**: extra material on top, to leave the material for finishing, and get a cleaner cut

![](rough_toolpath.png)

![6mm endmill rough milling preview](6_endmill_preview.png)

![rough milling](rough_mill1.jpg)

![rough milling result](rough_mill2.jpg)

### Finshing milling
>0.045 * 2 * 14000 = 1260  
1960 * 0.25 = 315

![ballmill 3mm](ballmill3mm.png)
![endmill 3mm](endmill3mm.png)
I was following Kris' tutorial and use 3mm ball mill, but I did not like the radius on the edge since I will make a double sided plate, I want the silicone mold to be as fit as possible, so I changed to the 3mm endmill instead, and in the preview, the edge looks shaper.

{{< video "./milling_simulation.mp4">}}

![First finish milling](finish_mill1.jpg)
stepover 0.5mm

![finish milling result](finish_mill2.jpg)
I found that the milling results are not fine enough, so I change the stepover to **0.1mm**, and change the milling bits to **3mm ballmill**.

![Second finish milling](finish_mill3.jpg)

![Second finish milling results](finish_mill4.jpg)
The milling took very very long, but the result is pretty good this time. The lattice-like surface is probably due to the low mesh resolution.

## Casting
### Material in Aalto fablab
* milling  
  - rigid foam (for practice)  
  - sikablock
  - freeman wax (wax+plastic)
  - wood

* silicone
  - Moldstar
  ![Moldstar](Moldstar.jpg)
  - Dragon skin
  ![Dragon skin](Dragon_skin.jpg)
  Food class silicone
  - Sorta-clear
  ![Sorta-clear](Sorta-clear.jpg)

Releasing agents for silicone rubber molds:   
only needed when plan to make many cast out of the same mold (like more than 20). If you don't use the release, the silicone absorb a little bit of the casting material, cast after cast, until it looses its "unsticky" property. so you use mold release as a protective film

* cast material
  - polyurethane
  - epoxy

### Cast Process
#### Cast silicone mold
![silicone material I am using](silicone1.jpg)

![](Dragon_skin2.jpg)
Fill the wax mold with the water to get the rough silicone amount. Here I need about 135g of silicone in total.

mix both A and B part well in bottle first, then measure same amount of **part A and part B (1:1)**, by weight or volumn, then add thinner (no more than **10%**) to helping mixing.
> For my mold I need : 65g A + 65g B + 10g thinner

After mixing well, put the silicone cup into the vaccumn chamber. Vaccumn till the pressure reach the red line. turn off vaccumn and slowly let air in through valve. Repeat this three times.
![vaccumn silicone to remove bubbles](vaccumn_silicone.jpg)

![pour the silicone into wax mold](pour_into_mold.jpg)

![silicone mold result](silicone_mold.jpg)

#### Cast Epoxy
![fill water to estimate the epoxy needed](epoxy_water.jpg)

![measure exoxy, mix ratio: 100A:30B by weight](measure_exoxy.jpg)
Measure both A and B into cup and mix it well. **mix ratio: 100A:30B by weight.**   
It is very liquidy but to get best result, I still vaccumned it once.
> For my model I need: 17g A + 5g B (22g in total)

![cast epoxy into silicone](cast_epoxy.jpg)
There were a bit bubbles on top (the white ones), but they slowly disappear as it cure.

![Cast reault under sun](epoxy_result.jpg)
What I find out is that it took more than 24 hours to cure. It was still soft after 24 hours, but 48 hours it become quite hard.
Still, Kris mentioned that the ratio of **part A and B shall be precise**.

## Warp up
So all in all, there are a few things to mind for this process:  
- Set **higher resolution** for the model.
- Set up wood block around wax block seems work very well.
- For finish milling, use **endmill** to get the sharp edge for the button if it is going to be two pieces mold. Then use **ballmill** to finish mill the second time.
- 0.5mm step over end mill is not fine enough, but 0.1mm took too long, maybe ball mill will get a better results with higher stepover.
- Measure **part A and part B** precisely.

## File
[Wax milling file](wax_milling.stl)
