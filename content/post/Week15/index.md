---
title:  Interface and Application programming
Week: 15
weight: 150
description:
date:
slug:
image:
categories:
---


## Tkinter
[Installing Tk on Windows](https://tkdocs.com/tutorial/install.html#installwin)

For my final project interface, I am thinking that I could have an interface for the cymatic water lamp project. People can choose the **frequency** and that would input into the system and change the frequency of amplifier. I have never try Tkinter before so I start with following Kris's tutorial.

* Pack layout
  * Use the `columnconfigure()` and `rowconfigure()` methods to specify the weight of a column and a row of a grid.
  * Use `grid()` method to position a widget on a grid.
  * Use `sticky` option to align the position of the widget on a cell and define how the widget will be stretched.
  * Use `ipadx`, `ipady` and `padx`, `pady` to add internal and external paddings.

![](tk_1.png)
First I create some button with different frequency range. Then I was thinking if the button could have color.


![](tk_2.png)
>lbl4 = tkinter.Label(Window, width=10, height=5, bg='SteelBlue2')  
lbl4.grid(column=0, row=1, sticky=tkinter.W)  
btn4 = tkinter.Button(Window, text='200 HZ')  
btn4.grid(column=0, row=1)   

> btn5 = tkinter.Button(Window, text='250 HZ', bg='SteelBlue2')  
btn5.grid(column=1, row=1)   

![](tk_3.png)
![](tk_3_1.png)
* columnspan > how many column include (width)


```
from asyncio.log import logger
import tkinter
from tkinter import *
from tkinter import ttk
import serial

window = tkinter.Tk()
window.title('Select Frequency')

mainframe = ttk.Frame(window, padding="20")
mainframe.grid(column=1, row=1, sticky =(N, W, E, S))

mainframe.columnconfigure(0, pad=10)        
mainframe.columnconfigure(1, pad=10)
mainframe.columnconfigure(2, pad=10)
mainframe.rowconfigure(0, pad=10)
mainframe.rowconfigure(1, pad=10)
mainframe.rowconfigure(2, pad=50)

lbl1 = tkinter.Label(mainframe, text="Select the Frequency:", width=20, height=1)
lbl1.grid(column=0, row=0, columnspan=2, sticky=tkinter.W)

btn1 = tkinter.Button(mainframe, text="50 HZ", width=8, height=3, bg='#8FD9CB')
btn1.grid(column=0, row=1)

btn2 = tkinter.Button(mainframe, text="100 HZ", width=8, height=3, bg='#ABBF63')
btn2.grid(column=1, row=1)

btn3 = tkinter.Button(mainframe, text='150 HZ', width=8, height=3, bg='#F2B872')
btn3.grid(column=2, row=1)

btn4 = tkinter.Button(mainframe, text='200 HZ', width=8, height=3, bg='#D2D9D8')
btn4.grid(column=0, row=2)

btn5 = tkinter.Button(mainframe, text='250 HZ', width=8, height=3, bg='#E78E84')
btn5.grid(column=1, row=2)

btn6 = tkinter.Button(mainframe, text='300 HZ',width=8, height=3, bg='#F28705')
btn6.grid(column=2, row=2)

logger = Text(mainframe,width=40, height=3)
logger.grid(column=0, row=3, columnspan=3)

mainframe.mainloop()
```
![](tk_4.png)
I want to use as input, but I have not figure out how to link with TouchDesigner yet.


I also find [CustomTkinter](https://github.com/TomSchimansky/CustomTkinter) that has an python UI-library and can do customizable widgets.  
To install: `pip3 install customtkinter`

![](pip_install.png)

However, when I run the py file it shows:
>ModuleNotFoundError: No module named 'customtkinter'  
$ customtkinter --version  
bash: customtkinter: command not found  

Then I find that `pip install ...` drops scripts into `~/.local/bin` and I need to add `pip install` folder into Path.


[Tkinter-Designer](https://github.com/ParthJadhav/Tkinter-Designer/)

## Processing
[Download processing](https://processing.org/)

I have tried to use processing a few times before and in the input week I tried to connect serial port with processing so the sensor value would be more intuitive. Processing can access the computer’s serial ports and get the value.

And I was following this [tutorial](https://learn.sparkfun.com/tutorials/connecting-arduino-to-processing/all) to learn how to connect Arduino to Processing.

* Read the Data in Processing  
`import processing.serial.*;` // To use the serial library,

* Graph the Sensor Value
```
import processing.serial.*;

Serial myPort;
Float sensorValue = 0.0; // serial variable
int mul = 15;

void setup() {
  size(1024, 200);// (1024=sensor max. value)
  myPort = new Serial(this, "COM12", 115200); // serial port
  background(255);
}

void draw() {  //draw function loops

  noStroke();
  fill(139, 189, 210,100); //sensor block
  rect(0, 0, sensorValue*mul, height); //position and size

  fill(255,70); //empty block
  rect(sensorValue*mul, 0, width-sensorValue*mul, height);

  println(sensorValue); //text
  fill(218, 181, 88);
  text(sensorValue + " " , sensorValue*mul, height/2);
  textSize(35);

}

void serialEvent(Serial myPort) { // sketch read the serial data
  String inString = myPort.readStringUntil('\n');
  if (inString != null) {
    inString = trim(inString);
    float[] values = float(split(inString, ","));
    if (values.length >=1) {
      sensorValue = values[0]; //first value in the list
    }
  }
}
```

{{< video "./temperature.mp4">}}

This [link](https://www.arduino.cc/education/visualization-with-arduino-and-processing) also introduced some examples of visualization with Arduino and Processing.



## P5.js
I am not a pro of coding, and I know I can not make stunning code in such a short time. But I think I can try to modify the existing code. So I was looking through project at [open processing](https://openprocessing.org/).

I saw this project [Pixel Worm Hole](https://openprocessing.org/sketch/874113) by Che-Yu Wu that I quite like. The simplest way is to try to use it as background. Then I find Wu is using [p5.js](https://p5js.org/), which is a JavaScript library that follows the idea of “sketchbook for code”.

Then I was trying to figure out how to connet p5.js with serial port. And I find [this](https://medium.com/@yyyyyyyuan/tutorial-serial-communication-with-arduino-and-p5-js-cd39b3ac10ce) and [this](https://itp.nyu.edu/physcomp/labs/labs-serial-communication/lab-serial-input-to-the-p5-js-ide/) tutorial talk about the serial communication with Arduino and p5.js.

The P5.js serialport library can’t access serial ports directly, because the browser doesn’t have direct access to the serial port. But it can communicate with another program  that can exchange data with the serialport (such as Processing)

To do that, we need to use the [p5.serialport library](https://github.com/p5-serial/p5.serialport/blob/master/lib/p5.serialport.js) and the [p5.serialcontrol app](https://github.com/p5-serial/p5.serialcontrol/releases).


[p5.js web editor](https://editor.p5js.org/)

```
let serial; // variable to hold an instance of the serialport library

function setup() {
  serial = new p5.SerialPort(); // make a new instance of the serialport library
  serial.on('list', printList); // set a callback function for the serialport list event

  serial.list(); // list the serial ports
}

// get the list of ports:
function printList(portList) {
  // portList is an array of serial port names
  for (var i = 0; i < portList.length; i++) {
    // Display the list the console:
    console.log(i + portList[i]);
  }
}
```

## Python
![checking my python version](checking_python.png)

[pySerial’s documentation](https://pyserial.readthedocs.io/en/latest/)

`pip install pyserial`

[Arduino and Python](https://playground.arduino.cc/Interfacing/Python/)

I was testing with code of [this](https://makersportal.com/blog/2018/2/25/python-datalogger-reading-the-serial-output-from-arduino-to-analyze-data-using-pyserial) website.

{{< video "./python_test.mp4">}}

But I have not get any further yet with graphing the sensor value.

## Grasshopper
[firefly](https://www.food4rhino.com/en/app/firefly)

## TouchDesigner
[TouchDesigner](https://derivative.ca/)

## For my project
Audio Visualizer
[1](https://openprocessing.org/sketch/768837)
[2](https://openprocessing.org/sketch/755040)
[3](https://openprocessing.org/sketch/485156)
[4](https://openprocessing.org/sketch/1415424)
[5](https://openprocessing.org/sketch/600380)
