---
title: 3D Scanning and Printing
Week: 06
weight: 60
description:
date:
slug:
image: second_print.jpg
categories:
- 3D print
- rhino
- modeling
- parametric
---

## 3D Scanning
### Artec Leo
![Artec Leo](Leo.jpg)
Artec Leo can scan medium to big object, it is wireless with 0.2mm resolution.

Here I am scanning a cup I made last year, just curious about how the complicated glaze texture would map in the scanning.

To start scanning, press `New project` in the biutton of screen, point at the cup, find the angle till the sceen shows "ready to scan", and press the red button on handle to start scanning.

I scaned with three positions:  
![Position one](pos1.jpg)
![Position two](pos2.jpg)
![Position three](pos3.jpg)

Slowly move around the cup, move Leo up and down in order to scan all corner. the green suggest the sampling is okay and red means it need more sampling. Rotate the cup on Leo screen and check if it is okay.  
{{< video "./scan.mp4">}}

What I find hard is the **cup inside button**, because I checked `BASE REMOVAL` The inside of the cup is hard to scan, also probably because it is shiny and white?

### Processing
In Artec Studio
* **Import**
![Import scans from Leo](import2.PNG)
![find the object and import](import1.PNG)

Make sure the computer and Leo are **under the same wifi**, find the **IP address** of Leo in [setting - WIFI]. and add the IP address in Artec Studio in order to connect with Leo.

Then find the project and import.

![imported project](import_project.PNG)

* **Clean the little pieces around the object**  
Editor -> erasor -> lasso selection   
Ctrl + drag mouse to select the unwanted fragments -> Erase

* **Alignment**
![Auto alignment](auto_alignment.PNG)
Use the auto alignment to align the scans.

* **Registration**  
After cleaning, we can run the registration. I try both global registration and rough serial registration, it seems for the cup, **the global registration** get a better result.
![global registration](global_registration.PNG)
![rough serial registration](rough_serial_registration.PNG)

* **Fusion**
![fast fusion](fast_fusion.PNG)
![smooth fusion](smooth_fusion.PNG)

At this stage, I find that I am not happy with the any of fusion result. Even though I run the `outlier removal`, there were still a few surfaces just flying and the quality were not good. I felt a bit frustrated, and wanted to try to scan again. But when I look at the scans on Leo screen, it just looks fine. So I decided to **run the processing** again.  

This time, after the alignment, I did not run the `global registration`, because the result was not good. Instead, I tried `outlier removal` and `fast fusion` directly, and this is the result:  
![Second Processing](2nd_run.PNG)

It looks much better, at least the exterior. Then run the `small-object filter` and `hole-filling`. We can also manually fix holes:   
![fix holes](fix_holes.PNG)

* **Clean and Smooth**  
I found that there are a few small surfaces fused and flying inside the cup. I try to remove them by using the erasor, with 3D selection tool to select those fused surfaces.
![manually clean](manually_clean.PNG)

After carefully remove those excessive surfaces, fix holes again. However, the inside surface quality is still quite rough, with some overlapping and bumpy surfaces.
![overlap surfaces](overlap.PNG)

Then I find this handy tool `Defeature brush` which can magically flat the overlapping surfaces and make it smooth. This tool saved me from a lot of pain.
![Defeature brush](defeature_brush.PNG)

Then I manually smoothed the surfaces a bit more with `smooth brush`. Although, I used the minimal strength, the brush still a bit strong, so I need to be careful to not over do it.  
![smooth brush](smooth_brush.PNG)

`Smoothing` in postprocessing:  
![before smooth](before_smooth.PNG)
![after smooth](after_smooth.PNG)

* **Map texture**   
![map texture](Texture.PNG)
Select one or some texture source to map the texture, and apply. Here is the result that I am quite happy with:  
{{< video "./scan_cup.mp4">}}

### Reflection
I tried a few object: my cow purse, a little sculpture, and the cup. The sculpture was too tiny and hard to catch, the purse is too soft and change shape when I rotate, and the cup I also had button problem.
![bag scan](bag_scan.jpg)
Also the processing part took some practise to find the best solution and tools.

Just try many times and find the best result.

## 3D Printing
A method of addictive manufacturing  
FDM( fused deposition modeling)

* **Material:**  
PLA(plant-based)  
ABS  

* **Design rule:**  
  * how close the gap between object can be so they won't stick together.(0.3mm)
  * Over hang angle: need support over 60 degree
  * Over hang distance:
  * bridging: over 20mm
  * how thin a layer can be / wall thickness:
  high resolution
  * infill percentage

* **More tips:**  
  - Mind the printing direction  
  - Avoid sharp corner(won't be printed)  
  - bed adhesion  
the bed is clean, no need for adhesive  
Adhesion is also material dependent. ABS does not play nicely for instance  

* **machine in Fablab:**  
[Lulzbot](https://www.lulzbot.com/)  
[Ultimaker](https://ericklarenbeek.com/)  
[Formlab](https://formlabs.com/)  
stereolitography: traveling a beam of UV light over a photosensitive pool of liquid

* **Other 3D printing technique**
  - LOM (laminated object manufacturing)
  cut and glue thousands of sheets of material together
  - 3D metal printing
  - 3D glass printing  
  The fine glass powder is glued and then sintered.


* **Some interesting applications:**
[3D Printing Records](https://www.instructables.com/3D-printing-records-for-a-Fisher-Price-toy-record-/)
[Printed Optics](https://www.youtube.com/watch?v=eTeXTbXA6-Y&ab_channel=DisneyResearchHub)
3D print that change shape after being exposed to heat [How can 4D printing change the furniture industry](https://www.youtube.com/watch?v=-Ily_kMGZeA&ab_channel=TrendforceOne)  

[Kinetic Smart Facade Prototype with Arduino](https://www.youtube.com/watch?v=PgFYAwe6JvI&ab_channel=ChristianLadefoged)

### Inspiration
I have been using ultimaker for a while and I am quite familiar with it, so what I want to try here is something I have not tried before.  
- **Complicated structure**
![flora ring of Nervous System](1.jpg)
![Soft Fold by Daniel Widrig Studio](3.jpg)
![The Monomer Jewelry Collection by Thomas J. Mroken](4.jpg)
I have been wanted to try those complicated structures especially porous structure. I want to use the Formlab resin printer for this kind of structure.

- **Flexible 3D print**  
[DefeXtiles: 3D Printing Quasi-Woven Fabric via Under-Extrusion](https://www.youtube.com/watch?v=_o7Duvtbd5M&ab_channel=ACMSIGCHI)  
![3D printed stretchy mesh, with customized patterns designed to be flexible yet strong, for use in ankle and knee braces](MIT-Printed-Mesh.jpg)  
[3D printing fabric](https://class.textile-academy.org/2021/sara.alvarez/assignments/week07/)   
Designing a block/shape in Rhino + import to Cura + change Cura settings to obtain a fabric like structure + print    

- **Double material**

### Formlab
![](formlab.jpg)
Two resin printer in aalto fablab:  
* white resin - KnowingPrairie - white V4   
* tough 1500 resin - MatureWrasse - tough 1500 V1

* **Design**  
![basic shape](basic_shape.png)

![basic shape definition](basic_shape_definition.png)   
I am using grasshopper for the shape generating. First generate a basic shape, then use voronoi to have the intricate structure. Finally, use weave bird to add thickness and smooth it out.
![complex_shape](complex_shape.png)

![complex shape definition](complex_shape_definition.png)

* **In preform**

![job setup](job_setup.png)
![select printer](select_printer.png)   
In the job set up, select the printer. Here I am using white resin, so I chose **KnowingPrairie**, material choose **White V4**.

![generate support](generate_support.png)
Then in support, **auto-generate support** and since I want the support to be easy removable, change the **Touchpoint Size** to 0.35-0.4. I can also manually edit the support.   

![print](print.png)
Upload the job to printer, open cartridge vent and check the print bed is clean, then start printing.

* **Post processing**  

First, wear the glove. Close the cartridge vent, lift the cover and remove the printing bed, and place it on the support.
![close the cartridge vent](cartridge_vent.jpg)
![lift the cover](open_cover.jpg)

![lift the lock and slide the resin bed out](bed_removal.jpg)
![Place the resin bed on support](bed_support.jpg)   

![move the printed object to_sieve](move_to_sieve.jpg)   

Soak the object in left isopropyl alcohol tank, move up and down a bit to wash thoroghly. Set a timer for 10 min.
![First Isopropyl soak](Isopropyl_wash1.jpg)
![Set timer fot 10 min](timer.jpg)    
Move the sieve to the second right isopropyl alcohol tank and soak another 10 min
![second Isopropyl soak](Isopropyl_wash2.jpg)  

Then use a tweezer to move the object to UV machine and expose it at 60℃ for 30min.  
![move ro UV machine](UV.jpg)   

![UV exposure](UVing.jpg)

![Object result](first_print0.jpg)

Drip isopropyl alcohol on print bed and clean the surface with paper towl. Repeat this process **three times**, and put the print bed back and close the cover. Use isopropyl alcohol to clean the used tools and set table again.
![wipe with isopropyl alcohol](clean_bed.jpg)
![put the print bed back](put_back.jpg)

![Clean all the used tools and set table again](clean.jpg)

![First print](first_print.jpg)
This is the first print, and I found it is very brittle. It broke when I was trying to remove the support. I realize the structure is too thin, so I add a bit thickness as well as adjust the touch point so it is easier to remove.

![Second print](second_print.jpg)
I modify the shape a bit and added a bit thickness. It is much better now.

### Ultimaker
* Flexible 3D print
![flexible model in grasshopper](flexible_model.png)

![flexible print](flexible.jpg)
I try to follow the MIT stretchy mesh design, but it didnt turn out as I hoped. It is still quite stiff.  

* Chains
![chain_design](chain_design.JPG)

* Chess
