---
title: Applications & Implications
Week: 17
weight: 170
description:
date:
slug:
image:
categories:
---


## Cymatics Lamp project
* What will it do?  
The original idea I had is just trying to explore the RGB LED and caustic effect to create **an light dancing atmospheric lamp**. Since Matti introduced me cymatics which translate sound waves into visual and tactical phenomena, I have been heading to that direction and incorporate music and frequency into the design. So in other words, it is an fun experimental design combines visual and acoustic aspects. And most importantly, allows users to **explore the cymatics phenomena and have fun**.
![cymatics picture from bassamfellows](BassamFellows-Cymatics.jpg)

* Who's done what beforehand?  
There are many project play with light, or cymatics itself, such as this:
[CYMATICS SERIES, NEON LAMP by HITENCHO](http://fracas-online.squarespace.com/lighting/cymatics-series-neon-lamp-by-hitencho)  
Or combining light with audio：[Cymatics Lighting](https://www.cymaspace.org/news/cymatic-lighting-system-press-release/)  
And of course cymatics with music:[CYMATICS: Science Vs. Music - Nigel Stanford](https://www.youtube.com/watch?v=Q3oItpVa9fs&ab_channel=NigelJohnStanford)  
But I have not find something exact same as my project.


* What will you design?
  - A lamp base that hold all electronics
  - A touch pad that can select frequency
  - a water plate that could show cymatics pattern
  - an origami lamp cover
  - PCB designed for the project

* 2D and 3D design  
**The lamp base and controller box** will be designed and modelled in Rhinoceros and Grasshopper.    
**The touch pads** are designed and processed in Rhinoceros and Adobe illustrator.   
**The origami lamp cover** is designed in Adobe illustrator.

![](type3.png)

![](type3-1.png)

* Additive and subtractive fabrication processes    
Additive fabrication processes: 3D printing  
Subtractive fabrication processes: milling machine, laser cutting  

* Electronics design and production  
My PCB board needs to have the following functions:  
  - **I2C communication** with touch controller  
  - Read signal from **PIR sensor**  
  - Send signal to **amplifier**  
  - Power and program the **Neopixel strips**  

* Microcontroller interfacing and programming  
I mostly use Arduino IDE to program the board.

* System integration and packaging.  
Since I have different parts that I need to incorporate, I need to figure the location of different parts and the connection.


* What materials and components will be used?  
  - Lamp base  
    PLA filament, magnet, DC Power Jack
    ![base model Version2](base_model_V2.png)
    ![base model Version3](base_model_V3.png)
  - Touch pad  
    PLA filament, ITO, acrylic, screws, trill, copper
    ![copper pad cutout](copper_pad_cut.jpg)  
    ![copper on ITO pad](ITO_copper.jpg)
  - Water plate  
    PLA filament     
  - Origami lamp cover  
    semi-translucent PET, metal wire
    ![](lamp_cover_test.jpg)
  - PCB designed for the project  
    double sided FR2
    ![designed PCB board](PCB_board.png)

* Where will come from?  
Adafruit, DigiKey, Bela

* How much will they cost?  
What parts and systems will be made?
What processes will be used?
What questions need to be answered?
How will it be evaluated?


## Bill Of Materials
![](Bill_Of_Materials1.png)

![Bill Of Materials](Bill_Of_Materials2.png)

Here is a also [Link](https://docs.google.com/spreadsheets/d/1bsjvE2zsdeR58Kc-XcFlqDamVPfTUuSSLueRFOQvqOI/edit?usp=sharing) of details of the Bill Of Materials.
