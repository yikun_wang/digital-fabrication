---
title: Principles and Practices
week: 01
weight: 10
description: Learning about Bash, HTML basic and CSS
date:
slug:
image:
categories:
    - Git
    - HTML
    - CSS
---

## Goal for the week
* Studying HTML basics
* Studying css
* Git setup
* build a simple website with html and CSS
* Plan and sketch a potential final project


## Shell Basics  
I am quite new to terminal. I was a bit lost in the beginning, but soon I caught up by watching tutorials and able to run some simple command.
Here is a few notes of my shell command learning:
`rm ‘filename’`// delete file  
`mv ‘filename’ ‘folder/filename’`  
`mv ‘filename’ ‘filename’` // rename  
`cp`// copy file  

`cd`// change directory  


`ls` // list files  
`ls -la` & `ls -a` // list all files (include the hidden ones)  
`ls id_*`//filter the list items  

`nano` // edit the file  
`cat` // print file  
`less` // read files in scrollable mode    

`touch`// create new files  
`mkdir` // create new directory  
`which` `whereis`// find file location
`find`
`tree ./` // check out the structure

Ctrl+A -- cursor to beginning  
Ctrl+E -- cursor to end  
Ctrl+L = `clear`  
Tab -- auto fill

### bash loop
`touch script.sh`
`nano script.sh`


![Avoid script file in loop](script_sh.png)
```
#!/bin/bash

echo "HELLO!"

FILES=$(ls *.jpg)
EXT="123"

echo ${FILES}

for F in ${FILES}; do
  echo "Renaming File $F to $F${EXT}"
  cp "${F}" "${F}${EXT}"
done
```
![Script in Bash](script.png)
![Results](Cmder_loop.png)

## HTML basics
Building a website is quite new to me. But the HTML Basics tutorial by Kris was quite detailed and shows step by step how to create a plain website with basic tags.

### understanding basic tags   
Here is my notes of what I learned about HTML：

{{< highlight html >}}
Heading: <!--h1 h2 h3 h4 h5 h6-->
<h1>title</h1>

Paragraph: <p>Text</p>   
<b>bold</b>, <strong>bold text</strong>, <i>italicized</i>, <u>underlined</u>

image:
<img src="image.jpg"> <!--src:Specifies the path to the image-->

lists:
<ol> <!--organized Lists-->
    <li>Element one</li>
    <li>Element two</li>
</ol>

<ul> <!-unorganized Lists>
    <li>First</li>
    <li>Second</li>
</ul>

tables:   <!--rows:<tr> ; columns:<td>-->
<table>
    <tr>
        <td>This column 1</td>
        <td>This column 2</td>
    </tr>
</table>

Hyperlinks and the Menu:
<a href="index.html">index</a>

{{< /highlight >}}


#### Other tags I learned from other material:
{{< highlight html >}}
<br>
<div class="about">... </div> <!-- container for other HTML elements.-->
<span>Hello World</span>
<pre>
{{< /highlight >}}


### building structure for the website
```html
<!doctype html>
<html>
<head>
  <title>Yikun Fab Academy</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
</head>

<body>
  <h1>title/<h1>
  <p>Text</p>
  <img src="Me.jpg" width="100%">
</body>

</html>
```
**HTML Layout Elements**  
`<header>` - Defines a header for a document or a section  
`<nav>` - Defines a set of navigation links  
`<section>` - Defines a section in a document  
`<article>` - Defines an independent, self-contained content  
`<aside>` - Defines content aside from the content (like a sidebar)  
`<footer>` - Defines a footer for a document or a section  
`<details>` - Defines additional details that the user can open and close on demand  
`<summary>` - Defines a heading for the <details> element  

From this part, I am capable of building the most basic documentation website.

### My HTML website
![My HTML code in VS Code](html.png)


### Find my text editor
I tried three different text editor:
* Atom
* Visual Studio code
* Bracket

They all have their benefits:

**Bracket** -- Allows live preview for HTML and changing color in a very easy way.  
**VS code** -- Has great extension features.  
**Atom** -- I chosed Atom in the end and installed color-picker and atom-html-preview package as they are quite useful!


## CSS Studying and modify my website
### CSS basics
For CSS learning, I started with the [HTML CSS Lecture of Fab Academy 2015](https://www.youtube.com/watch?v=PeWIU7UQ_rc&ab_channel=openp2pdesign)  
It is a great lecture, I tried as much as possible what Massimo introduced to modify my website.  

* Id---> **#**  
  class ---> **.**

* Link CSS with HTML:  
**Inline** - by using the style attribute inside HTML elements  
**Internal** - by using a <style> element in the <head> section  
**External** (common) - by using a <link> element to link to an external CSS file>  
In  `<head>` section add `<link rel="stylesheet" type="text/css" href="index.css">`to link the CSS file.

* Font---[Google external font](https://fonts.google.com/)    
Add fond link to `<head>` of html  
copy css rules to CSS file  
font-size: 18px  /  80%  
font-weight: 500  
change specific text ---> add id  

* [Flex](https://www.w3schools.com/css/css3_flexbox_container.asp)  
`flex-direction` : column, column-reverse, row; row-reverse;  
`flex-wrap`: wrap, nowrap, wrap-reverse;  
`flex-flow`:(both flex-direction & wrap properties.) row wrap;  
`justify-content`(horizontally): center, flex-start, flex-end, space-around; space-between;  
`align-items`(vertically): center, flex-start; flex-end; stretch; baseline  
`align-content`:stretch; center; flex-start, flex-end, space-around;space-between;

`<body>` -- parent ---all changes  
`<p>` -- children ---- local changes
HTML style can overwrite CSS  


### using CSS for my HTML
```
body {
  color: #555555;
  font-family: 'Work Sans', sans-serif;
  font-weight: 400;
  font-size: 19px
}

a
 { color: #5894db;}

a:hover{
  text-decoration: inherit;
}

#Welcome {
  color: #c78300;
  font-size: 25px
}

#title{
 color: #c78300;
 background: #ffffff;
 width: 600px;
 font-size: 50px
}

h1{
  color: #c78300;
  background: #dedede;
  width: 300px
}

h2{
  color: #c78300;
}

#motto{  
  color: #c78300
}

#Caustics {
  color: #989898;
  font-size: 15px
}

figcaption {
  color: #787878;
  font-size: 15px
}

.contact {
  border: 2px;
  border-style: dotted;
  width: 20px;
  height: 20px;
  color: #5894db;
  padding: 20px;
  margin: 10px;
}

.About strong {
  color: #c78300
}
```




## Wrap up and Showcase
I found a few things during the building of my website. The first is that in editor, paragraph texts can be quite long. It is very unconvenient to drag very far just to change one word. The second is that when I want to show some of my previous works with the same structure, I have to repeat it five times. Besides, every little changes on the structure needs to repeat five times as well, which make it hard to modify.

Here is a picture of my website built with HTML and CSS. It is a very basic documentation website. Now look back, I would like to also try some advanced CSS features such as flex and grid, to build a nice layout so the contents display nicely.

![My First simple website with style](first_site.JPG)
