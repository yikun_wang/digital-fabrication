---
title: Computer-Controlled Machining
Week: 08
weight: 80
description:
date:
slug:
image: assemble.jpg
categories:
- CNC milling
- modeling
-
---


## About CNC  
- CNC (Computer Numerical Control)

- **Feed and speed**  
  - chip load: how much material one bites cut out
  - Feedrate: how fast tool moves (mm/min)  
  - spindle speed(rpm)
  - Cut depth: tool plunge depth each path
  - step-over: the overlapping of tools

>  chip load: ~ 0.001-0.010"  
   feed rate (inches per minute) / (RPM x number of flutes)  
   cut depth: ~ **0.5-1.0** of the diameter  
   step-over: ~ tool diameter/2



- **Flutes**  
![Down cut and up cut](flute.jpg)
Each flute is taking the material away, less flute the cut is **not as smooth**, but less flute is good at **cleaning the material**  
  - fewer flute - roughing  
  - more flute - finishing  

![](endmill.jpg)

- **Cut air**  
set Z origin higher, make sure everything works porperly before actually run it  
The radiused corners bump into each other and the part edges don’t line up.

- **Joinery**  
Always try joinery and tolerances first before milling the design.
[50 Digital Wood Joints by Jochen Gros](http://winterdienst.info/50-digital-wood-joints-by-jochen-gros/)

- **Three axis milling**  
Rough cut and finish cutout  
two sided Milling---registration and flip   

[Platform CNC | Milling a Two Sided Soap Dish](https://www.youtube.com/watch?v=BLD4dFoXC7o&ab_channel=BrianOltrogge)

[Simulating Water Drops on Wood](https://www.youtube.com/watch?v=npGsQcd1QJI&ab_channel=BrianOltrogge)

- Registration  
Write down X and Y coordinates, and dont mill around it, preserve it for future passes
Use pin to line up for the second side    
leave space for milling bits to pass(1.4mm offset for rough finish)  
Draft angle can help reaching deeper  

[RhinoCAM](https://mecsoft.com/rhinocam/)

[Numerically Controlled : Poster Series](http://mwmgraphics.blogspot.com/2011/07/numerically-controlled-poster-series.html)

## Design
![Coffee table design](table_design.png)
I want to deisgn a set of coffee table with organic shape. The joint is just simple 8mm pocket.  
![Milling outline](outline.png)
## CNC milling

### Preparation
- **Measurement**  
**wear gloves** when carrying the wood board    
measure plywood board's **width, length and thickness**

![Milling tools (first number suggect diameter)](milling_tools.jpg)
Milling tools:  
check flute length and diamete, number of flute, shank diameter(mounting the tool)

### Calculate tool path
Vcarve pro

- **Job set up**  
![Job set up](job_setup.png)  
input the measurement of wood board and set origin.

- **Fillet corner**  
  - Dog bone fillet  
  - T bone fillet  
![test piece](test1.PNG)
![fillet manually](fillet_manually.PNG)
Since the milling tools are cylinder and will leave a radius for inside corner.   
I find that dog bone fillet will not generate on the curved angled corner, so I `draw circle` around the corner and `boolean difference `to do manual fillet.

- **Generate cut out tool path**  
![set cut out tool path](toolpath.png)
- select cut out vector, set the `cutting depth`, ( **material thickness+0.2mm** to make sure it cut through)  
- Select milling tools. Calculate the feedrate and plunge rate.
> Feedrate = N * C * S  
N - number of flutes  
C - chip load (mm) (depend on the manufacturer)  
S - spindle speed (rpm)
plunge rate(verticle movement) = 1/4 of feed rate
step over  40%

- direction
  - climb - cleaner cut,  more pressure on tool  
  - conventional - opposite direction   

![add_tabs](add_tabs.PNG)
- Add tabs to toolpath  
Can manually add or automatic generate (need to adjust the tabs out of corner).
Softer material needs bigger tabs
- Calculate

- **Generate pocket toolpath**  
![Generate pocket toolpath](pocket.png)
select pocket vector, set cut depth , and select tools, `calculate`

- **Save toolpath**  
![Save toolpath](save.PNG)
If we use the same tool for pocket and cutout --> check `output to one file`.  
Adjust the sequence of toolpath (**pocket first, cutout last**). Select both toolpaths and `save toolpaths`

- **Coffee table setting**
![Add tabs](table_add_tabs.png)  
![table milling preview](table_preview.PNG)


### Milling
- **Bed setting**   
![](vent.png)
1. Open vents cap, and put them in the yellow box.  
2. Adjust the rubber band on the milling bed, they should be a bit smaller than the material (manage where air flows)
3. Put the sacrifice MDF sheet on millign bed. (the quality of MDF also matters, the better the surface, the easier for vaccum.)

- **Turn on machine**  
![Turn on switch](turn_on_switch.jpg)
![Press green button to turn on power](press_green_button.jpg)

- **Mach 3**    
![Initialize March3 settings](initialize.png)

- **Intall tools**  
![Move dust shoe aside](dust_shoe.jpg)
Hold `dust shoe` with one hand, the other hand rotate screw, move down the dust shoe a bit and move it aside.  
Tighten the screw to fix.(no gloves when handling tools)

![Soft protection underneath](spounge.jpg)
Move the milling head closer to bed, put a **sponge underneath** for protection.  

![Screw the nut out](rotate_nut.jpg)
![push collet into nut](collet_in_nut.jpg)
Screw the nut out. Find the right size of **collet** in the tool holder, and gentlely push it into the nut.   
Check if the button of surface is **flat**, and screw it back to tool holder.

![insert milling tool](insert_tool.jpg)
Insert milling tool, normally **1-1.5cm** deep into the collet.

![Tighten the nut](tighten_the_nut.jpg)
Tighten the nut with wrench (the red wrench go from button), not too much pressure though

- **Set Z origin**  
![check if the valve is on](check_valve.png)
![press pump button to start vaccumming](pump_button.jpg)
Check if the valve is on, **wear ear muffs**, and press **vaccum pump** button to start vaccumming  
![Z origin sensor](Z_origin_sensor.png)
Put the sensor on flat MDF board and click `Terän mittaus`.  
When the sensoring is done, put the sensor back.
![Set Z origin](set_Z_origin.jpg)

- **Set material**  
![set material on bed](set_material.jpg)
Move tools away, and use pressuried air to clean dust on MDF.  
Wear gloves and set material on milling bed.

- **Set XY origin**  
![set XY origin](set_XY_origin.jpg)
Move the tool to left button corner, and **set XY** origin on Mach3  
![put the dust shoe back](put_shoe_bacl.jpg)
Put the dust shoe back, **activate** the vaccum and leave the room.

- **Start milling**  
![Green button on wall to start milling](green_button.jpg)
`Load G code`, press `green button` on the wall to start milling.

![Done with milling!](done.jpg)

- **Cleaning**  
![blow away wood chips residue](air_blow.jpg)
Clean out dust   
Put all tools back  
Sweep floor and vaccum the wood dust.

- **Turn off machine**  
Press **red button** to turn off power, and turn off **main switch**.

### Post processing  
Wear gloves and goggle.  
- **Remove cut out from board**  
![remove the cut out](remove.jpg)
use chisel and knife to remove the piece from the board

- **Clean tabs**  
Connect `router` with vaccum, clamp the pieces. Turn on both switches, and turn on the router, keep it 90 degree with wood surface, and move the roouter along the edge. The router can also set speed.

- **Sanding**  
![Sand the edge chipping and smooth the surface](sanding.jpg)

- **Cleaning**  
Put all tools back and clean the floor.

![Assemble](assemble.jpg)

### Result
The joints works okay, it is tight. But my design is very problematic and they won't stand firmly. I think for a while how I can improve it but I can not find a good solution. Maybe I should just do something box-like with finger joint to make it as least solid.

## Failures and reflection
![Failed test pieces](failure.jpg)

I have not have so many issues in the previous assignment, but I also learned a lot from the failure.

- 80% of original size  
Firstly I used `svg` saved from Adobe illustrator. I did not check the size in Vcarve, and after it milled I realize the pieces shrinked. I also tried rhino exported DXF directly and it is okay.
> Re-check the size in Vcarve again.

- different depth of two blocks   
One time some how the depth of two block are not the same (3-4mm difference, and one edge way deeper in MDF), even though in the preview they all look cut through.
>(still not sure whats the reason is)

- vaccum issue  
The plywood is bending, and the vaccum does not hold the piece properly. The plywood moved during milling.  
> Clean dust between MDF and plywood  
Set rubber bend nicely  
Find less bendy plywood.

- milled
also because the wood is bending up, and the tabs get all through, and not able to hold the cut out pieces, when mill the pocket, it moved.  
>Milling sequence: pocket -> cut


- Vaccum was off when set the Z origin  
One time I forgot to turn on vaccum to set Z origin after I change sacrifice board, and all pieces did not cut through, left about 1mm think material in button.
> Turn on vaccum when sensoring Z origin.

## Files
[CNC file for tea table](cnc.AI)
