---
title:  Networking and Communications
Week: 14
weight: 140
description:
date:
slug:
image:
categories:
---

## Communications Protocol
Before I start to test my board, I want learn more about communication protocols. In the embeded program week and input devices week, I have learned a little bit about those communication protocol. But I was not very clear how it works.

### Serial Communications
Serial communications is **asynchronous**, and it has a number of built-in rules to ensure error-free data transfers. We need to making sure that both devices on a serial bus are configured to use the exact same protocols.

- `Data bits `   
Amount of data range from 5 to 9 bits.

- `Synchronization bits `  
only one start bit & one (more common) or two the stop bit(s).
start bit | from 1 to 0
stop bit(s) | holding the line at 1

- `Parity bits `  
A simple, low-level error checking form (odd / even). not very widely used, slow down your data transfer and requires both sender and receiver to implement error-handling.
`0b01011101` === odd number of 1's (5), the parity bit === 1.

- `Baud rate` [bits-per-second (bps)]    
Specifies how fast data is sent over a serial line. The higher a baud rate goes, the faster data is sent/received, but there are limits to how fast data can be transferred. The common baud rates is **9600 bps**, and we barely see speeds exceeding **115200** - that's fast for most microcontrollers.

![A serial frame](serial_frame.png)

* `Wiring and Hardware`
![Two transmitters sending to a single receiver--> bad!](2transmitters_1receiver.png)
![one transmitters sending to two receiver--> Ok](1transmitters_2receiver.png)
Two devices trying to transmit data at the same time is **BAD**!
Multiple receiving devices to a single transmitting device is acceptable but could be dangerous to firmware still.

### SPI | Serial Peripheral Interface
* MISO/MOSI, CIPO/COPI  
master/secondary, controller/peripheral

SPI communication allows high-speed **synchronous** data transfer between the AVR and peripheral devices. Second purpose of SPI is used for In System Programming (ISP)

I read through this [article](https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi/all) to better understand what SPI means. Here are some notes:

* Problem for Common serial port   
Common serial port ( TX and RX ) has no control over **when** data is sent, so two system can not properly communicate if they have **different clocks**. To solve that, add **extra start and stop bits** to each byte in asynchronous serial connections help the receiver sync up to data as it arrives.
![asynchronous communication](asynchronous_communication.png)

* A Synchronous Solution  
Still, the receiver is sampling the bits at very **specific times**. If the receiver is looking at the wrong times, it will see the wrong bits. SPI is a "synchronous" data bus, **data and a "clock"** uses separate lines to keeps both sides sync perfectly. The clock is an oscillating signal that tells the receiver exactly **when to sample the bits** on the data line.
![SPI communication](SPI_communication.png)

* Receiving Data  
In SPI, only one side generates the clock signal (CLK or SCK). To receive data and sending data, the controller would generate clock cycles. Also, because the controller always generates the clock signal, it must know in advance **when a peripheral needs to return data and how much data will be returned.**
![Basic SPI](BasicSPI_Updated.jpg)

* CS | Chip Select  
This tells the peripheral that it should wake up and receive / send data and is also used when multiple peripherals are present to **select the one** you'd like to talk to. CS line is normally held `high`, which disconnects the peripheral from the SPI bus.

* Multiple peripherals
![Multiple CS](MultipleCS_1.jpg)
Each peripheral will need a **separate CS line**. And two peripherals should not activated at the same time (one low and keep the rest high)

![Single CS](SingleCS.jpg)
The other option is a **single CS line**, which goes to all the peripherals. (example: Neopixel) Since data pass through all peripherals, **lots of data** is needed to reach all of them. (I think this is also the **hop-count** Neil was talking about?)

This is typically used in **output-only** situations. In these cases, the controller's CIPO line can be disconnected.

* Programming for SPI
  * `shiftIn()` and `shiftOut()` > slow
  * **SPI Library** >  fast, but only work on certain pins

### I2C | Inter-Integrated Circuit
Again, I find this [article](https://learn.sparkfun.com/tutorials/i2c/all) on Sparkfun explaining I2C. I feel the I2C protocol is more complicated than the SPI protocol, I am not following as well as the SPI explaination. But still, I try to take some notes of what I comprehend:

I2C protocol allow **multiple "peripheral"** digital integrated circuits ("chips") to communicate with **one or multiple "controller"** chips. (SPI only allows one controller on the bus)

![I2C Block Diagram](I2C-Block-Diagram.jpg)
* SDA | Serial Data
* SCL | Serial Clock

The I2C bus drivers are **"open drain"** ( they can pull the corresponding signal line `low`, but cannot drive it `high`.) Each signal line has a pull-up resistor on it, to restore the signal to `high` when no device pull it to `low`.
![I2C Schematic](I2C_Schematic.jpg)

Messages are broken up into two types of frame:
`Address Frame` | where the controller indicates the peripheral to which the message is being sent
`Data Frames` | 8-bit data messages passed from controller to peripheral
Data is placed on the SDA line after `SCL goes low`, and is sampled after the `SCL goes high`.

![I2C Basic Address and Data Frames](I2C_Basic_Address_and_Data_Frames.jpg)

The controller leave SCL `high` and pulls SDA `low` to **initiate** the address frame, to notice peripheral devices that a transmission is about to start. Then **address frame** will be sent, following the **data frames**. Finally, the controller will generate a **stop condition** (defined by a 0->1 (low to high) transition on SDA after a 0->1 transition on SCL).

## Understand bite and binery
After know a bit more about communication protocols, I followed Kris's tutorial and had simulation on [wokwi](https://wokwi.com/) to better understand how to handle bites and binery for communication.

![LED connection](wokwi_connection.png)

* Shift bytes
```
void setup() {
byte A = 0b0100;
Serial.print(A, BIN);
Serial.println(" < A");

byte shiftLA = A << 2; // shift byte left
Serial.print(shiftLA, BIN);
Serial.println(" < shift Left A");

byte shiftRA = A >> 1; // shift byte right
Serial.print(shiftRA, BIN);
Serial.println(" < shift Right A");
}
```
100 < A  
10000 < shift Left A  
10 < shift Right A  

* Extract Address and action
```
byte incoming = 0b00000010;
Serial.print(incoming,BIN);
Serial.println("> Full incoming byte");

byte Addess = incoming & 0b00000001; // extract the last byte
Serial.print(Addess,BIN);
Serial.println("> Extract Addess");

byte Action = (incoming & 0b00000010) >> 1 ; // extract the second last byte
Serial.print(Action,BIN);
Serial.println("> Extract Action");
```
10> Full incoming byte  
0> Extract Addess  
1> Extract Action  

* Define LED function
```
void setLED (byte Addess, byte Action){ //Define setLED function
  int led_pin; //Find incoming data address
  if(Addess == ADD_LEDY){
    led_pin = ADD_LEDY;
  }
  if(Addess == ADD_LEDG){
    led_pin = ADD_LEDG;
  }
  bool led_state;
  if(Action == ACT_ON){
    led_state = HIGH;
  }
  if(Action == ACT_OFF){
    led_state = LOW;
  }
  digitalWrite(led_pin,led_state);
}
```
Since the monitor on wokwi can not send bites, we can use [ASCII control characters](https://www.ascii-code.com/) to control the program.

![wokwi LED test](wokwi_test.png)
Unfortunately it did not work, I was not sure if it because I used (I want to fade the LED thats why I connect analog pin), So I change to PMW digital pin and it still doesnt work. None of the LED was on when I change the incoming data.

```
#define PIN_LEDY 3
#define PIN_LEDG 9

#define ADD_LEDY 1 //00000001
#define ADD_LEDG 0 //00000000

#define ACT_ON 1
#define ACT_OFF 0

void setup() {
  // put your setup code here, to run once:
pinMode(PIN_LEDY, OUTPUT);
pinMode(PIN_LEDG, OUTPUT);
Serial.begin(9600);
Serial.println("HELLLOO");

/*
byte A = 0b0100;
Serial.print(A, BIN);
Serial.println(" < A");

byte shiftLA = A << 2;
Serial.print(shiftLA, BIN);
Serial.println(" < shift Left A");

byte shiftRA = A >> 1;
Serial.print(shiftRA, BIN);
Serial.println(" < shift Right A");
*/

byte incoming = 0b00000010;
Serial.print(incoming,BIN);
Serial.println("> Full incoming byte");

byte Address = incoming & 0b00000001;
Serial.print(Address,BIN);
Serial.println("> Extract Address");

byte Action = (incoming & 0b00000010) >> 1 ;
Serial.print(Action,BIN);
Serial.println("> Extract Action");

setLED(Address, Action);
}

void loop() {
  // put your main code here, to run repeatedly:
  while(Serial.available()){
    char incoming = Serial.read();
    Serial.print("< Received byte");
    Serial.print(incoming, BIN);
    Serial.println();
    byte Address = GetAddess(incoming);
    Serial.print(Address, BIN);
    Serial.println("< Address");
    byte Action = GetAction(incoming);
    Serial.print(Action, BIN);
    Serial.println("< Action");
    setLED(Address,Action);
  }
  delay(50);
}

byte GetAddess (byte incoming){
  byte Address = incoming & 0b00000001;
  return Address;
}

byte GetAction (byte incoming){
  byte Action = (incoming & 0b00000010) >> 1 ;
  return Action;
}

void setLED (byte Address, byte Action){ //Define setLED function
  int led_pin; //Find incoming data address
  if(Address == ADD_LEDY){
    led_pin = ADD_LEDY;
  }
  if(Address == ADD_LEDG){
    led_pin = ADD_LEDG;
  }
  bool led_state;
  if(Action == ACT_ON){
    led_state = HIGH;
  }
  if(Action == ACT_OFF){
    led_state = LOW;
  }
  digitalWrite(led_pin,led_state);
}
```

## Connect my board
I want to use the existing board I made during input and output device week. I have **two HELLO board** (one with two LED and one with one), **a moduler sensor board**, **a MOSFET board** and **a DC motor board**. (step motor board unfinished yet).

### Sensor + LED
To start simple, I am just using one HELLO board and a phototransistor sensor. I was looking into [Adrian](https://fabacademy.org/2020/labs/leon/students/adrian-torres/week14.html#hello_serial_bus)'s networking assignment to get an idea how should I connect the boards and program them.

In his code, I find `Serial.parseInt()` that I am not sure what it does, so I look it up. And here are some examples to help understanding:

* //serial receive buffer--> "I ate 314 tacos"  
int dataIn = Serial.parseInt();  //dataIn now holds `314`

* //serial receive buffer--> "¡Arriba, arriba!"  
int dataIn = Serial.parseInt();  //dataIn now holds `0`

* extra notes:  
parseInt() actually returns a `long` datatype, not an `integer` datatype!  
parseInt will `“time out”` after a given programmable set point. The default set point is 1 second `(1000 milliseconds)`.  
To adjust this time out period, I can use the `Serial.setTimeout(time)` function.

![sensor LED connection](sensor_LED_connection.png)

After I upload the code to sensor, I was using FTDI to read the value the sensor is sending. I was confused why Adrian's code is sending one extra line with value, but I did not change it and program the HELLO board still, but it does not work.

Matti helped me understand how `Serial.parseInt()` worked in the receiver board code. Basically, `Serial.parseInt()` is only taking the package of numbers and ignore other characters. So in Adrain's code, adding a **","** is just to stop the reading and **extract address**, then `Serial.parseInt()` **again** and it would read the value after ",", to **extract sensor value**.

And this are the modified code that work for my board:  
**Sensor Code**
```
int sensorPin = 0;
int sensorValue = 0;
int address = 1;

void setup() {
  Serial.begin(115200);
}

void loop() {
  sensorValue = analogRead(sensorPin);
  sensorValue = map(sensorValue, 0, 1024, 1024, 0);
  Serial.print(address);
  Serial.print(",");
  Serial.println(sensorValue);
  delay(10);
}
```
Here is the address and value the phototransistor read and sent:  
{{< video "./address1.mp4">}}

Then for the receiver code, Adrian is using `softwareSerial()` Library, I did not understand why. But I still kept the function and modified the code according my board. When I upload the code to HELLO board, the white LED turned on and blue did not, and the sensor absolutly do nothing here.

Then Matti told me `softwareSerial()` is to configure other pins into **RX and TX pins**. Since I am using RX and TX pin already, there is no need to use this library. Then I modified the code again and upload, this time it worked!!

And this are the modified code that work for my board:  
**LED**
```
int id, value;
int MyId = 1;

int LED_pin1 = 2; //LED white
int LED_pin2 = 3; //LED blue

void setup() {
  Serial.begin(115200);
  pinMode(LED_pin1, OUTPUT);
  pinMode(LED_pin2, OUTPUT);
}

void loop(){
   int average;
   if (Serial.available()){
    id = Serial.parseInt();
    if (id == MyId){
    average = Serial.parseInt();
    }
    else {
    average = 0;
    }
  }
    if (average > 5) {
       digitalWrite(LED_pin1, HIGH); // blue on
       digitalWrite(LED_pin2, LOW); // white off
    }
    else if (average <= 5) {
      digitalWrite(LED_pin1, LOW); // blue off
      digitalWrite(LED_pin2, HIGH); // white on
  }
}
```
{{< video "./sensor_LED_result.mp4">}}

### Sensor + Motor
Then I was thinking using the DC motor, but unfortunately there is no serial communication on my board. the TX, RX pin was also taken since ATtiny 412 only habe 8 pins. Matti suggest maybe we could try use the `UPDI` pin as TX pin with `softwareSerial()`.

![sensor motor connection](sensor_motor.jpg)

![The sensor motor code](sensor_motor_code.png)

It did not work, the **motor is running** but the **sensor is not doing anything**. We fumbled the code for a while, tried many method and try to use the LED to debug. But it just would not work, maybe the UPDI pin can not be used as TX pin?

### Sensor + LED + LED
Then I was thinking using two HELLO board. Since there will be two address for receiving the data, I need two address: one address **with light** and one **without light**.

Here is the data the sensor is sending
{{< video "./address2.mp4">}}
I notice that in some data, the address **disappeared**, I still have not figure out why.

Another thing is that on my board there is only one pinout for TX, so I was using the pin socket, solder the back pin's together and then I got the wire distribution port!
![connection socket](connection_socket.jpg)

Here is the board connection:
![sensor + LED + LED](sensor_LED_LED.jpg)

Here is the **Sensor** code
```
int sensorPin = 0;
int sensorValue = 0;
int address1 = 1;
int address2 = 2;

void setup() {
  Serial.begin(115200);
}

void loop() {
  sensorValue = analogRead(sensorPin);
  sensorValue = map(sensorValue, 0, 1024, 1024, 0);
  if(sensorValue > 10){ //light address 1
  Serial.print(address1);
  Serial.print(",");
  Serial.println(sensorValue);
  }
  else if(sensorValue <= 10) //no light address 2
  Serial.print(address2);
  Serial.print(",");
  Serial.println(sensorValue);
  delay(50);
}
```


**HELLO board one**  
This is the hello board I design in previous week. The plan is that the white and blue LED **take turns to blink**. And when there is a certain amount of light, the switching between them **slow down**.   
```
int id, value;
int MyId = 1;  //light address 1

int LED_pin1 = 2; //LED white
int LED_pin2 = 3; //LED blue

void setup() {
  Serial.begin(115200);
  pinMode(LED_pin1, OUTPUT);
  pinMode(LED_pin2, OUTPUT);
}

void loop() {
  int average;
  if (Serial.available()) {
    id = Serial.parseInt();
    if (id == MyId) {
      average = Serial.parseInt();
    }
    else {
      average = 0;
    }
  }
  digitalWrite(LED_pin1, HIGH); // blue and white blink
  digitalWrite(LED_pin2, LOW);
  delay(average*100); // pause time depend on sensor value
  digitalWrite(LED_pin1, LOW);
  digitalWrite(LED_pin2, HIGH);
  delay(average*100);
}
```

**HELLO board two**  
This is the hello board I did to try out flexible PCB. There is one white LED on it. So I was thinking using the sensor value to affect the **brightness** of the LED. It is reading the value when there is less light. And the less light, the dimmer.
But since the maximum amount the LED pin can read is 255. I use the  `constrain()` function to limit.

```
int id, value;
int MyId = 2; //no light address 2

int LED_pin = 2; //LED

void setup() {
  Serial.begin(115200);
  pinMode(LED_pin, OUTPUT);
}

void loop() {
  int average;
  if (Serial.available()) {
    id = Serial.parseInt();
    if (id == MyId) {
      average = Serial.parseInt();
    }
    else {
      average = 0;
    }
  }
  average = map(average, 0, 10, 0, 255);
  average = constrain(average, 0, 255);
  analogWrite(LED_pin, average);
  delay(500);
}

```
{{< video "./sensor_LED_LED.mp4">}}
I find on blue and white board, the white LED blink a bit weird, sometimes it blink really weakly. Not sure what the reason is.

## Sensor + Neopixel
I would like to try out Neopixel since I want to use it in the final project. And I soldered pin on the Neopixel strip and connect with my sensor base board.

Then install the[Adafruit_NeoPixel library](https://github.com/adafruit/Adafruit_NeoPixel). And run a example code to test it. One thing is that I need to change to `16HZ` or it wont work.

{{< video "./Neopixel_test.mp4">}}
