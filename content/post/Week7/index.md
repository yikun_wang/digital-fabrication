---
title: Electronics Design
Week: 07
weight: 70
description:
date:
slug:
image: holes.png
categories:
- electronics
- CNC milling
- soldering
---

## Design
### KiCad
I have not try to design PCB board myself before, so I would like to follow tutorial and repeat the Hello board design process first and then try to modify or design my own board.  
[Download Kicad 6.0](https://www.kicad.org/)    

![Kicad](Kicad.png)
`Schematic Editor `--arrange symbols created in `Symbol Editor `  
`PCB Editor `--arrang footprint created in `Footprint Editor `  
`Gerber Viewer`--double check gerber file

### Load library in KiCad

[Fab electronics component library for KiCad](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad)  
- **Add symbol libraries:**  
![Manage symbol libraries](Manage_symbol_libraries.png)

![Add folder](Add_folder.png)

![Load symbol files](symbol_files.png)

- **Add footprint libraries:**  
Same way of adding symbol libraries, but select fab.pretty folder.
![footprint libraries](fab.pretty.png)

- **Configure libraries paths:**  
![configure paths](configure_paths.png)

![configure FAB path](configure_paths2.png)

- **Create new project**
![new file](new_file.png)

### Schematic Editor
![Schematic editor](Schematic_editor.png)

![ATtiny412 data sheet](ATtiny412.png)

- **Add symbol**   
![Add symbol tool(A)](Add_symbol.png)

![Add symbol](Add_symbol2.png)

![Add all needed symbol](Add_symbol3.png)
`R` rotate element  

- **Link symbol and add labels**     
double click to stop  
Let KiCad know where is the ground  
Bypass capacitor - avoid noisy current for controller  

`S` select components  
`G` grab a component  
`ctrl L` Add labels  
select label and copy paste  
**Add labels first then add wire to connect**

TXD -- transfer   
RX -- receive
![Link_symbol](Link_symbol.png)

- **Explicit value**
  - C - capacitor
  - U - integrated circuit
  - J - jumper
  - D - dial
  - R - resistor

**Manually**: double click    
**Build-in function**: fill in Schematic symbol reference designator    
![Auto fill function](auto-fill.png)

![Annotate schematic](auto-fill1.png)

![After auto-fill](auto-fill2.png)

- **Perform Electrical rules check**
![perform Electrical rules check ](rule_check.png)
Need to add power flag to where the power comes in

![Add power flag](add_power_flag.png)

![All good!](no_error.png)

Run foot print assignment tool  
Footprint: actual drawing of copper layer

### PCB Editor
- **Import schematic**
![Import schematic to PCB editor](import_schematic.png)
click update

![imported schematic](import_schematic2.png)

- **Layout schematic**   
![Layout schematic](Layout_schematic.png)
Grid:  
0.1 mm  - finner adjustment   
2.54 mm   

- **Add tracks**    
Edit pre-defined size for tracks
![edit pre-defined size](edit_pre-defined_size.png)

![board setup](board_setup.png)

route tracks tool `X`  
edit tracks:  
`U` - select all track  
`D` - drag line around
`Alt 3` - PCB Viewer

Connect signal first (**UPDI / LED / RX / TX**)

![First attempt](First_try.png)
I want to try to connect all lines without holes first. After a few tries, I reached this result and I realized that there was one track I could not connect anyhow. So I deleted all tracks, adjust layout and redo the tracks again.   
![Second attempt](second_try.png)
After adjust layout I achieved the no hole goal, pushing this help me to better understand how to connect different component.

- **Add cutout outline**    
Switch layer to `Edge Cuts`
![Draw arc](draw_arc.png)
Here I was using a rectangle as reference, and draw the 4mm arc, and copy this arc to four corners and use `R` to totate arc. Then I draw the lins to connect arcs.

- **Add texts**    
Switch layer back to copper `F.Cu`
Add text and edit the size

![Edit text](text.png)
If the design will be sent to PCB manufacturer, then change texts layer to silkprint

- **Double-side PCB**   
![track to modify](track_to_modify.png)
`V` draw tracks and hit V to draw track on back side.
![double side](double_side.png)

- **Add mounting holes**   
![Add footprint](Add_footprint.png)
`Add footprint`, search `hole` and find the proper size hole, adjust grid size to fit the holes. Copy the hole to the other corner.
![through hole](through_hole.png)

![holes](holes.png)

- **Design rules checks**
![Design rules checks tool](check.png)

![Find error](find_error.png)

![Fixed!](Fixed.png)

- **Export gerbers**
![export gerbers setting](plot.png)
`Plot` to generate gerber files and drill files.
![generate drill file](drill_file.png)

- **Export SVG**
![Export SVG](export_SVG0.png)

![Export SVG](export_SVG.png)

### Final hello board design
![updated schematic](updated_sch.png)  
![final PCB design](final_hello.png)

## Producing
### CopperCAM
- **Set cut out lines and mounted hole**
![Cut PCB mounted holes](PCB_mounted_holes.JPG)


- **Add back layer**
![Add back layer](add_layer.jpg)
file - open - additional layer - add back layer file
Do not set the back layer outline to contour path, so we will be able to select them and delete them `delete all identical tracks`.
![No to contour path](back_layer.JPG)
![Delete back layer outline](back_layer1.JPG)

- **Edit holes**  
![Edit drilling holes](edit_holes.JPG)
Select holes, right click and edit all identical pads.  
**connector** : round / size - 1.8/ drill - 1  
**UPDI** : round / size - 2.1/ drill - 1.4  
[need 0.4 between holes so the milling bites can go through]

- **Align holes**
![Select one hole in front as reference](reference_hole.JPG)
![adjust holes in the back](adjust_back_holes.jpg)
The holes in back layer did not line up nicely, so we need to align the holes in the back side.    
Select one hole in front side, right click and `set as reference pad`. Then switch to second layer (back layer), right click the hole, and `adjust to reference pad#1`    

- **File dimention**  
Check if margin is 1mm.  
Double side FR1- thickness 1.8mm
![](depth_no.png)
Since the drilling bits head is V shape, it need a few more millimeter to be able to drill through the hole. So no change the value.

- **check selected tools**
![check active tool](active_tool.JPG)
Here I am using `2`(engraving); `3`(cutting); `5`(drilling)

- **Calculate contours**  
Edit Text and calcute milling contour

- **Export milling file**
![Export PCB front milling file](PCB_front_export.png)

![Export PCB back engraving file](back_export.png)
`Front` - export engrave, drilling and cut file.  
`Back` - export **mirror** engrave file  

![Front milling preview](front_path.JPG)
![Back milling preview](back_path.JPG)

### Milling

Same process of milling PCB of week 5.  

- **Milling sequence:**  
**Front**: engrave (T2), drill(T5-1mm drill), cut out(T3)  
**Back**: engrave  
Everytime change the milling bits, move head away and `Set Z origin`

![Engrave the front](Milling_engrave.jpg)

![Milling cutout](Milling_cutout.jpg)

When the front layer is done, take it out, remove tape, add tape on the front side, flip left/right and put back to pocket. and keep 1mm on every seam, press down with cloth and make sure it stick nicely.
![Tape PCB front](tape_front.jpg)

![flip the PCB in the Y axis](Milling_flip.jpg)

![Engrave the back](Milling_engrave_back.jpg)

![Milling result](Milling_result.jpg)

- **Attach rivet**  
Check the size of the pin, if neccessary, change the pin.    
![](changing_pin.jpg)
![](changing_pin2.jpg)

T shape facing down, put the PCB hole through rivet  
(PCB board facing does not matter as long as consistant.)  
![Rivet position](add_rivet.jpg)

![press the pin to fold rivet](put_PCB_on_rivet.jpg)

### Soldering
- **Through hole component**  
Soldering iron tip touch the rivet and pin   
Through hole component is more stable  
When solder from the back side, need rivet, solder on the front side, no need for rivet(smaller holes).

![collect components](collect_components.jpg)

- **Check polarity**

![](votage.jpg)
Connect two tip and adjust amp, then connect with the components (black is ground).  
![The green suggest the GND](LED_polarity.jpg)  

* Solder for the button  
I had problem soldering button since the pin is very tiny. Matti suggested add a bit solder on the button pin first, then solder to the PCB board.

![finish soldering](finish_soldering.jpg)

## Testing and Programing
My UPDI board is Programmed already in the electronic production week when I want to test if it works.

So I just connect my HELLO board with my UPDI board, and program the hello board in Arduino.

### Setup Arduino
Add board
[megaTinyCore](https://github.com/SpenceKonde/megaTinyCore/blob/master/Installation.md)
`http://drazzy.com/package_drazzy.com_index.json`

[Fab SAM D|L|C Core for Arduino](https://github.com/qbolsee/ArduinoCore-fab-sam)
`https://raw.githubusercontent.com/qbolsee/ArduinoCore-fab-sam/master/json/package_Fab_SAM_index.json`

File -> Preferences, enter the above URL in "Additional Boards Manager URLs"
![load board link](load_board_link.png)

Tools -> Boards -> Boards Manager
search for the board package, select the package and click "Install".
![install board](install_board.png)

![board installing](board_installing.png)

Insert UPDI board, select the right port.  
![select port](select_port.png)

![select board](select_board.png)
There are two ATtiny412, but the second one took too much memory, here we are using the first one

### Programing
[Arduino programming language reference](https://www.arduino.cc/reference/en/)  
[ARDUINO:BASICS](https://newmedia.dog/c/efa/arduino-basics/)

`Ctrl T `-- to clean up code

Pinmode in Arduino is not the same as pin of microcontroller, always check data sheet.
![ATtiny](ATtiny.png)

* For button
use `digitalRead`
since I am using an internal pullup. It is a bit different. When press the button, pin connect to ground, it actually disconnect, val == LOW.
![Button knowledge notes from Matti](Button.jpg)

```
if (condition1) {
  // do Thing A
}
else if (condition2) {
  // do Thing B
}
else {
  // do Thing C
}
```

![When hold button, the white LED turns on](hold_on.png)  
{{< video "./on_and_off.mp4">}}
![When hold button, blue LED switch off and white LED switch on](switch.png)
{{< video "./switch.mp4">}}

## Use oscilloscope

## Reflection
I had not much knowledge about Electronics. I can follow the tutorial to design the board and it works. But when I try to design my own PCB board, I still have many confusions like how to connect different microcontrollers, add different components, what resistor should I use etc. I think learn a bit more with electronics would greatly help me.

Another thing I notice is during soldering, Some tracks was a bit too close to the footprint and when I solder I have to be really careful to not connect the circuit tracks. I would adjust them a bit further next time
