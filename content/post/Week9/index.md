---
title: Embedded Programming
Week: 9
weight: 90
description:
date:
slug:
image:
categories:
---

Honestly, when I saw this weeks assignment I was a bit confused about what I should do. Then I realize this is a good chance to know more about electronics and microcontroller since two weeks ago I was trying to use the LGB LED and use microcontroller ATtiny1614 and got confused which pin should be connectting and never actually made the board. This process was not easy for me because I do not have any related background and there are many new words to me.

## Load the program
Before starting, I need to flash the board and load the firmware so I can use Arduino IDE to upload the program. When I made the Bridge Serial D11C and UPDI in Electronic production week, Kris helped me flash it, but I was not quite sure how to do that. Now it is more clear.

### Flashing bootloader
First download the [binary](sam_ba_SAMD11C14A.bin). Connecting the board to a [SWD programmer](https://gitlab.fabcloud.org/pub/programmers/programmer-swd-d11c)

in terminal:
edbg:
>edbg -ebpv -t samd11 -f sam_ba_SAMD11C14A.bin

Now the Arduino IDE and computer can detect the SAMD board.

### Setup megaTiny and SAMD Arduino Core
to program from the Arduino IDE, we need to install the Arduino core for SAMD.  
[Fab SAM D|L|C Core for Arduino](https://github.com/qbolsee/ArduinoCore-fab-sam)  
`https://raw.githubusercontent.com/qbolsee/ArduinoCore-fab-sam/master/json/package_Fab_SAM_index.json`

[MegaTinyCore](https://github.com/SpenceKonde/megaTinyCore/blob/master/Installation.md)  
`http://drazzy.com/package_drazzy.com_index.json`

`File` ->` Preferences`, enter the above URL in `Additional Boards Manager URLs`
![load board link](load_board_link.png)

`Tools` -> `Boards` -> `Boards Manager`
search for the board package, select the package and click `Install`.
![install board](install_board.png)

![board installing](board_installing.png)

### Loading USB-to-serial firmware
in Arduino IDE
* tools->board: `Generic D11C14A`
* USB config: `CDC_only`
* Serial config: `TWO_UART_NO_WIRE_NO_SPI`
* Bootloader size: `4KB_BOOTLOADER`

Select port and Upload [sketch](https://github.com/qbolsee/SAMD11C_serial/blob/main/SAMD11C_serial/SAMD11C_serial.ino)

### Programmer
Connect the board to be programed with UPDI board:
![select board](select_board.png)
There are two ATtiny412, but the second one took too much memory, here we are using the first one

![select port](select_port.png)
- Board: `ATtiny 412/1614` and so on
- clock `20MHz internal` if the internal clock is not very precise, then select the external clock
- Port: select the `SAMD11C14's serial port`.
- Programmer: `SerialUPDI - SLOW 57600 baud`

## A bit more about Microcontroller
I read through this [The Practical Microcontroller Primer](https://mtm.cba.mit.edu/2021/2021-10_microcontroller-primer/), it is quite useful and cleared lots of my confusions.

### BASICS
`UPDI` | The Unified Program and Debug Interface | an atmel / microchip proprietary programming interface
much like `JTAG/SWD` used in ARM chips with CMSIS-DAP tools.

`FTDI` | Future Technology Devices International Limited

`bootloader`|
a computer program that is responsible for booting a computer.

`Toolchain`|
a set of programming tools that is used to perform a complex software development task or to create a software product,

`compiler`|
a special program that processes statements written in a particular programming language and turns them into machine language or "code"( “string of 1’s and 0’s” ) that a computer's processor uses.

`IDE`|
An integrated development environment
a software suite that consolidates basic tools required to write and test software.

`GPIO` | general-purpose input/output
an uncommitted digital signal pin on an integrated circuit or electronic circuit board which may be used as an input or output, or both, and is controllable by the user at runtime.

### Bootloaders  
Bootloaders are like “programming programs” or “flashing programs”

Most microcontroller chips have a special programming interface such as `JTAG` or `AVRISP` that allows you to burn programs into Flash memory.

A Bootloader is a **pre-loaded program** that allows you to load other programs via a more convenient interface like a standard USB cable. When power-up or reset microcontroller board, the bootloader checks to see if there is an upload request. If there is, it will upload the new program and burn it into Flash memory.  If not, it will start running the last program that you loaded.


### Programming a microcontroller
![procedure to program and/or debug a microcontroller ](8GaEQ.png)

`JTAG` | joint test action group | a protocol that all program (and debug) with the same interface in 90s
`SWD` | serial wire debug | a two-pin variant of the JTAG protocol  
`CMSIS-DAP` | is the specification (or standard, or protocol, or ...) from ARM, defining how a programmer/debug probe should be built

Any device (programmer) that can write programs into a microcontroller’s memory using JTAG or SWD is a `CMSIS-DAP device` (for example Programmer UPDI D11C)

To program microcontrollers, using a cmsis-dap tool: `EDBG`, `openOCD`, `pyOCD`

### EDBG
EDBG is software we use on our PC to program microcontrollers using a cmsis-dap tool.
[Installing EDBG](https://github.com/ataradov/edbg/actions)

>./edbg.exe -b -t samd11 -pv -f sam_ba_Generic_D11C14A_SAMD11C14A.bin

`./edbg.exe` invokes the executable  
`-b` tells it to produce verbose messages  
`-t samd11` tells edbg which board you’re flashing  
`-pv` tells it to program, then verify
`-f sam_ba_Generic_D11C14A_SAMD11C14A.bin` points it to the .bin file you’ll flash.



## The ATtiny Datasheet
I took a rough look of ATtiny412 datasheet last time when I try to design the board two weeks ago. Back then I had many confusions since I do not have anybackground of electronics and many words are new to me. I think it is good that I look into the data sheet first and learn more about microcontroller.

> ATtiny412, ATtiny1614, ATtiny3216:  
• 1-series  
• 8 bit, 1.8-5.5V, 20 MHz  
• single-cycle global instructions (each time clock tick, everthing move forward)  
• simple peripheral register access  
• low pin-count packages  
• one-pin serial programming  

>ARM:
D11C, D11D, D21E, D51  
• 32 bit, 1.6-3.6V  
• faster clocks, more complex clock distribution and synchronization  
• more powerful peripherals, more complex register access and libraries  
• diverse family, larger packages  
• standard in-circuit debugging  


### Block Diagram
![ATtiny212/ATtiny412 Block Diagram](attiny412_block_diagram.png)

CPU + peripheral (some small circuit that performs a specialized function.) We can think of these as little hardware “libraries” that are included on the microcontroller. Each of these runs independently of the cpu itself,

timers (which count time, and also operate PWM)
ADCs (analog to digital)
DACs (digital to analog)

### Pinout
![The Pinout overview](pins_attiny.png)
- `VDD` | Power supply
- `GND` | Ground
- `PA` | PortA
- `EXTCLK` (External Clock)| Connect this pin to an external clock(count external events)
- `RESET` | Use this pin to program, debug, reset the microcontroller
- `UPDI` | Use this pin to program the board via a UPDI connection - this is linked with the RESET

![The Pinout of ATtiny1614](ATtiny1614_pinout.png)
- `PB` | PortB; which port it's on is important if you're doing direct port manipulation. Not important if not doing that.
- `TOSC1,TOSC2` | the PB2,PB3 can also be used as Timer2/Counter2 oscillator, The Timer2/Counter2 clock can also be input externally. Once the “EXCLK” bit in the ASSR register is set, the external clock can be input from the TOSC1 pin

Last time when I try to program my board Matti taught me that Arduino pin numbers are different from pin numbers of microcontroller. For example, for ATtiny1614, pin number is 4 , while in Arduino pin numbers is 6.

`Clock signal`  
A clock signal oscillates between a high and a low state and is used like a metronome to coordinate actions of digital circuits.


![The pinout of the ATtiny212/412](pins_arduino.png)
- `MISO` | Master In Slave Out | To send data from the slave to the maser.
- `MOSI` | Master Out Slave In | To send data from the master to the slave / peripherals.
- `RXD` | Receive | The receiving pin to do serial communication.
- `TXD` | Transmit | The transmitting pin to do serial communication.
I also observed that MOSI and TXD are linked, MISO and RXD are linked, which make a lot of sense.
- `SCK` | Serial Clock | Which are the clock pulses that synchronize data transmission generated by the master.
- `SCL` | Serial Clock Line | SCL is the clock line for an I2C bus while SCK is the clock line for SPI communication.
- `SDA` | Serial Data | The pin that carries the data when using I2C
- `DAC` | Digital to Analog converter | converting digital pulses to analog signals.
- `IO multiplexing` | input output multiplexing   
Allowing the programmer to examine and block on multiple I/O streams (or other "synchronizing" events), being notified whenever any one of the streams is active so that it can process data on that stream.

![Digital and Analog converter](D_A.jpg)

### Memories
![Memories of ATtiny x12](Memories_attiny412.png)
![Memories of  ATtiny161x](Memories_attiny1614.png)
> Memory:  
   registers (instructions)  
   SRAM (fast): data fast but not dense  
   DRAM (big): larger but not fast  
   EEPROM (non-volatile): store data without power  
   FLASH (programs, strings): store data without power, denser, harder to program  
   fuse (configuration)  

Honestly, I was not even sure what those different memories are, then I find this page that helped me to undertand better: [three pools of memory in the microcontroller used on avr-based Arduino boards](https://www.arduino.cc/en/Tutorial/Foundations/Memory)
- `Flash memory` (program space)| is where the Arduino sketch is stored.
- `SRAM` (static random access memory)| is where the sketch creates and manipulates variables when it runs.
- `EEPROM` (Electrically Erasable Programmable Read-Only Memory)| is memory space that programmers can use to store long-term information.

So to summary, `Flash memory` can store uploaded program and `SRAM` store data while execute tasks, and `EEPROM` store long-term data even though the power is off. Now I undertand why when I plug my board back and the LED still blink like I programed before.

![Different Types of Memory](Different_Types_of_Memory.png)

### Peripherals and Architecture
smaller, special function circuits that the computer-part of a microcontroller can interact with (via special function registers) in order to operate on the physical world: setting voltages on external pins, reading and writing data to networks, reading data (or voltages) from sensors - etc.
`Peripherals`| Any external device that provides input and output for the microcontroller.
There is the `Peripheral Module Address Map` in datasheet, but I am not sure what the base address does.
>Peripherals:  
   ports  
   A/D (converts analog voltage to digital value)  
   comparator quickly compare different voltadges  
   D/A  
   timer/counter/PWM: count time/ events  
   USART  
   USB  
   math  
   crypto  

8. AVR CPU
9. NVMCTRL - Non Volatile Memory Controller
10. CLKCTRL - Clock Controller
11. SLPCTRL - Sleep Controller
12. RSTCTRL - Reset Controller.
13. CPUINT - CPU Interrupt Controller
14. EVSYS - Event System.
15. PORTMUX - Port Multiplexer
16. PORT - I/O Pin Configuration
17. BOD - Brownout Detector
18. VREF - Voltage Reference.
19. WDT - Watchdog Timer.
20. TCA - 16-bit Timer/Counter Type A.
21. TCB - 16-bit Timer/Counter Type B.
22. TCD - 12-bit Timer/Counter Type D.


## Arduino
[Arduino programming language reference](https://www.arduino.cc/reference/en/)  
[ARDUINO:BASICS](https://newmedia.dog/c/efa/arduino-basics/)

`Ctrl T `-- to clean up code

### Function
- **pinMode()** | Configures the specified pin to behave either as an input or an output  

- **digitalRead()**  Reads the value from a specified digital pin, either `HIGH `or `LOW`.  
**digitalWrite()**

- **analogRead()**  Reads the value from a specified analog pin  
**analogWrite()**  
The input range can be changed using **analogReference()**, while the resolution can be changed (only for Zero, Due and MKR boards) using **analogReadResolution()**.

- **delay()**  
**delayMicroseconds()**

- **map()** | Re-maps a number from one range to another  
`map(value, fromLow, fromHigh, toLow, toHigh)`

- **random()**  
`random(max) / random(min, max)`

- **Serial [Communication]**
  - Serial.begin() | opens the serial port
  - Serial.print() or Serial.println() | Prints data to the serial port
  - Serial.read() | is used to read messages sent to the Arduino board



### Variables
- Data Types
  - int
  - float
  - string

- `static int` and `int`  
If we declare a variable as static, it exists till the end of the
program once initialized
```
#include <stdio.h>

 void func(){
 	int i = 0; //static int i = 0;
 	i++;
 	printf("i = %d\n",i);
 }
int main(){
	func();
	func();
	func();
	func();
	return 0;
}
```
i = 1 1 1 1 (int)  
i = 1 2 3 4 (static int)

- `INPUT`  `OUTPUT`  `INPUT_PULLUP`  
(INPUT mode explicitly disables the internal pullups)
When press the button, pin connect to ground, it actually disconnect, val == LOW.

- `long`
Long variables are extended size variables for number storage, and store 32 bits (4 bytes), from -2,147,483,648 to 2,147,483,647.

### Structure
**#define (define)**
// #define ledPin 3

- **if**  
```
if (condition1) {
  // do Thing A
}
else if (condition2) {
  // do Thing B
}
else {
  // do Thing C
}
```

* == (equal to)
* != (not equal to)
* += (compound addition)
* -= (compound subtraction)

## Program the board
### LED blicking
In electronic design week, I tried to use a button to control the blinking of LED:
![When hold button, the white LED turns on](hold_on.png)  
{{< video "./on_and_off.mp4">}}
![When hold button, blue LED switch off and white LED switch on](switch.png)
{{< video "./switch.mp4">}}


### LED fading

```
int ledPin = 2;   

void setup() {
}

void loop() {
  // fade in from min to max in increments of 5 points:
  for (int fadeValue = 0 ; fadeValue <= 255; fadeValue += 5) {
    // sets the value (range from 0 to 255):
    analogWrite(ledPin, fadeValue);
    delay(30);
  }

  // fade out from max to min in increments of 5 points:
  for (int fadeValue = 255 ; fadeValue >= 0; fadeValue -= 5) {
    analogWrite(ledPin, fadeValue);
    delay(30);
  }
}
```
{{< video "./fading.mp4">}}

I also find this [article](https://create.arduino.cc/projecthub/ankitha_kadem/led-fade-using-arduino-uno-with-code-f05156) and the author kept iterating his code for 4 times to make it brief. Also quite interesting for me.



### Serial Communications
I have not tried to use FTDI to commicate with my board yet. I have [Bridge Serial D11C](https://gitlab.fabcloud.org/pub/programmers/programmer-serial-d11c) prepared in electronic production week.

- Upload the code with UPDI
  - Board: ATtiny 412
  - Port: SAMD11C14's serial port.
  - Programmer: SerialUPDI - SLOW 57600 baud

- Then connect FTDI to read the incomning data
  - Board: Generic D11C14A
  - Serial config: TWO_UART_NO_WIRE_NO_SPI
  - Port: SAMD11C14's serial port.


```
void setup()
{
  Serial.begin(9600); // send and receive at 9600 baud
}
int number = 0;

void loop()
{
  Serial.print("The number is ");
  Serial.println(number);    // print the number

  delay(500); // delay 0.5s
  number++; // add to next number
}
```

{{< video "./number.mp4">}}

### Button
[this](arduino.cc/en/Tutorial/BuiltInExamples/StateChangeDetection) and [this](https://www.kasperkamperman.com/blog/arduino/arduino-programming-state-change/) site about state change detection
