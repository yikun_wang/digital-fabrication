---
title:  Mechanical design
Week: 12
weight: 120
description:
date:
slug:
image: cover.jpg
categories:
---

## Test model with Makeblock beams
This week we are going to build a Pen plotter. Firstly, we want to quickly build the model with the makeblock beams, to figure out how the machanism works. We were trying to learn from Matti's Pen plotter Axidraw, and build our own plotter.
![Matti's Axidraw](Axidraw.jpg)

### T-Bot
![T bot](tbot.png)
* Advantages  
Needs only one belt.  
Simple configuration.  
Moves along two axes.  
* Disadvantages  
The configuration itself (the T) takes up a lot of space.  
Creates a torque around the center of mass.  

![Parts of Makeblock beams](test_model1.jpg)

![build the frame](test_model2.jpg)

### Drive the motor
We are using G shield to drive the motor. Power the G shield with 12V eternal power supply and connect Arduino board with conputer too.

* [Setting the stepper motor current limit](https://discuss.inventables.com/t/setting-the-stepper-motor-current-limit/14998)
![G shield test points for reference voltage](G_shield.jpg)
The gShield (version 5) uses a 0.1 ohm current sense resistor so the formula is **Vref = 0.8 * I**

![step motor specification](step_motor_specification.png)
The Step motor we are using has a maximum current rating of **1.68 amps** per phase. To set the maximum current limit for this motor, we need to set the potentiometer to **(0.8 * 1.68 amps) = 1.34 volts**.

{{< video "./set_up_current_limit.MOV">}}

* Connect and test
![connect motor wire go G shield](connect_motor.jpeg)
Then we use multimeter to find which two wires are connected and put connected wires to the same side of srew terminal.

![Gcode platform controller](Gcode_platform.jpg)

Then Matti did some configuration and it works!

{{< video "./test_model.mp4">}}

## Modeling the pen plotter
Then the next step is build the parts we needed and replace the makeblock beams.
{{< video "./modeling_video.mp4">}}

![Pen plotter model](Pen_plotter.png)

### Print pulley with Formlab
I used grasshopper to build the pulley for the belt.  
![Printed pulley](pulley.jpg)

We first print 4 of those to test, and them work quite well so we print a bunch more.
![printed resin pulley](SLA_print1.jpg)

![bathing](SLA_print2.jpg)

![UV curing](SLA_print3.jpg)

[DrawBot](https://www.printables.com/model/137296-drawing-robot-arduino-uno-cnc-shield-grbl)

### Print base with Ultimaker
![](cura.PNG)

![](PLA_print3.jpg)

### Laser cut
![Vector for laser cutting](laser_file.png)

![laser cut parts](laser_cut.jpeg)

### Assemble
After laser cut the parts, we start to aseemble. First adjust the pulley height so they are at the same level.
![](aseemble.jpg)

Then attach the linear bearing platform, and insert shaft.
![](aseemble1.jpg)

Add the end parts, attach the motor and fix belt.
![](aseemble2.jpg)

At this stage we could run test with it already.The pen was a bit shaky since the pen holder was not ready.
{{< video "./test2.mp4">}}

Then Yoona build the platform for the plotter and Darren build the pen holder.
![](platform.jpg)

With the new pen holder, the drawing is much more stable and accurate.
{{< video "./Test3.mp4">}}

![Final look](final_look.jpg)

## Using Pilvi Pen Plotter
### Configure the Pen plottor.
[Universal Gcode Platform](https://winder.github.io/ugs_website/)  

![firmware setting](firmware_setting.jpg)
`machine` -- `firmware setting ` to set up speed and so on.

![setup wizard](setup_wizard.jpg)
`machine` -- `setup wizard  `to configure the machine.
### Generate G code
We use [DrawingBotV3](https://github.com/SonarSonic/DrawingBotV3/releases/tag/v1.3.5-stable-free) to generate G code
![DrawingBotV3](DrawingBotV3.png)
First, import the picture into DrawingBotV3, then adjust the
setting of `path finding control` to get the ideal result, then `file` -- `export G code`.

Those are setting that I find would greatly affect the results.
- Plotting resolution
- line density
- adjust brightness

### cncjs to draw
Then we switch to [cncjs](https://github.com/cncjs/cncjs/releases) to operate machine since it is a bit more user friendly than Universal Gcode Platform.
![CNCjs](CNCjs.png)

Connect the machine with the right `port`. `Unlock` first and then `Homing` the pen holder. Then `upload G-code` and start drawing.

![Bunny drew by Pilvi](rabbit.jpg)


## Files
[lasercut_parts_acylic_6mm](lasercut_parts_acylic_6mm.dxf)  
[3D print models](3D_print_model.zip)
[3D print models](3D_print_model.zip)
