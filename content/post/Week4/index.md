---
title:  Computer-Controlled Cutting
Week: 04
weight: 40
description: Vinyl cutting and laser cutting
date:
slug:
image: lamp.JPG
categories:
    - modeling
    - parametric
    - laser cut
    - vinyl cut
---


## Vinyl Cutting
**Applications for the vinyl cutter**
Flexible circuits   
Screen printing mask  
Sandblast mask for glass  
Thermal transfer  

### File preparing
I want to make a few panda sticker because they are sooooo cute!
![panda image tracing](panda.png)
![panda vector outline](panda_outline.png)

### Using Roland GX - 24 vinyl cutter
* **Load vinyl**  
1. press the loading lever down, place the vinyl sheet under the pinch roller  
2. the pinch roller need to stay in the white sticker indicator area. (to measure the width)  
3. mind that vinyl needs to cover the sensor
4. then lift the loading lever to tighten the pinch roller  
Roll: feed from roll behind the machine  
piece: check both length and width  
edge: check only width
![load vinyl](load_vinyl.png)

* **Machine setting**   
Turn on the machine   
select roll/piece/edge, press ENTER   
Measure vinyl: W (width) / L (length) on screen.   

* **Testing**   
Before cutting real pieces, we need to cut a test piece to check if the setting is right.   
Hold TEST button for 1s   
check if it cut through   
then move the blade with cursor keys and hold ORIGIN button for 1s to reset the origin.    
![testing](Roland.png)
**vinyl cutter settings:**  
force: too much > tear tear up paper ; too less > won't cut through, normal: 80gf   
speed: slower > more precise (normal-- 4cm/s, circuits--1cm/s)
rotate angle > blade tilt : for stiff material and not hurt the blade
![Test cut](test_cut.jpg)

* **Setting in Adobe illustrator**   
Put the width and length data in Roland and set canvas size   
In Roland Cutstudio tab (window--extension)  
if pattern has infill, select *output all lines*    
Mirror if it is for hot press   
check *move to origin*    
click print in top right corner   
![settings in Adobe illustrator](vinyl.png)

When the cutting is done, press MENU button and ENTER button
unload vinyl sheet

* **Weeding**   
remove the unwanted vinyl (not lift up, tweezers sheer).  
apply transfer tape to the pattern, and rub the tabe with a scraper.    
lift the transfer tape with the vinyl adhering to it, and apply it to the desired surface.
![cut finished](cut_finish1.jpg)
![transfer tape on the cutting](transfer.jpg)

### Heat transfer vinyl
1. Turn on the heat press machine (It takes 10min to reach 130 C)
2. cut the outline
There are two things different in the cutting.
* shiny side is too thick and may not cut through, place the mat side on top
* mirror the outline in Roland Cutstudio tab
3. Mode, Temp adjust temperature, press Mode again to set time.
4. press the fabric for 3s to flatten the fabric
5. glossy side on top, press for 10s
* For multi layers, heat press the  1st layer for 5s, then add 2nd layer and heat press 10s.


## Laser cutting
**How laser works:**   
Light bouncing between gain medium and activate photon.  
Different gain medium have different wave length, which affect laser beam waist, and determine how detail it can be.  
Laser also have life span: when it gets old, the power drops, and the setting would change.   
laser class: class 1 is safe.

**Applications:**   
[halftone](https://xoihazard.com/tools/halftone/)   
marking, engraving    
bending acrylic      
kerfing-- bend wood/hinge   


**Material:**   
No PVC, hazadous fume---> flame test    
Metal -need fiber laser, never CO2 laser (use CerMark spray, so laser wont reflect back

## Using Epilog Legend 36EXT Laser cutter
switch to turn on the fan,
switch the key, activate the box with card   
turn on the machine  
![activate the machine](start1.jpg)
![switch the key to turn on the machine](start2.jpg)

* **In Adobe illustrator**    
Change stroke thickness to 0.01  
Set diffetent color if it is for different cut setting  
Print (ctrl+P)  

* **In Epilog Dashboard**   
Move the print works to empty area    
Select auto-focus:    
  -plunger: detext the thickness   
  -thickness: needs to input thickness info  
Load settings   
Send to JM  
![Epilog Dashboard Setting](Epilog_board.PNG)

* **In Epilog_manager**
Select jobs tab, choose jobs and hit print.
![Epilog_manager setting](Epilog_manager.png)

* **On machine screen**   
Select job and hit START

### Joints and notch
* Setting test  
focus/power/speed/rate:   
rate: pause rate, too fine --> too much energy in one position    
coordinate system, origin   
vector, raster  

* **Kerf measurement**
![Kerf measurement](Kerf.png)
Kerf varies with different settings, needs to do test for each settings.  
I also realize the square would have overlapping lines, and the kerf will not be very precise. So I adjusted my kerf definition.    
![GH new kerf definition](GH_kerf_definition.png)
![My Kerf test](Kerf_test.png)
material-Acrylic  
thickness: 2mm  
setting:  

* **Different joints:**
![Joint examples from global class](joints.png)   
I was trying to repeat the notch structure in GH, to better understand how it works. During the building, I realize my kerf was wrong, the sequence of offset notch curve should be after I orient the notch.
![Red line is without kerf, green line is the wrong kerf](offset_wrong.png)

![Notch results by Grasshopper](Notch.png)

![GH notch definition](GH_notch_definition.png)

![My notch test](notch.JPG)
I was using 3mm plywood, and mostly test the speed settings. The loaded settings for 3mm plywood with speed 12% is too burned and the joints were very lose. Then I tried speed 20%, 30%, and 40%, and I found at speed-45%, I got a pretty tight joints.

I also want to try this nodes in this [video](https://www.youtube.com/watch?v=6abrj0ikHak&ab_channel=PonokoLaserCutting)

### Raster testing
Since there are three settings affect the raster results, I made a grid of circles in Adobe Illustrator assigned with different colors.
![Raster test](raster_test.png)  
![setting details](raster_test2.png)

![Raster test result](raster_test_result.JPG)

### Shape optimization for laser cutting
At first I want to make something solid and slice it. So I made this definition in Grasshopper with a test sphere. First, I slice the solid and get the surface, then map the sliced surfece to canvas.   
{{< video "./mapping.mp4">}}

Then I used Galapagos to optimize the distance between elements. Here is a short video of the process.  
{{< video "./optimization.mp4">}}

And this is the results:(Here I forgot to add the spacing between elements)   
![optimization result](optimization_result.png)

There is also a very convenient GH plugin that can do this: [OPENNEST](https://www.food4rhino.com/en/app/opennest)

### My press fit
#### Inspiration
I was also inspired by those artist. I want to make something sculptural that can be assembled in multiple way, and hypothetically has a function. For example, it can be used as lamp or candle holders.  
![Karl Nawrot sculpts](Karl_Nawrot_sculpts.png)
![Sidney Gordin](Sidney_Gordin.jpg)

#### Designing
I always like de stijl art pieces, and I think it would be perfect for this press fit assignment. At first, I just went random and played around with different rectangles and squared. But I quickly got lost with notching and not quite happy with the composition either. So I want to use a logic to design the piece and make it easier for me to do the notching. And here is a draft model:  
{{< video "./laser_lamp.mp4">}}
The logic is simple. First, make a cube and shrink all the surfaces to leave the gap.Then use four rectangles on X, Y and Z to connect the six cube surfaces.
![Logic of the pieces](logic.png)
All the elements are build in the Grasshopper, so all parts are parametric and adjustable, such as the location of notching, the kerf size, the shape for the notching, and the shape of elements.

{{< video "./elements.mp4">}}
{{< video "./parametric.mp4">}}

#### Laser cutting
![Lamp](lamp_cut.jpg)   
![Assembling](assemble.jpg)    
![Assembling](assemble2.jpg)  
![Lamp](lamp.JPG)

## Files
[Panda for vinyl cutting](panda.ai)   
[kerf testing](kerf_testing.AI)   
[notch](notch.AI)   
[lamp](lamp.AI)
