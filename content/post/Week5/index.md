---
title: Electronics Production
Week: 05
weight: 50
description:
date:
slug:
image: PCBs.jpg
categories:
    - electronics
    - CNC milling
    - soldering
---


##  Basic info for the PCB production  
PCB: printed circuit boards  

### PCB fabrication  
* Etching  
(Etching is traditionally the process of using strong acid or mordant to cut into the unprotected parts of a metal surface to create a design in intaglio in the metal.)  
water consumption, waste  

* machining  
fast, little waste, easy to handle (just need to vaccum for cleaning)   

* other possible method  
  * laser cutter (require both fiber and CO2 laser)
  * printing with conductive inks [A intereting project](http://fab.cba.mit.edu/classes/863.19/CBA/people/joaowilbert/week6/)
  * combine with sewing of fabric
  * vinyl cutter

climb vs conventional machining (?)  


### material
* Rigid   
FR (Fire retardant)
  * FR1: phenolic paper, a layer of copper foil on top, machine beautifully, 105 °C
the one we use: 1.6mm
  * FR2: similar to FR1, 130 °C
  * FR4:(green) glass fibre in resin, good property, but wear out tool quickly, and dust is hazadous
reflow---can not use FR1, constant 300°C, would burn the FR1,
rotate

* Flexible
1. epoxy film
2. 1126 copper tape

### boardhouse  
Source: [OSH Park](https://oshpark.com/), [JLCPCB](https://jlcpcb.com/)   
The board house can do much detailed traces
* design rules  
how skinny the trace can be   
the distance between traces   

### components
* through-hole
* surface-mount
* chip-scale

mind the component orientation   
check the mark of first pin   
resistor --value
![chamder and dot](chip_assignment.png)

### CAM
Gerber  
png: do not compress, need to use the resolution of milling (1000/inch)


* [EEVblog](https://www.youtube.com/c/EevblogDave)

## CNC Milling
* [Download PCB files](https://gitlab.fabcloud.org/pub/programmers)
![UPDI](UPDI.PNG)
* **Work flow**   
input -- select tool --  calculate the path -- output   

* Attaching PCB to underlay   
  * adheresive tape: force distributed  
  * clamp: uneven force slightly bow the board, can break the   milling. also when cut out, nothing hold the piece  

* Deburring:  
when the bits are extra new or a bit old, the edge might not clean and have some chips, use a ruler to scrap surface to clean the chips  

* milling bits
PCB isolation milling v tool (careful with the Z)  
0.2-0.5mm engrave milling bits  
1mm contour bits  
milling--side ways  
drilling--only verticle ways  


### Calculate milling path in CopperCAM

Open file -- gerbers folder--  *_Cu.gbr  

`file- dimensions`
![dimension](dimension.JPG)
Re-confirm the dimension - and margin should be 1mm  

`file- origin`
![origin](origin.JPG)
Check the origin of the board - , origin should be **0**   

`tool library `
![tool library](tool_library.JPG)
- **tools' identification** --> numbers  
Here , we are using tool 2 and tool 3  
[2 - engraving - 0.2-0.5 mnm diameter, here we are aiming at 0.4]  
[3 - contour - 1mm]
- **plunge speed** : the speed tool move down   
- **maximum depthn per pass** : 0,1 (thickness for copper)    
- **rotation** : can up to 14000, 9000

`parameter- selected tools`   
![set tools](set_tool.JPG)
hatching : remove the rest of the board
drilling, can select one tool or multiple tools

`calculate contour`
![set contour](set_contour.JPG)
![calculated contour](calculate_contour.JPG)

Click `render` to check if all path cut through.  
If not, right click that path, **edit all identical tracks**, and set width to 0.38mm.
![edit path.JPG](edit_path.JPG)

`engrave texts`   
engrave tracks as centerlines
![final setting](final_setting.JPG)

Output - `Mill`
![output](output.JPG)
Check the milling sequence, engrave first, then cut outline.    
Select **white cross** for XY zero point, and **circuit surface** for Z zero point.   
Click OK and save files.
![files for different milling tools](egx.JPG)

### Roland SRM-20
![SRM-20](SRM-20.jpg)

* **Set up**
![brush away dust on underlay  ](brush.jpg)
![put adhesive tape in the back of PCB board ](adhesive.jpg)
Brush away dust on underlay  
Put adhesive tape in the back of PCB board  
Attach PCB board to the underlay     
Use cloth to rub the copper surface    

* **in Vpanel for SRM-20**
![Vpanal for SRM-20](Vpanal20.JPG)

![Vpanal setup](Vpanal_setup.JPG)
select **RML -1**, and change to **User Coordinate System**.

![Milling bits](milling_bits.jpg)
Find the 0.20-0.50 engrave milling bits, and attach it to milling head with allen key  

![Manually set Z origin](setZ.jpg)
Move the bits to as close as possible to PCB board. (mind the distance, too close it won't go any down)   
Hold the milling bits and use allen key to lose a bit untill the tip touch the surface, tighten again  
In `Vpanel`, click Z to set z origin  

![set XY origin](setXY.jpg)
Move the head to the milling origin.  
In `Vpanel`, click `X/Y` to set XY origin  

`Cut`
![cut](cut.JPG)
Add engraving file (T2), `Output` start milling.

When it is done, **brush away dust and check** if the engraving is okay. If not, change the setting and mill again.   
![Reset Z origin after switch milling bits](resetZ.jpg)
**Change milling bits** to 1mm contour milling bits   
Set Z origin again. (the milling bits have different length)  
Change to engraving file (T3), and cut again.  

![milling done](done_milling.jpg)
press `view` to move the bed forward, **vaccum** the dust away. And use a blunt knife to remove the finishes PCB.
![cleaned PCB](cleanedPCB.jpg)
### Roland MDX-40
![Roland MDX-40](MDX-40.jpg)


[Bridge Serial D11C](https://gitlab.fabcloud.org/pub/programmers/programmer-serial-d11c)  
[Programmer Adapter Serial to UPDI](https://gitlab.fabcloud.org/pub/programmers/programmer-adapter-serial-to-updi)

![serial-d11c](D11C.JPG)
![Adapter-serial-to-updi](adapter.JPG)
Similar to updi-d11c, I use CopperCAM to calculate the milling path for both of the PCB boards. and open in Vpanal.

![Vpanal for MDX-40](Vpanal40.png)

Everything else is very similar to SRM-20, except setting for the Z origin. There is a sensor that can automatically set the Z origin.
![sensor on the side](sensor.jpg)
![sensor sensing the Z origin](sensorZ.jpg)
Put the sensor on top of the PCB board, move the head above and close to sensor, `Detect` the Z origin using sensor. Then set XY origin manually.

### Milling failure
* **Z origin not deep enough**  
When I try MDX-40 for the first time, the milling bits only draw lines on half of the PCB board, the other half has nothing (maybe the bed is not totally level). So Z origin is not deep enough.   
To solve this, we can manually adjust the Z origin.   
![adjust Z origin manually](setz-manually.png)
Move head outside of the PCB board, move to z origin, then lower the head （10 step 0.1-2 mm）  
Set Z origin，and apply.

* **PCB board moved during milling**  
the PCB board moved during milling and the whole piece were ruined, the milling bits broke, too.  
I am not quite sure what's the reason, maybe it is because I did not clean the MDF underlay thoroughly, maybe the underlay is a bit old, maybe I did not put enough adhesive tape on the PCB.
![Failure](fail.jpg)

## Soldering
![soldering station](solder_station.jpg)  
* iron
* fume extractor
* lead free wire / paste
* tweezer, knife
* Magnifying Glass

### Collect electronics components
![interactive BOM](component.PNG)  
![All BOM for UPDI](component.jpg)

![Collect electronics component](collect.jpg)
Take a container, and find all material and put them in, so we won't lose anything.

### Preparation
![Wet the sponge](sponge.jpg)
![turn on the soldering iron](turn_on.jpg)
Press both + and - at the same time to turn on the soldering iron, check the solder infomationa and adjust the temperature.

![clean the PCB board with isopropyl alcohol](isopropyl_alcohol.jpg)
Take the isopropyl alcohol from fridge and pour a bit on napkin, and rub the PCB board with the napkin to remove grease and dust.

![Use voltimeter to test if all path is clean](voltimeter_test.jpg)
Turn to Ohm meter and test on different path, if it beeps, then use the knife to clean the circuit.

Take one flux pen from the fridge, too.

### Stuffing
**Sequence for soldering:**  
Start with the important component(SAM D11C) so if it failed, other electronics will not be wastes.  
Then other small component, last with pins. (big pin was interfare soldering)
* button to top
* inside out
* small to big
![](solder1.jpg)
Use flux pen to tap on the soldring area, and melt a bit solder on one side

![](solder2.jpg)
Use tweezer to pick and hold the electronics, at the same time, put the solder iron between the pin and board, heat the solder to fix the electronics.

![](opposite_solder.jpg)
solder the oppisite pin, then fix one by one.

![](more_solder.jpg)
Finally, add more solder to the first solder point.

![](solder_pin.jpg)
solder the connector is a bit hard. Need to put a bit more solder in the beginning, and heat both pin and board with soldering iron tip.

![Done!](PCBs.jpg)

### Verification
{{< video "./blink.mp4">}}

### reflection for soldering
soldering iron tip should be clean and shiny, or the heat wont conduct through. Can melt a bit solder on the tip and wipe on the sponge to clean the tip.  

When soldering, hold the soldering iron and wait a bit to let solder flow, or it will be bumpy and uneven.  

### Other techniques
* Reflow    
**hot plate:** button up, not suitable for FR1, better for FR4    
**hot air:** can use FR1    
put the paste on, locate the component, and place the board on hot plate or use hot air to melt the paste.

* Desoldering: when there is **too much solder**   
**copper braid**: put on the solder, press the solder iron on the braid, and the braid will absorb the excessive solder.  
**vacuum**  
**hot air**: use tweezer hold the component, and hanging in the air, then use hot air to heat the pin. When it is hot enough, the board shall fall.

* Modifying   
When design the board wrong, we can **cutting traces** or **adding jumpers** to fix the board to run test before making a new board

## Flexible PCB
### Notes from tutorial
* material  
3M Copper Foil Tape 1126  
Kapton Tape ( protect Pvc sheet from overheating while soldering)  
Flexible Support such as thin PVC sheet
Cutting mat for vinylcutter  
Electronic components for your board  
**Layer**: Cutting mat-PVC-Kapton-copper    

[Honghao Deng](http://fab.cba.mit.edu/classes/863.17/Harvard/people/HonghaoDeng/project-9/project-9.html) is using backfilm of the transfer tape (not too sticky or too smooth)

* Issues might happen:  
  * The traces have broken  
As they are flexible try to move and solder them, if not you can cut a little piece of tape and stick them together.
  * The traces are not being cut  
Show more blade while cutting and apply more force*
  * The traces are being "lifted"  
Use less force and show less blade


### My test
* Prepare file    
[Hello ATtiny412](https://gitlab.fabcloud.org/pub/programmers/hello-attiny412)  
export SVG  
ungroup all the shape and union the over-lapping shape.

* Set the cutting parameters  
speed: 1cm/s  
Force: 100gf  

![stick copper sheet to epoxy](peeling_copper.jpg)
Cut a piece of copper and epoxy, peel the copper sheet and stick it to epoxy, scrape and avoid bubbles. (epoxy is also fire retardant)

![Load prepared sheet to vinyl cutter](vinly_cut.jpg)

![First try](first_try.jpg)
The blade was too deep and chip the circuit during cutting already

![Second try](second_try.jpg)
It looks nice, but still the epoxy was cut through, and peeling take away epoxy as well.

![third try](third_try.jpg)
Successful!

![cut epoxy mask](epoxy_mask_cut.jpg)
Cut the epoxy mask and carefully remove the cut out epoxy.

![Apply epoxy mask on copper circuit](epoxy_mask.jpg)

![Tada!](mask_on.jpg)

![collect all components](component_flexible.jpg)

![Soldering](Solder_flexible.jpg)
Mind that LED, schottky has polarity, and microcontroller has direction.

{{< video "./blink_flexible.mp4">}}


### Pin head cable
![components](connect_cable0.jpg)
![components and cable](connect_cable.jpg)   
![punch through](connect_cable2.jpg)
