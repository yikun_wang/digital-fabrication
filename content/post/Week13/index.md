---
title: Input Devices
Week: 13
weight: 130
description:
date:
slug:
image: cover.jpg
categories:
---

[Connecting Arduino to Processing](https://learn.sparkfun.com/tutorials/connecting-arduino-to-processing/all)

I learned from my double motor control board failure that it is not a good idea to put everthing on one board. Then I find [Adrianino](https://fabacademy.org/2020/labs/leon/students/adrian-torres/adrianino.html) which use the moduler sensors. I like the idea a lot and decide to make something similar.

## Main board
![ATtiny1614](ATtiny1614.png)

![main board schematic](main_schematic.png)

![main board PCB](main_PCB.png)

![main board](main_board.jpg)

## Sersor Module
### Hall Effect Analog
A Hall effect sensor (or simply Hall sensor) is a type of sensor which detects the presence and magnitude of a magnetic field using the Hall effect.

Hall sensors are commonly used to **time the speed of wheels and shafts**, such as for internal combustion engine ignition timing, tachometers and anti-lock braking systems.  
[Hall-Effect Sensors Applications Guide](https://www.allegromicro.com/en/insights-and-innovations/technical-documents/hall-effect-sensor-ic-publications/hall-effect-ic-applications-guide)

![Hall_effect_analog_schematic](Hall_effect_analog_schematic.png)

![Hall_effect_analog_PCB](Hall_effect_analog_PCB.png)


```
int sensorPin = 0;    // analog input pin
int sensorValue = 0;  

void setup() {
  Serial.begin(115200);
}

void loop() {
  sensorValue = analogRead(sensorPin);
  sensorValue = map(sensorValue, 0, 1024, 1024, 0);
  Serial.println(sensorValue);
  delay(500);
}
```
{{< video "./hall_effect_analog.mp4">}}

### Hall Effect XYZ
![hall effect xyz](hall_effect_xyz.png)

![Hall xyz circuit](Hall_xyz_circuit.png)

I made a mistake though. I saw Neil's board using a 3.3V regulator but I did not check the datasheet of the sensor that VDD requirement is 2.8V to 3.5V.

And there is no regulator on the main board, so my sensor is over powered.

```
#include <Wire.h>

#define address 0x35

void setup() {
   //
   // start serial and I2C
   //
   Serial.begin(115200);
   Wire.begin();
   Wire.setClock(400000);
   //
   // reset TLE493D
   //
   Wire.beginTransmission(address);
   Wire.write(0xFF);
   Wire.endTransmission();
   Wire.beginTransmission(address);
   Wire.write(0xFF);
   Wire.endTransmission();
   Wire.beginTransmission(address);
   Wire.write(0x00);
   Wire.endTransmission();
   Wire.beginTransmission(address);
   Wire.write(0x00);
   Wire.endTransmission();
   delayMicroseconds(50);
   //
   // configure TLE493D
   //
   Wire.beginTransmission(address);
   Wire.write(0x10);
   Wire.write(0x28);
      // config register 0x10
      // ADC trigger on read after register 0x05
      // short-range sensitivity
   Wire.write(0x15);
      // mode register 0x11
      // 1-byte read protocol
      // interrupt disabled
      // master controlled mode
   Wire.endTransmission();
   }

void loop() {
   uint8_t v0,v1,v2,v3,v4,v5;
   //
   // read data
   //
   Wire.requestFrom(address,6);
   v0 = Wire.read();
   v1 = Wire.read();
   v2 = Wire.read();
   v3 = Wire.read();
   v4 = Wire.read();
   v5 = Wire.read();
   //
   // send framing
   //
   Serial.write(1);
   Serial.write(2);
   Serial.write(3);
   Serial.write(4);
   //
   // send data
   //
   Serial.write(v0);
   Serial.write(v1);
   Serial.write(v2);
   Serial.write(v3);
   Serial.write(v4);
   Serial.write(v5);
   }
```

{{< video "./Hall_XYZ.mp4">}}

### Temperature
A thermistor is a type of resistor whose resistance is strongly dependent on temperature

* NTC:   
Negative Temperature Coefficient thermistors have less resistance at higher temperatures
inrush current limiter device in power supply circuits
low-temperature measurements
* PTC:   
Positive Temperature Coefficient thermistors have more resistance at higher temperatures

Application: for circuit protection, as replacements for fuses

Here I am using NTC thermistor.

![NTC thermistor schematic](NTC_schematic.png)

![NTC thermistor PCB](NTC_PCB.png)

```
int sensorPin = 0;
int sensorValue = 0;  

void setup() {
  Serial.begin(115200);
}

void loop() {
  sensorValue = analogRead(sensorPin);
  sensorValue = map(sensorValue, 482, 890, 19, 180);
  Serial.println(sensorValue);
  delay(500);
}
```
{{< video "./temperature.mp4">}}


### Phototransistor
Phototransistor is an electronic switching and current amplification component which relies on exposure to light to operate.

A smaller resistor in series with the phototransistor makes the circuit less sensitive to light.
![phototransistor data](phototransistor_data1.png)

![phototransistor datasheet](phototransistor_data.png)

![Phototransistor schematic](Phototransistor_schematic.png)

![Phototransistor PCB](Phototransistor_PCB.png)

```
int sensorPin = 0;
int sensorValue = 0;

void setup() {
  Serial.begin(115200);
}

void loop() {
  sensorValue = analogRead(sensorPin);
  sensorValue = map(sensorValue, 0, 1024, 1024, 0);
  Serial.println(sensorValue);
  delay(500);
}
```
{{< video "./phototransistor.mp4">}}

### Step Response
transmit (tx) pin is made alternately high (5V or 3.3V) and low (0V), This charges and discharges the tx electrode. The electric field between two copper field would affect the result.

Step response is using voltage divider which is a simple circuit which turns a large voltage into a smaller one. Using just two series resistors and an input voltage, we can create an output voltage that is a fraction of the input.  
[Voltage Dividers](https://learn.sparkfun.com/tutorials/voltage-dividers)

Here is Rober's explaination of how step response works: [Sensing with Step Response](https://roberthart56.github.io/SCFAB/SC_lab/Sensors/tx_rx_sensors/index.html)

![Robert's TX/RX schematic example](txrx_schematic.jpg)

![step response schematic](step_response_schematic.png)

![step response PCB](step_response_PCB.png)

![step response module](step_response.jpg)
```
long result;   //variable for the result of the tx_rx measurement.
int analog_pin = 1; // PA5 -- Tx
int tx_pin = 0;  //    PA4 -- Rx
void setup() {
pinMode(tx_pin,OUTPUT);      //Pin 2 provides the voltage step
Serial.begin(115200);
}

long tx_rx(){         //Function to execute rx_tx algorithm and return a value
                      //that depends on coupling of two electrodes.
                      //Value returned is a long integer.
  int read_high;
  int read_low;
  int diff;
  long int sum;
  int N_samples = 100;    //Number of samples to take.  Larger number slows it down, but reduces scatter.

  sum = 0;

  for (int i = 0; i < N_samples; i++){
   digitalWrite(tx_pin,HIGH);              //Step the voltage high on conductor 1.
   read_high = analogRead(analog_pin);        //Measure response of conductor 2.
   delayMicroseconds(100);            //Delay to reach steady state.
   digitalWrite(tx_pin,LOW);               //Step the voltage to zero on conductor 1.
   read_low = analogRead(analog_pin);         //Measure response of conductor 2.
   diff = read_high - read_low;       //desired answer is the difference between high and low.
 sum += diff;                       //Sums up N_samples of these measurements.
 }
  return sum;
}                         //End of tx_rx function.


void loop() {

result = tx_rx();
result = map(result, 8000, 11000, 0, 1024);  //I recommend mapping the values of the two copper plates, it will depend on their size
Serial.println(result);
delay(100);
}
```
{{< video "./step_response.mp4">}}

I was also looking into Matt's [Touchpad](http://fab.cba.mit.edu/classes/863.10/people/matt.blackshaw/week8.html) idea, but I have not get that far yet.


### Flex Sensor
[Flex Sensor Hookup Guide](https://learn.sparkfun.com/tutorials/flex-sensor-hookup-guide/all)  
A flex sensor or bend sensor is a sensor that measures the amount of deflection or bending.

-Flat Resistance: 10K Ohms
-Bend Resistance Range: 60K to 110K Ohms

I need one resistor values from 10KΩ to 100KΩ.

![](how-it-works-bent.png)
![](how-it-works-straight.png)

![example circuit](example_circuit.png)
![example circuit schematic](example_circuit_schem.png)

First I need to measure the **resistance of Flex sensor**, both **straight and bent**, also measure the **resistor value**, and put into the code.
- VCC = 4.98V
- resistance of resistor = 22.15 kΩ
- resistance of straight Flex sensor  = 11.0 kΩ
- resistance of bent Flex sensor = 9.0 kΩ

![Flex Sensor Circuit](flex_sensor.jpg)


```
const int FLEX_PIN = 0;
const float VCC = 4.98; // Measured voltage of Ardunio 5V line
const float R_DIV = 22150.0; // Measured resistance of 3.3k resistor

// Upload the code, then try to adjust these values to more
// accurately calculate bend degree.
const float STRAIGHT_RESISTANCE = 11000.0; // resistance when straight
const float BEND_RESISTANCE = 9000.0; // resistance at 90 deg

void setup()
{
  Serial.begin(115200);
  pinMode(FLEX_PIN, INPUT);
}

void loop()
{
  // Read the ADC, and calculate voltage and resistance from it
  int flexADC = analogRead(FLEX_PIN);
  float flexV = flexADC * VCC / 1023.0;
  float flexR = R_DIV * (VCC / flexV - 1.0);
  Serial.println("Resistance: " + String(flexR) + " ohms");

  // Use the calculated resistance to estimate the sensor's
  // bend angle:
  float angle = map(flexR, STRAIGHT_RESISTANCE, BEND_RESISTANCE,
                   0, 90.0);
  Serial.println("Bend: " + String(angle) + " degrees");
  Serial.println();

  delay(500);
}
```

{{< video "./flex_sensor.mp4">}}

### PIR Sensor
Since I was planning using PIR sensor in my final project. I also want to test it this week. I am using pir sensor from DFROBOT. There are three pins and easy to connect.

```
int ledPin = 10; // LED
int pirPin = 0;  // PIR Out pin
int pirStat = 0; // PIR status

void setup() {
 pinMode(ledPin, OUTPUT);     
 pinMode(pirPin, INPUT);     
 Serial.begin(115200);
}
void loop(){
 pirStat = digitalRead(pirPin);
 if (pirStat == HIGH) { // if motion detected
   digitalWrite(ledPin, HIGH);  
   Serial.println("You moved!");
 }
 else {
   digitalWrite(ledPin, LOW);
 }
}
```
There is a tiny blue LED on the board already but I linked the white LED on the main board still.
The first thing I notice is that there is a small delay, even though I stopped moving, the LED is still on for a while. At first I thought the LED is just delayed after receive the signal.
{{< video "./PIR_testing.mp4">}}

Then I opened serial monitor to check the massage the sensor is sending.
{{< video "./PIR_test.mp4">}}
I find that it is still sending signal even though I stopped moving. The LED turn off when the signal stopped. I am not sure if I want to adjust this for my final project yet.

Then I was testing the sensing distance. It can be around **3 meters** far. I think that is plenty for my final project.
{{< video "./PIR_distance.mp4">}}
