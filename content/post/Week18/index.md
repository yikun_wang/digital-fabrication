---
title: Invention, IP and Income
Week: 18
weight: 180
description:
date:
slug:
image:
categories:
---


## Future Plan of Final project
My final project is **Cymatics Lamp**, if you are interested, you can find more details of how it was develop on my final project page.

This project is more like a great learning chance for me, to put together what I have learned in Fablab and create something interesting since I have no related background. It was truly an exiting moment when I press the touch button, the music started to play and the water wave pattern would change along with music. I am very happy that the main function work successfully.

Since this project is out of pure interest and very personalized, I have no intention to commercialize it. The project is currently not fully done, and I can improve some parts too, and I will do that in summer.

* Design the lamp structure  
I have tested and laser cut the origami lamp cover, and fold it. But the structure of holding the cover is still under testing. I am considering use a metal frame, but I have not figure out some join details with lamp base.

* Incorporate LEDS  
Right now the LEDs are not connected yet, and it has only been tested with code. Since the controlling box just control music, one idea I have is that the LED light effect could react to the music to enhance the music.

* Base  
I sure did put lots of thoughts into base design to hide all electrics and make a few integrations to make it look as simple as possible. However as a lamp base which hold a water plate on top, it is not very stable with light weight itself. I think maybe metal would be more stable but I do not know how to do the complicated shape with metal.   

* Noise and volume  
When I test the speaker, I noticed that to be able to generate a clear pattern on the water surface, the volume of the speaker is high and there are also some noise generated from vibration. One thing I can do it attaching the speaker and water plate tighter to reduce the air vibrations and reduce noises. But I do not know what else I can do with the volume, or it just have to be an loud lamp.

## Copyright and license
I have a blur idea about CC licenses before and was not sure what it protect exactly. I was looking at [ Creative Commons](https://creativecommons.org/share-your-work/) learned a bit more details of different CC licenses from [here](https://creativecommons.org/about/cclicenses/).

I would like to see that others trying the project or get inspired when developing their project. However, since I have no intention to commercialize it, I do not what it to be commercialize by others without me knowing it. So I am using **CC BY-NC-SA** license which license allows reusers to distribute, remix, adapt, and build upon the material in any medium or format for noncommercial purposes only, and only so long as attribution is given to the creator.

## One min Video & Slide
You can see the one minute video and slide of final project from [here](https://yikun_wang.gitlab.io/digital-fabrication/final-project/).
